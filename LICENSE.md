# Licence

## Source code

If it is not specified, you can:
* run the program
* study how the program works (of course, you have accessed to the source code)
* modify the program
* share the program, either with or without modifications, in binary form or source code

However, it is not free/libre software.
In fact, some contributions can not be used:
* in a commercial organization (like a company)
* in a commercial way (like a commercial product)
* for making profit
* for selling a software, even if the sell does not generate profit

This project was made by: Jérémy Bonnardel, Alexi Coard, Anthony Demongeot, Nicolas Foulon, Jason Labiod, [Nicola Spanti](http://www.nicola-spanti.info/), Geoffrey Thomas.
You can know who wrote the source code thanks to @author comments and the git repository (`git log --follow file_path | grep -i author | sort | uniq`).
Details of commits that created and modified a file can be known with `git log -p file_path`.

The source code wrote by Nicolas Foulon and Jason Labiod have the commercial restrictions.
The source code made by others is under LGPLv3+, except if there is information about a license.
The source code made by new contributors is always under LGPLv3+.
LGPL3+ is an abbreviation for [GNU Lesser General Public License (of the Free Software Foundation)](https://www.gnu.org/licenses/lgpl.html) version 3 or at your option any later version.

### SQLite

"All of the code and documentation in SQLite has been dedicated to the public domain by the authors."
[sqlite.org/copyright.html](https://www.sqlite.org/copyright.html)

## Images

Images in `src/res/drawable/*x*/` are in the public domain and were done by ["The Tango Desktop Project"](http://tango.freedesktop.org).
src/res/others/launcher.png was apparently done by familyalins.com that have agreed that everyone can do what he wants with it.

## Presentation

Presentations in all languages are under [Creative Commons 0 license](https://creativecommons.org/publicdomain/zero/1.0/) (that is equivalent to the public domain).
Images included in the presentation are under their own licenses.
