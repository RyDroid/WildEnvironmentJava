# Wild Environment

## What is it?

Read the presentation in english or french.

## Contribute

### Files

#### Source code

##### Text file "settings"

A text file is a file readable with a text editor.
For example, files containing only Markdown, TeX, HTML, SVG and source code in C, C++, Java, Scala are considered as text files, but OpenDocument and PDF are not.

All text files have to be in [UTF-8](https://en.wikipedia.org/wiki/UTF-8) (without [BOM](https://en.wikipedia.org/wiki/Byte_order_mark)) encoding (generally UTF-8 on GNU/Linux and [ISO 8859-15](https://en.wikipedia.org/wiki/ISO/IEC_8859-15) on Windows) and LF character newline (sometime called UNIX newline/endline) (Windows generally used CRLF).
Unfortunately, by default some text editors save with the same encoding and character newline of your OS.
However, if your text editor manages [EditorConfig](http://editorconfig.org/), you have nothing to do, enjoy!

An empty line must be at the end of text files.
The aim is readability with [`cat file`](https://en.wikipedia.org/wiki/Cat_%28Unix%29).

###### Eclipse

In order to force to use UTF-8 and LF for this project, do right click on the project in "Package Explorer", then "Properties", in the right panel select "Resource", now you are on the right and enough intelligent to find what to change.

##### Coding style

We use english, so please use it everywhere in the project (messages, function names, doc, etc).

Names of variables, functions/methods, classes and everything else have to be clear, even if the name is a little longer.
You also do not have to forget to create documentation.

Following the [PEP (Python Enhancement Proposal) 20](https://www.python.org/dev/peps/pep-0020/) is a good thing, even if the project does not use the Python language.

utils packages are packages that can be useful in an other project.

If you do not want Nicola Spanti to eat you, the project has to be developable with 100% [free software](https://www.gnu.org/philosophy/free-sw.html) (with free as in freedom).
If you do not want other people to be unhappy, you should also consider the fact that free can be equals to gratis.

static members and methods must before non static ones.
Classes that only have static members and methods must be final and have a private constructor.

#### Text files

If you need to make a structured document, you should consider [Markdown](https://en.wikipedia.org/wiki/Markdown).
For example, this document uses the Markdown syntax.

For longer texts or presentations, [LaTeX](http://latex-project.org/) could be a good option.

##### Markdown

A list item must be done with "-" (minus character) to avoid potential confusion with "*" that can be used for emphasizing.
The extension of Markdown files must be "md" or "markdown", "mkd" is not widely recognized as Markdown so it must not be used.

### Git

Use `git status` to check that your modifications will be commited.
You have to pull before push. 

## License and authors

See [LICENSE.md](LICENSE.md) and logs of git for the full list of contributors of the project.
