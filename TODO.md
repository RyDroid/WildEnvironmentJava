# Things to do

* core
  * Corriger le problème de saturation mémoire qaund il y a beaucoup d'éléments
  * Améliorer le makefile
    * Compiler les sources Java
    * Créer un jar
    * Nettoyer dont les fichiers intermédiaires LaTeX
    * "Compiler" la présentation en PDF
    * Créer une archive des sources sans les fichiers intermédiaires
    * Créer des paquets prêt pour l'installation (Debian, RPM, etc)
  * Cycle jour/nuit (pas important)
  * Saisons (pas important)
  * Sexe hermaphrodite (pas important)
  * Événements cataclysmique (volcan, tremblement de terre, météorite, etc) (pas important)
  * Défense de certains végéteaux (comme les cactus) (pas important)
  * Plantes carnivores (pas important)
  * Omnivores (pas important)
  * Différents types de végétaux, herbivores et carnivores (pas important)
    * Animaux spéciaux (KingKong, Godzilla, etc) (pas important)
  * Menstruations pour les femmes
  * Artificial Intelligence
    * Gérer les groupes (problème : RequestGroup)
    * Gérer la modification de l'information d'un neuronne -> pendant une mise à jour de neuronnes (méthode update de Neuron)
    * Le leader s'adresse au groupe et le commande (méthode order), un membre ne peut que parler au leader (méthode request)
    * Lors de la transmission d'un message, on remonte celui-ci au chef, qui décide ou non de le transmettre (à ajouter pour la réponse à l'attaque)
    * Analyser un message
    * Se grouper et se dégrouper
    * Réseau neuronnal
    * Méthode si rien ne correspond dans le champ de vision et rien dans la mémoire.
  * Environment fait presque 600 lignes !
    * Faire des méthodes statiques pour EnvironmentStats en prenant en paramètre un Environment
    * Faire un EnvironmentSaveable extends Environment pour les méthodes de bases de données et ne pas oublier de faire EnvironmentDrawable extends EnvironmentSaveable

* graphics
  * faire apparaitre au bon endroit (c'est à dire en dessous du menu) la toolbar
  * ajouter un bouton play/pause à la toolbar
  * impression du graphicPanel (pas important)
  * inclure dans "Aide" la visualisation du PDF de presentation/ (pas important)

* data_management
  * database
    * Faire un EnvironmentSaveable extends Environment pour les méthodes de bases de données et ne pas oublier de faire EnvironmentDrawable extends EnvironmentSaveable
    * Faire un script de création de la structure de la base de données

* license
  * Convaincre les autres de passer leur travail sous une licence libre.
  * Recoder les parties qui ne sont pas libres.

* presentation
  * Refaire l'image du MCD de la base de données d'une manière un peu plus complète, mais pas trop pour rester sobre. (pas important)
  * Pouvoir éditer l'image du MCD de la base de données avec un logiciel libre et gratuit qui fonctionne sur un OS libre et gratuit. (pas important)
