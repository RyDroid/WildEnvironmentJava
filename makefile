# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


SRC_DIR=src
BIN_DIR=bin
DOC_DIR=doc
PRESENTATION_DIR=presentation

RM=rm -f

PACKAGE=wild-environment
FILES_TO_ARCHIVE=$(SRC_DIR)/* $(PRESENTATION_DIR)/* build.xml makefile \
	LICENSE* README* TODO* \
	.classpath .project .settings .gitignore .editorconfig \
	sqlite* database*


.PHONY: \
	default \
	check test test-static test-encoding \
	jar $(DOC_DIR) \
	archives dist default-archive zip tar-gz tar-bz2 tar-xz 7z \
	clean clean-build clean-build-release clean-build-debug \
	clean-bin clean-bin-native clean-bin-java \
	clean-compile-tmp clean-archives clean-git


default: jar dist

all: jar archives


check: test

test: test-static

test-static: jar test-encoding

test-encoding:
	@chmod +x ./tools/encoding-tests.sh
	./tools/encoding-tests.sh $(SRC_DIR)


jar: build.xml $(SRC_DIR)/*
	ant jar


archives: zip tar-gz tar-bz2 tar-xz 7z

dist: default-archive

default-archive: tar-xz

zip: $(PACKAGE).zip

$(PACKAGE).zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- $(FILES_TO_ARCHIVE)

tar-gz: $(PACKAGE).tar.gz

$(PACKAGE).tar.gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz -- $(FILES_TO_ARCHIVE)

tar-bz2: $(PACKAGE).tar.bz2

$(PACKAGE).tar.bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 -- $(FILES_TO_ARCHIVE)

tar-xz: $(PACKAGE).tar.xz

$(PACKAGE).tar.xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz -- $(FILES_TO_ARCHIVE)

7z: $(PACKAGE).7z

$(PACKAGE).7z: $(FILES_TO_ARCHIVE)
	7z a -t7z $(PACKAGE).7z $(FILES_TO_ARCHIVE)


clean: clean-tmp clean-build clean-bin clean-compile-tmp
	$(RM) -rf -- $(BIN_DIR)/ $(DOC_DIR)/ $(SRC_DIR)/*~

clean-tmp:
	$(RM) -f -- \
		*~ .\#* \#* *.swp *.swap *.bak *.backup \
		*.sav *.save *.autosav *.autosave \
		*.log *.log.* log/ logs/ \
		.cache/ .thumbnails/ \
		.CACHE/ .THUMBNAILS/

clean-build: clean-build-release clean-build-debug
	@$(RM) -rf -- build*/ Build*/ builds*/ Builds*/

clean-build-release:
	@$(RM) -rf -- release/ Release/

clean-build-debug:
	@$(RM) -rf -- dbg/ DBG/ debug/ Debug/

clean-bin: clean-bin-native clean-bin-java

clean-bin-native:
	$(RM) -rf -- \
		*.o *.a *.so *.ko *.lo *.dll *.out \
		bin/ BIN/ binaries/

clean-bin-java:
	$(RM) -f -- *.class

clean-compile-tmp:
	$(RM) -f -- \
		*.asm *.ihx *.lk *.lst *.map *.mem \
		*.rel *.rst *.s *.sym

clean-archives:
	$(RM) -f -- \
		*.deb *.rpm *.exe *.msi *.dmg *.apk *.ipa \
		*.DEB *.RPM *.EXE *.MSI *.DMG *.APK *.IPA \
		*.tar.* *.tgz *.gz *.bz2 *.lz *.lzma *.xz \
		*.TAR.* *.TGZ *.GZ *.BZ2 *.LZ *.LZMA *.XZ \
		*.zip *.7z *.rar *.jar \
		*.ZIP *.7Z *.RAR *.JAR \
		*.iso *.ciso *.img *.gcz *.wbfs \
		*.ISO *.CISO *.IMG *.GCZ *.WBFS

clean-git:
	git clean -fdx
