package core.action;

import core.environment.Element;
import core.living_being.Animal;

/**
 * An action that an animal can do.
 */
public interface Action
{
	void act(Animal actor, Element target);
	
	/**
	 * Returns the id of the action.
	 * @return id of the action
	 */
	int getId();
}