package core.action;

import core.environment.Element;
import core.living_being.Animal;

/**
 * An action to attack
 */
public class Attack implements Action
{
	@Override
	public void act(Animal actor, Element target)
	{
		if(target instanceof Animal)
			this.act(actor, (Animal) target);
		else
			System.out.println(this.getClass().getSimpleName()+": target is not an animal");
	}
	
	public void act(Animal actor, Animal target)
	{
		target.addHealth((int) -actor.getStrength());
		target.fireIsAttacked(actor);
		if (target.getHealth() <= 0)
		{
			actor.fireActionPerformed();
		}
	}

	@Override
	public int getId()
	{
		return 0;
	}
}