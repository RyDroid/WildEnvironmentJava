package core.action;

import core.environment.Element;
import core.environment.Zone;
import core.living_being.Animal;

public class Drink implements Action
{
	@Override
	public void act(Animal actor, Element target)
	{
		if(target instanceof Zone)
			this.act(actor, (Zone) target);
		else
			System.out.println(this.getClass().getSimpleName()+": target is not a zone");
	}
	
	public void act(Animal actor, Zone zone)
	{
		if (zone.isDrinkable())
		{
				actor.addThirst(-20);
		}
		if (actor.getThirst() == 0){
			actor.fireActionPerformed();
		}
		//actor.getBrain().addNeuron(new Neuron(actor.getBrain(), new Information(InformationType.WATER,actor.getPosition(),50) ,new Date(), actor.getBrain().getNeurons())); // pour l'instant l'information a une importance de 50, il faut qu'on definisse comment on donne les importances.
	}
	
	@Override
	public int getId()
	{
		return 1; 
	}
}