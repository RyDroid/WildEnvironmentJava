package core.action;

import core.environment.Element;
import core.living_being.Animal;
import core.living_being.LivingBeing;

public class Eat implements Action
{
	@Override
	public void act(Animal actor, Element t)
	{
		if(actor.isAlive())
		{
			LivingBeing target = (LivingBeing) t;
			if (target.size() <= 0 || actor.getHunger() == 0)
			{
				actor.fireActionPerformed();
				if (target instanceof Animal && target.size() <= 0)
				{
					Animal a = (Animal) target;
					a.setShouldDisappear(true);
				}
			}
			else
			{
				int hunger = actor.getHunger();
				if (hunger > 5)
					hunger = 5;
				
				double targetSize = target.size();
				double sizeEaten;
				if(hunger - targetSize > 0)
					sizeEaten = targetSize ;
				else
					sizeEaten = hunger;
				
				actor.addHunger(- (int)sizeEaten);
				target.addToSize(- sizeEaten);
				//actor.getBrain().addNeuron(new Neuron(actor.getBrain(), new Information(InformationType.FOOD, actor.getPosition(), 50),new Date() , actor.getBrain().getNeurons()));
			}
			//System.out.println(actor.getType() + " yumyum " + target.getType());
		}
	}
	
	@Override
	public int getId()
	{
		return 2;
	}
}