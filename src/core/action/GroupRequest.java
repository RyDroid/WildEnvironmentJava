package core.action;

import core.environment.Element;
import core.living_being.Animal;

/**
 * A request to be part of a group
 */
public class GroupRequest implements Action
{
	
	@Override
	public void act(Animal actor, Element target)
	{
		//Animal a = (Animal) target;
		/*a.fireGroupRequest(actor);*/ //TODO manage groups
		actor.fireActionPerformed();
	}
	
	@Override
	public int getId()
	{
		return 3;
	}
}