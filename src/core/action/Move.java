package core.action;

import java.util.ArrayList;

import core.environment.Element;
import core.living_being.Animal;
import data_management.utils.Log;

public class Move implements Action
{
	ArrayList<Integer> tabId = new ArrayList<Integer>();
	
	@Override
	public void act(Animal actor, Element target)
	{
		if(!tabId.contains(target.getId()))
		{
			try {
				Log.addToFile(actor +"move to " + target);
				tabId.add(target.getId());
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		actor.move(target.getPosition());
		if (target instanceof Animal) {
			Animal a = (Animal) target;
			if (a.isNearToSee(actor))
				a.fireSomethingIsComing(actor);
		}
		if (actor.isNearToAct(target))
			actor.fireActionPerformed();
		if (actor.isNearToAct(target))
			actor.fireActionPerformed();
	}
	
	@Override
	public int getId()
	{
		return 4;
	}
}