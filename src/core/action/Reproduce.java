package core.action;

import core.environment.Element;
import core.living_being.Animal;

public class Reproduce implements Action
{
	@Override
	public void act(Animal actor, Element target)
	{
		if(target instanceof Animal)
			this.act(actor, (Animal) target);
		else
			System.out.println(this.getClass().getSimpleName()+": target is not an animal");
	}
	
	public void act(Animal actor, Animal partner)
	{
		if (actor.isMale()) {
			partner.birth(actor);
			partner.unPregnantable();
		}
		else {
			actor.birth(partner);
			actor.unPregnantable();
		}
		actor.fireActionPerformed();
	}

	@Override
	public int getId()
	{
		return 5;
	}
}