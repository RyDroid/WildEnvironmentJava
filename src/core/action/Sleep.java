package core.action;

import core.environment.Element;
import core.living_being.Animal;

public class Sleep implements Action
{
	@Override
	public void act(Animal actor, Element target)
	{
		actor.addTiredNess(-1);
		if (actor.getTiredNess() == 0)
			actor.fireActionPerformed();
	}
	
	@Override
	public int getId()
	{
		return 6;
	}
}