package core.action;

import core.environment.Element;
import core.living_being.Animal;

public class Tell implements Action
{
	@Override
	public void act(Animal actor, Element target)
	{
		if(target instanceof Animal)
			this.act(actor, (Animal) target);
		else
			System.out.println(this.getClass().getSimpleName()+": target is not an animal");
	}
	
	public void act(Animal actor, Animal target)
	{
		Animal a = (Animal) target;
		a.fireIsSaid(actor.getMessage());
		actor.fireActionPerformed();
	}
	
	@Override
	public int getId()
	{
		return 7;
	}
}