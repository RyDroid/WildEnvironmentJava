package core.action;

import org.junit.Test;

import core.living_being.Carnivore;

public final class Tests {
	/**
	 * Don't let anyone instantiate this class.
	 */
	private Tests() {}
	
	
	@Test
	public static void testEatAnotherOne() {
		Carnivore A = new Carnivore(null);
		Carnivore B = new Carnivore(null);
		
		System.out.println(A);
		System.out.println(B);
		
		Action action = new Eat();
		action.act(A, B);
		System.out.println(A);
		System.out.println(B);
	}
}