package core.action;

import core.environment.Element;
import core.living_being.Animal;

public class Wait implements Action
{
	@Override
	public void act(Animal actor, Element target)
	{
		actor.fireActionPerformed();
	}

	@Override
	public int getId()
	{
		return 8;
	}
}