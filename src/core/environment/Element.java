package core.environment;

import core.living_being.Animal;
import core.utils.Position2D;
import data_management.database.Connexion;

/**
 * An element that has a position.
 */
public abstract class Element
{
	protected static int id = 0;
	
	private int num;
	private Position2D position;
	protected Connexion connection = new Connexion("database.db");
	
	
	/**
	 * Constructor.
	 * @param Position2D position of the new element
	 */
	public Element(Position2D position)
	{
		this.num = ++Element.id;
		this.position = position;
	}
	
	/**
	 * Default constructor.
	 */
	public Element()
	{
		this(new Position2D());
	}
	
	
	/**
	 * Returns the id of the element.
	 * @return int id of the element
	 */
	public int getId()
	{
		return this.num;
	}
	
	public void setId(int id)
	{
		this.num = id;
	}
	
	public void setPosition(Position2D position)
	{
		this.position = position;
	}
	
	public void setPosition(double x, double y)
	{
		this.position = new Position2D(x, y);
	}
	
	public Position2D getPosition()
	{
		return this.position;
	}
	
	/**
	 * Returns the x position.
	 * @return the x position
	 */
	public double getX()
	{
		return this.position == null ? 0 : this.position.getX();
	}
	
	/**
	 * Returns the y position.
	 * @return the y position
	 */
	public double getY()
	{
		return this.position == null ? 0 : this.position.getY();
	}
	
	public boolean isVisibleFor(Animal animal)
	{
		return animal.getPosition().isNear(position, 10000/*animal.getVision()*/);
	}
	
	public double distanceBetween(Element element)
	{
		return this.position.distanceBetween(element.getPosition());
	}
	
	public abstract double getWidth();
	public abstract double getHeight();
	
	
	@Override
	public String toString()
	{
		return "Element [num=" + num + ", position=" + position + "]";
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Element)) {
			return false;
		}
		Element other = (Element) obj;
		if (position == null) {
			if (other.position != null) {
				return false;
			}
		}
		else if (!position.equals(other.position)) {
			return false;
		}
		return true;
	}
}
