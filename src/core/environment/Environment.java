package core.environment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import core.living_being.Animal;
import core.living_being.Carnivore;
import core.living_being.FootStep;
import core.living_being.FootStepsList;
import core.living_being.Herbivore;
import core.living_being.LivingBeing;
import core.living_being.Vegetal;
import core.living_being.brain.Information;
import core.living_being.brain.Neuron;
import core.utils.Position2D;
import data_management.database.BrainRequest;
import data_management.database.CarnivoreRequest;
import data_management.database.Connexion;
import data_management.database.ElementRequest;
import data_management.database.HerbivoreRequest;
import data_management.database.InformationRequest;
import data_management.database.NeuronRequest;
import data_management.database.VegetalRequest;
import data_management.database.ZoneRequest;

/**
 * A living environment with a lot of things.
 */
public class Environment implements Runnable
{
	public static final short TIME_BETWEEN_THREAD_SLEEP_ZOMBIE          = 100;
	public static final short TIME_BETWEEN_THREAD_SLEEP_VERY_SLOW       =  60;
	public static final short TIME_BETWEEN_THREAD_SLEEP_SLOW            =  45;
	public static final short TIME_BETWEEN_THREAD_SLEEP_NORMAL          =  30;
	public static final short TIME_BETWEEN_THREAD_SLEEP_FAST            =  15;
	public static final short TIME_BETWEEN_THREAD_SLEEP_VERY_FAST       =  10;
	public static final short TIME_BETWEEN_THREAD_SLEEP_INCREDIBLY_FAST =   5;
	public static final short TIME_BETWEEN_THREAD_SLEEP_LIGHT           =   1;
	
	private static FootStepsList footStepList = new FootStepsList();
	private ArrayList<Element>   elements = new ArrayList<Element>();
	private ArrayList<Vegetal>   vegetals = new ArrayList<Vegetal>();
	private ArrayList<Herbivore> herbivores = new ArrayList<Herbivore>();
	private ArrayList<Carnivore> carnivores = new ArrayList<Carnivore>();
	private ArrayList<Zone>      zones = new ArrayList<Zone>();
	private ArrayList<Element>   tmp = new ArrayList<Element>();
	
	private short timeBetweenThreadSleep = TIME_BETWEEN_THREAD_SLEEP_NORMAL;
	protected int counterTime = 0;
	private int counterLive;
	private int width  = 100;
	private int height = 100;
	
	private boolean play = false;
	
	
	public Environment()
	{}
	
	public Environment(int width, int height) {
		this();
		this.setWidth(width);
		this.setHeight(height);
	}
	
	public Environment(int width, int height, int nbVegatals, int nbAnimals) {
		this(width, height, nbVegatals, nbAnimals, nbAnimals);
	}
	
	public Environment(int width, int height, int nbVegatals, int nbHerbivores, int nbCarnivores) {
		this(width, height);
		this.add(nbVegatals, nbHerbivores, nbCarnivores);
	}
	
	public void addTmp(Element element) {
		if (element != null) {
			tmp.add(element);
		}
	}
	
	public void add(Element element) {
		if (element != null)
		{
			if(element instanceof Vegetal) {
				vegetals.add((Vegetal) element);
			} else if(element instanceof Carnivore) {
				carnivores.add((Carnivore) element);
			} else if(element instanceof Herbivore) {
				herbivores.add((Herbivore) element);
			} else if(element instanceof Zone) {
				zones.add((Zone) element);
			} else {
				System.out.println("Unknown type: " + element.getClass());
			}
		}
		this.elements.add(element);
	}
	
	public void add(int nbVegatals, int nbHerbivores, int nbCarnivores)
	{
		while (nbVegatals-- > 0) {
			new Vegetal  (this, Position2D.getRandom(this.width, this.height));
		}
		while (nbHerbivores-- > 0) {
			new Herbivore(this, Position2D.getRandom(this.width, this.height));
		}
		while (nbCarnivores-- > 0) {
			new Carnivore(this, Position2D.getRandom(this.width, this.height));
		}
	}
	
	public void add(int nbVegatals, int nbAnimals)
	{
		this.add(nbVegatals, (int) (nbAnimals * 0.65), (int) (nbAnimals * 0.35));
	}
	
	public void remove(Element element)
	{
		if(element instanceof Vegetal) {
			vegetals.remove(element);
		} else if(element instanceof Carnivore) {
			carnivores.remove(element);
		} else if(element instanceof Herbivore) {
			herbivores.remove(element);
		} else if(element instanceof Zone) {
			zones.remove(element);
		} else {
			System.out.println("Unknown type: " + element.getClass());
		}
		this.elements.remove(element);
	}
	
	public void removeLivingBeings() {
		this.vegetals.clear();
		this.herbivores.clear();
		this.carnivores.clear();
	}
	
	public int getNbElements() {
		return this.elements == null ? 0 : this.elements.size();
	}
	
	public int getNbLivingBeings() {
		return this.getNbVegetals() + this.getNbAnimals();
	}
	
	public int getNbVegetals() {
		return this.vegetals == null ? 0 : this.vegetals.size();
	}
	
	public int getNbHerbivores() {
		return this.herbivores == null ? 0 : this.herbivores.size();
	}
	
	public int getNbCarnivores() {
		return this.carnivores == null ? 0 : this.carnivores.size();
	}
	
	public int getNbZones() {
		return this.zones == null ? 0 : this.zones.size();
	}
	
	public int getNbFootSteps() {
		return Environment.footStepList == null ? 0 : Environment.footStepList.size();
	}
	
	public int getNbAnimals() {
		return this.getNbCarnivores() + this.getNbHerbivores();
	}
	
	public LivingBeing[] getLivingBeings() {
		LivingBeing[] livingBeings = new LivingBeing[0];
		System.arraycopy(vegetals.toArray(),   0, livingBeings, 0,                       vegetals.size());
		System.arraycopy(carnivores.toArray(), 0, livingBeings, livingBeings.length + 1, carnivores.size());
		System.arraycopy(herbivores.toArray(), 0, livingBeings, livingBeings.length + 1, herbivores.size());
		return livingBeings;
	}
	
	public void removeZones() {
		this.zones = new ArrayList<Zone>();
	}
	
	public void removeAll() {
		this.removeLivingBeings();
		this.removeLivingBeings();
	}
	
	public ArrayList<Element> getElements() {
		return this.elements;
	}
	
	public Vegetal[] getVegetals() {
		Vegetal[] vegetals = {};
		return this.vegetals.toArray(vegetals);
	}
	
	public Herbivore[] getHerbivores() {
		Herbivore[] herbivores = {};
		return this.herbivores.toArray(herbivores);
	}
	
	public Animal[] getAnimals() {
		Animal[] animals = {};
		for (int i=0; i < animals.length-this.getHerbivores().length; ++i)
		{
			animals[i] = this.getCarnivores()[i];
		}
		for (int j=animals.length-this.getHerbivores().length; j < animals.length; ++j)
		{
			animals[j] = this.getHerbivores()[j];
		}
		return animals;
	}
	
	public Carnivore[] getCarnivores() {
		Carnivore[] carnivores = {};
		return this.carnivores.toArray(carnivores);
	}
	
	public Zone[] getZones() {
		Zone[] zones = {};
		return this.zones.toArray(zones);
	}
	
	public static FootStep[] getFootSteps() {
		FootStep[] footStep = {};
		return Environment.footStepList.toArray(footStep);
	}
	
	public static FootStepsList getFootStepList() {
		return Environment.footStepList;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public void createZone(int precision)
	{
		int zoneHeight=0, zoneWidth=0, windowHeight=this.getHeight(), windowWidth=this.getWidth();
		double x=0, y=0;
		
		while(y < windowHeight)
		{
			zoneHeight = precision;
			while(x < windowWidth)
			{
				zoneWidth = precision;
				this.add(new Zone(ZoneType.randomTypeZone(), new Position2D(x, y), zoneWidth, zoneHeight));
				x += zoneWidth;
			}
			x = 0;
			y += zoneHeight;
		}
	}
	
	public ArrayList<Herbivore> getTrueHerbivores()
	{
		return this.herbivores;
	}
	
	public void save() {
		// TODO Put in database package
		
		boolean stateBeforeSave = this.play;
		this.setPlay(false);
		
		Connexion c = new Connexion("database.db");
		c.cleanDatabase();
		
		ElementRequest reqE = new ElementRequest("database.db");
		BrainRequest reqB = new BrainRequest("database.db");
		NeuronRequest reqN = new NeuronRequest("database.db");
		InformationRequest reqI = new InformationRequest("database.db");
		HerbivoreRequest reqH = new HerbivoreRequest("database.db");
		reqE.connect();
		synchronized (this.elements) {
			for (Element e : elements) {
				reqE.saveElement(e);
			}
		}
		reqE.close();
		
		ZoneRequest reqZ = new ZoneRequest("database.db");
		reqZ.connect();
		synchronized (this.zones) {
			for (Zone z : zones) {
				reqZ.saveZone(z);
			}
		}
		reqZ.close();
		
		CarnivoreRequest reqC = new CarnivoreRequest("database.db");
		reqC.connect();
		synchronized (this.carnivores) {
			for (Carnivore carni : this.carnivores) {
				reqC.saveCarnivore(carni);
			}
		}
		reqC.close();
		
		synchronized (this.carnivores) {
			for (Carnivore ca : this.carnivores)
			{	
				for (Neuron n : ca.getBrain().getNeurons())
				{
					if (n.getInfo() != null)
					{
						reqI.connect();
						reqI.saveInformation(n.getInfo());
						reqI.close();
					}
					reqN.connect();
					reqN.saveNeuron(n);	
					reqN.close();
				}
				
				reqB.connect();
				reqB.saveBrain(ca.getBrain());
				reqB.close();
			}
		}
		
		
		reqH.connect();
		synchronized (this.herbivores) {
			for (Herbivore h : this.herbivores) {
				reqH.saveHerbivore(h);
			}
		}
		reqH.close();
		
		synchronized (this.herbivores) {
			for (Herbivore h : this.herbivores)
			{	
				for (Neuron n : h.getBrain().getNeurons())
				{
					if (n.getInfo() != null)
					{
						reqI.connect();
						reqI.saveInformation(n.getInfo());
						reqI.close();
					}
					
					reqN.connect();
					reqN.saveNeuron(n);	
					reqN.close();
				}
				
				reqB.connect();
				reqB.saveBrain(h.getBrain());
				reqB.close();
			}
		}
		
		VegetalRequest reqV = new VegetalRequest("database.db");
		reqV.connect();
		synchronized (this.vegetals) {
			for (Vegetal v : this.vegetals) {
				reqV.saveVegetal(v);
			}
		}
		reqV.close();
		
		System.out.println("Save done");
		this.setPlay(stateBeforeSave);
	}
	
	public void load() {
		// TODO Put in database package
		
		boolean stateBeforeLoad = this.play;
		this.setPlay(false);
		System.out.println("Loading");
		
		InformationRequest reqI = new InformationRequest("database.db");
		NeuronRequest reqN = new NeuronRequest("database.db");
		BrainRequest reqB = new BrainRequest("database.db");
		
		synchronized (this.zones) {
			this.zones.clear();
			ZoneRequest reqZ = new ZoneRequest("database.db");
			reqZ.connect();
			ResultSet resultSetZone = reqZ.query("SELECT * FROM Zone");
			try {
				while (resultSetZone.next()) {
					Zone zone = new Zone();
					zone.setId(resultSetZone.getInt("idElement"));
					this.zones.add(zone);
				}
				for(Zone z : this.zones)
				{
					reqZ.initialiseZone(z);
					System.out.println("Zone added");
				}
				reqZ.close();
				System.out.println("Zones loaded");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		synchronized (this.carnivores) {
			this.carnivores.clear();
			CarnivoreRequest reqC = new CarnivoreRequest("database.db");
			reqC.connect();
			ResultSet resultSetC = reqC.query("SELECT idElement FROM Carnivore");
			try {
				while (resultSetC.next()) {
					Carnivore c = new Carnivore(this);
					c.setId(resultSetC.getInt("idElement"));
					carnivores.add(c);
				}
				
				for (Carnivore c : this.carnivores)
				{
					reqC.initialiseCarnivore(c);
					System.out.println("Carnivore added");
				}
				reqC.close();
				System.out.println("Carnivores loaded");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			System.out.println("Initializating brains for carnivores");
			
			for (Carnivore c : this.carnivores)
			{
				for (Neuron n : c.getBrain().getNeurons())
				{
					if (n.getInfo() != null)
					{
						reqI.connect();
						Information i = new Information();
						i.setId(reqI.getIdInfoFromNeuron(n));
						reqI.initialiseInformation(i);
						reqI.close();
						n.setInfo(i);
					}
					
					reqN.connect();
					n.setBrain(c.getBrain());
					
					reqN.initialiseNeuron(n);
					reqN.close();
					c.getBrain().addNeuron(n);
				}
				
				reqB.connect();
				c.getBrain().setId(reqB.getIdInfoFromAnimal(c));
				c.getBrain().setAnimal(c);
				reqB.close();
			}
			System.out.println("Brains of Carnivore initialized");
		}	
		
		synchronized (this.herbivores) {
			this.herbivores.clear();
			HerbivoreRequest reqH = new HerbivoreRequest("database.db");
			reqH.connect();
			ResultSet resultSetH = reqH.query("SELECT * FROM Herbivore");
			try {
				while (resultSetH.next()) {
					Herbivore h = new Herbivore(this);
					h.setId(resultSetH.getInt("idElement"));
					this.herbivores.add(h);
				}
				for(Herbivore h : this.herbivores)
				{
					reqH.initialiseHerbivore(h);
					System.out.println("Herbivore added");
				}
				
				reqH.close();
				System.out.println("Herbivores loaded");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			for (Herbivore h : this.herbivores)
			{
				for (Neuron n : h.getBrain().getNeurons())
				{
					if (n.getInfo() != null)
					{
						reqI.connect();
						Information i = new Information();
						i.setId(reqI.getIdInfoFromNeuron(n));
						reqI.initialiseInformation(i);
						reqI.close();
						n.setInfo(i);
					}
					
					reqN.connect();
					n.setBrain(h.getBrain());
					reqN.initialiseNeuron(n);
					reqN.close();
					h.getBrain().addNeuron(n);
				}
				
				reqB.connect();
				h.getBrain().setId(reqB.getIdInfoFromAnimal(h));
				h.getBrain().setAnimal(h);
				reqB.close();
			}
		}
		
		synchronized (this.vegetals) {
			this.vegetals.clear();
			VegetalRequest reqV = new VegetalRequest("database.db");
			reqV.connect();
			ResultSet resultSetV = reqV.query("SELECT * FROM Vegetal");
			try {
				while (resultSetV.next()) {
					Vegetal v = new Vegetal(this);
					v.setId(resultSetV.getInt("idElement"));
					this.vegetals.add(v);
				}
				for(Vegetal v : this.vegetals)
				{
					reqV.initialiseVegetal(v);
					System.out.println("Vegetal added");
				}
				reqV.close();
				System.out.println("Vegetals loaded");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Successfully loaded");
		this.setPlay(stateBeforeLoad);
	}
	
	public boolean isPlay()
	{
		return this.play;
	}
	
	public void setPlay(boolean play)
	{
		this.play = play;
	}
	
	public void togglePlay()
	{
		this.play = !this.play;
	}
	
	@Override
	public void run()
	{
		long time = System.currentTimeMillis();
		
		while (true)
		{
			if (this.play)
			{
				// TODO split in multiple methods
				
				if (tmp.size() > 0) {
					for (Element e : tmp){
						this.add(e);
					}
					tmp.clear();
				}
				
				int size;
				
				size = this.herbivores.size();
				for (int i = 0; i < size; ++i)
				{
					if (herbivores.get(i).getShouldDisappear())
					{
						this.remove(herbivores.remove(i));
						--i;
						--size;
					}
				}
				
				size = this.carnivores.size();
				for (int i = 0; i  < size; ++i)
				{
					if (this.carnivores.get(i).getShouldDisappear())
					{
						this.remove(carnivores.remove(i));
						--i;
						--size;
					}
				}
				
				if (System.currentTimeMillis() - time > 10*this.timeBetweenThreadSleep)
				{
					time = System.currentTimeMillis();
					if (++this.counterTime > 10) {
						this.counterTime = 0;
						for (Herbivore herbivore : this.herbivores)
						{
							herbivore.live();
							if (herbivore.isAlive() && herbivore.getAge() < 18) {
								herbivore.grow();
							}
						}
						for (Carnivore carnivore : this.carnivores)
						{
							carnivore.live();
							if (carnivore.isAlive() && carnivore.getAge() < 18) {
								carnivore.grow();
							}
						}
					}
					for (Vegetal vegetal : this.vegetals) {
						vegetal.grow();
					}
				}
				
				if (++this.counterLive > 2)
				{
					this.counterLive = 0;
					for (Herbivore h : this.herbivores)
					{
						if (h.isAlive()) {
							h.live();
						}
					}
					for (Carnivore c : this.carnivores)
					{
						if (c.isAlive()) {
							c.live();
						}
					}
				}
				
				for (Vegetal vegetal : this.vegetals) {
					vegetal.live();
				}
				for (Herbivore herbivore : this.herbivores)
				{
					if (herbivore.isAlive()) {
						herbivore.run();
					}
				}
				for (Carnivore carnivore : this.carnivores)
				{
					if (carnivore.isAlive()) {
						carnivore.run();
					}
				}
				
				Environment.footStepList.run();
				
				this.doInRun();
			}
			
			try {
				Thread.sleep(this.timeBetweenThreadSleep);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * A method to override to do something at each change of the environment.
	 */
	public void doInRun()
	{}
	
	public void setTimeBetweenThreadSleep(short timeBetweenThreadSleep)
	{
		this.timeBetweenThreadSleep = timeBetweenThreadSleep;
		FootStepsList.setTimeToStay(timeBetweenThreadSleep*30);
	}
	
	public Element getOneElementNear(final Position2D aNearPosition)
	{
		for (Element e : this.elements)
		{
			if (e.getPosition().isNear(aNearPosition, 20)) {
				return e;
			}
		}
		return null;
	}
	
	public short getTimeBetweenThreadSleep()
	{
		return this.timeBetweenThreadSleep;
	}
}
