/*
 * Copyright (C) 2013, Thomas Geoffrey
 * Copyright (C) 2015, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package core.environment;

import core.living_being.Animal;
import core.living_being.LivingBeing;

/**
 * Get statistics of an environment.
 * @author Nicola Spanti, Thomas Geoffrey
 */
public final class EnvironmentStats
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private EnvironmentStats() {}
	
	
	public final static double averageSizeLivingBeings(final LivingBeing[] livingBeings)
	{
		if(livingBeings == null || livingBeings.length == 0) {
			return 0;
		}
		
		double averageSize = 0;
		for (LivingBeing element : livingBeings) {
			averageSize += element.size();
		}
		return averageSize / livingBeings.length;
	}
	
	public final static double averageSizeAnimals(final Environment env)
	{
		return env == null ? 0 : averageSizeLivingBeings(env.getAnimals());
	}
	
	public final static double averageSizeCarnivores(final Environment env)
	{
		return env == null ? 0 : averageSizeLivingBeings(env.getCarnivores());
	}
	
	public final static double averageSizeHerbivores(final Environment env)
	{
		return env == null ? 0 : averageSizeLivingBeings(env.getHerbivores());
	}
	
	
	public final static double averageAgeAnimals(final Animal[] animals)
	{
		if(animals == null || animals.length == 0) {
			return 0;
		}
		
		double averageAge = 0;
		for (Animal element : animals) {
			averageAge += element.getAge();
		}
		return averageAge / animals.length;
	}
	
	public final static double averageAgeAnimals(final Environment env)
	{
		return env == null ? 0 : averageAgeAnimals(env.getAnimals());
	}
	
	public final static double averageAgeCarnivores(final Environment env)
	{
		return env == null ? 0 : averageAgeAnimals(env.getCarnivores());
	}
	
	public final static double averageAgeHerbivores(final Environment env)
	{
		return env == null ? 0 : averageAgeAnimals(env.getHerbivores());
	}
	
	
	public final static double averageNbNeuronsAnimals(final Animal[] animals)
	{
		if(animals == null || animals.length == 0) {
			return 0;
		}
		
		double averageNbNeurons = 0;
		for (Animal element : animals) {
			averageNbNeurons += element.getBrain().getNeurons().size();
		}
		return averageNbNeurons / animals.length;
	}
	
	public final static double averageNbNeuronsAnimals(final Environment env)
	{
		return env == null ? 0 : averageNbNeuronsAnimals(env.getAnimals());
	}
	
	public final static double averageNbNeuronsCarnivores(final Environment env)
	{
		return env == null ? 0 : averageNbNeuronsAnimals(env.getCarnivores());
	}
	
	public final static double averageNeuronsHerbivores(final Environment env)
	{
		return env == null ? 0 : averageNbNeuronsAnimals(env.getHerbivores());
	}
}
