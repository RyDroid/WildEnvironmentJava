package core.environment;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import core.living_being.Carnivore;
import core.living_being.Herbivore;
import core.living_being.Vegetal;
import core.utils.Position2D;

public final class Tests {
	/**
	 * Don't let anyone instantiate this class.
	 */
	private Tests() {}
	
	
	@Test
	public void testZone() {
		new Zone(null, new Position2D(), 50, 50);
		new Zone(null, new Position2D(), 10);
		new Zone();
	}
	
	@Test
	public void testEnvironmentWithAnimals() {
		Environment env = new Environment();
		
		env.add(new Vegetal(env));
		env.add(new Herbivore(env));
		env.add(new Carnivore(env));
		
		assertEquals(env.getNbLivingBeings(), 3);
		
		int nbAnimals = 0;
		nbAnimals += env.getNbVegetals();
		nbAnimals += env.getNbAnimals();
		assertEquals(env.getNbLivingBeings(), nbAnimals);
		
		System.out.println(env);
	}
	
	@Test
	public void testEnvironmentWithZones() {
		Environment env = new Environment();
		env.add(new Zone());
		assertEquals(env.getNbZones(), 1);
	}
}
