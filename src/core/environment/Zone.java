package core.environment;

import java.util.Random;

import core.utils.Position2D;

/**
 * @author Nicola Spanti, Jason Labiod, Jérémy Bonnardel, Nicolas Foulon, Anthony Demongeot, Alexi Coard
 */
public class Zone extends Element
{
	private ZoneType type;
	private int quantity;
	private double width, height;
	private int temperature = 20;
	private int luminosity  = 70;
	private double hungerModificator = 1;
	private double thirstModificator = 1;
	private double speedModificator  = 1;
	
	public Zone(ZoneType type, Position2D position, double width, double height)
	{
		super(position);
		this.setWidth(width);
		this.setHeight(height);
		this.quantity = (int)(this.width*this.height);
		this.setType(type);
	}
	
	public Zone(ZoneType type, Position2D position, double size)
	{
		this(type, position, size, size);
	}
	
	public Zone(ZoneType type, Position2D position)
	{
		this(type, position, 30 /* TODO */);
	}
	
	public Zone(ZoneType type)
	{
		this(type, new Position2D()); // par défaut pour le moment
	}
	
	/**
	 * Default constructor that creates a random zone.
	 */
	public Zone()
	{
		this(ZoneType.randomTypeZone(), Position2D.getRandom(400.00,400.00), new Random().nextInt(100));
	}
	
	
	private void setType(ZoneType type)
	{
		if(type == null) {
			System.out.println(this.getClass().getSimpleName()+": zone type is null!");
		} else
		{
			this.type = type;
			
			switch (this.type)
			{
				case WATER:
					speedModificator = 0.7;
					luminosity =  80;
					break;
				case MOUNTAIN :
					speedModificator = 0.9;
					hungerModificator = 1.3;
					temperature =  5;
					luminosity =  70;
					break;
				case DESERT:
					speedModificator = 0.7;
					hungerModificator = 1.1;
					temperature = 40; luminosity = 100;
					break;
				case CAVERN:
					luminosity =  25;
					break;
				default :
					System.out.println(this.getClass().getSimpleName()+": type not managed ("+ this.type +")");
			}
		}
	}
	
	/**
	 * Returns the width of the zone.
	 * @return Width of the zone
	 */
	@Override
	public double getWidth()
	{
		return this.width;
	}
	
	/**
	 * Returns the height of the zone.
	 * @return Height of the zone
	 */
	@Override
	public double getHeight()
	{
		return this.height;
	}
	
	public void setWidth(double potentialNewWidth)
	{
		if (potentialNewWidth > 0) {
			this.width = potentialNewWidth;
		}
	}
	
	public void setHeight(double potentialNewHeight)
	{
		if (potentialNewHeight > 0) {
			this.height = potentialNewHeight;
		}
	}
	
	public double size()
	{
		return this.width * this.height;
	}
	
	public int getQuantity()
	{
		return this.quantity;
	}
	
	public boolean isDrinkable()
	{
		return this.quantity > 0;
	}
	
	public void reduceQuantity(int value)
	{
		this.quantity = this.quantity - value;
	}
	
	public ZoneType getZoneType()
	{
		return this.type;
	}
	
	public void setZoneType(ZoneType zoneType)
	{
		if(zoneType != null) {
			this.type = zoneType;
		}
	}
	
	@Override
	public String toString()
	{
		return this.type.getType() + " [pos=" + this.width + ";" + this.height + "]";
	}
}
