package core.environment;

public enum ZoneType
{
	CAVERN("Cavern"),
	DESERT("Desert"),
	MOUNTAIN("Mountain"),
	PLAIN("Plain"),
	WATER("Water");
	
	private String type;
	
	private ZoneType(String type)
	{
		this.type = type;
	}
	
	public String getType()
	{
		return this.type;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public static ZoneType randomTypeZone()
	{
		return values()[(int) (Math.random() * values().length)];
	}
}
