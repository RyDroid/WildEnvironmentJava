package core.living_being;

import java.util.ArrayList;
import java.util.Random;

import core.action.Action;
import core.action.Move;
import core.action.Sleep;
import core.action.Wait;
import core.environment.Element;
import core.environment.Environment;
import core.environment.Zone;
import core.living_being.brain.Brain;
import core.living_being.brain.Information;
import core.living_being.brain.InformationType;
import core.living_being.brain.Message;
import core.utils.Position2D;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon, Alexi Coard, Jeremy Bonnardel, Geoffrey Thomas
 */
abstract public class Animal extends LivingBeing implements Runnable
{
	private int health = 100, hunger = 0, thirst = 0; // plus thirst est élevée, plus l'animal à soif
	private double vision;
	private double actionRay;
	private double speed;
	private double strength;
	private Sex sex;
	private double tiredness = 0;
	private Brain brain;
	private Environment environment; // L'animal a accès à son environnement pour le visualiser par exemple, voir autour de lui
	private Message message;
	private int sociability = 0; /** caractère essentiel qui déterminent si oui ou non un animal voudra vivre en groupe ou parler avec un autre */
	private int age = 0;
	private Group group;
	private Behaviour behaviour;
	private int nbMaxNeurons = 10;
	private boolean shouldDisappear = false;
	private boolean canBePregnant = true;
	
	
	public Animal(
			final Environment environment, Position2D position, double size,
			int health, int hunger, int thirst, double vision, double speed, double strength, Sex sex, Brain brain)
	{
		super(position, size);
		Random r = new Random();
		this.environment = environment;
		this.environment.addTmp(this);
		this.actionRay = size * 1.5;
		this.health = health;
		this.hunger = hunger;
		this.thirst = thirst;
		this.vision = vision;
		this.speed = speed;
		this.strength = strength;
		this.sex = sex;
		this.brain = brain;
		this.brain.setAnimal(this);
		this.behaviour = initBehaviour(r.nextInt(5));
	}
	
	public Behaviour initBehaviour(int i)
	{
		switch (i)
		{
			case 0 : return Behaviour.AGRESSIVE;
			case 1 : return Behaviour.DEFENSIVE;
			case 2 : return Behaviour.FEARFUL;
			case 3 : return Behaviour.RECKLESS;
			case 4 : return Behaviour.SOCIABLE;
		}
		return null;
	}
	
	public Animal(
			final Environment env, Position2D position, double size,
			int health, int hunger, int thirst, double vision, double speed, double strength, Sex sex)
	{
		this(env, position, size, health, hunger, thirst, vision, speed, strength, sex, new Brain());
	}
	
	public Animal(
			final Environment env,
			int health, int hunger, int thirst,
			double vision, double speed, double strength, Sex sex)
	{
		this(env, new Position2D(), 5 , health, hunger, thirst, vision, speed, strength, sex, new Brain());
	}
	
	public Animal(
			final Environment env,
			int health, int hunger, int thirst,
			double vision, double speed, double strength)
	{
		this(env, health, hunger, thirst, vision, speed, strength, Sex.getRandom());
	}
	
	public Animal(final Environment env, int health, int hunger, int thirst)
	{
		this(env, health, hunger, thirst, 5, 5, 5);
	}
	
	public Animal(final Environment env)
	{	
		this(env, 100, 30, 30);
	}
	
	public Animal(final Environment env, Position2D position)
	{	
		this(env, position, 10, 100, 0, 0, 5, 5, 5, Sex.getRandom(), new Brain());
	}
	
	public Animal(final Animal mother, final Animal father)
	{
		this(mother.getEnvironment(), mother.getPosition());
		this.nbMaxNeurons = (mother.nbMaxNeurons + father.nbMaxNeurons)/2 + 1;
	}
	
	
	public int getNbMaxNeurons()
	{
		return this.nbMaxNeurons;
	}
	
	public double getActionRay()
	{
		return this.actionRay;
	}
	
	public void setActionRay(double actionRay)
	{
		this.actionRay = actionRay;
	}
	
	public void setEnv(Environment environment)
	{
		this.environment = environment;
	}
	
	public int getSociability()
	{
		return this.sociability;
	}
	
	public void setSociability(int sociability)
	{
		this.sociability = sociability;
	}
	
	public void setMessage(Message message)
	{
		this.message = message;
	}
	
	public Environment getEnvironment()
	{
		return this.environment;
	}
	
	public int getHealth()
	{
		return this.health;
	}
	
	public void setHealth(int health)
	{
		if (health < 0)
			this.health = 0;
		else if (this.health > 100)
			this.health = 100;
		else
			this.health = health;
	}
	
	/**
	 * @param health
	 */
	public void addHealth(int health)
	{
		this.setHealth(this.health + health);
	}
	
	public int getHunger()
	{
		return this.hunger;
	}
	
	public void setHunger(int hunger)
	{
		if (hunger < 0)
			this.hunger = 0;
		else if (this.hunger > 100)
			this.hunger = 100;
		else
			this.hunger = hunger;
	}
	
	public void addHunger(int hunger)
	{
		this.setHunger(this.hunger + hunger);
	}
	
	public int getThirst()
	{
		return this.thirst;
	}
	
	public void setThirst(int thirst)
	{
		if (this.thirst < 0)
			this.thirst = 0;
		else if (this.thirst > 100)
			this.thirst = 100;
		else
			this.thirst = thirst;
	}
	
	public void addThirst(int thrist)
	{
		this.setThirst(this.thirst + thrist);
	}
	
	public double getVision()
	{
		return this.vision;
	}
	
	public void setVision(double vision)
	{
		if (vision < 1)
			this.vision = 1;
		else
			this.vision = vision;
	}
	
	public void addVision(int vision)
	{
		this.setVision(this.vision + vision);
	}
	
	public double getSpeed()
	{
		return this.speed;
	}
	
	public void setSpeed(double speed)
	{
		this.speed = speed;
	}
	
	public double getStrength()
	{
		return this.strength;
	}
	
	public void setStrength(double strength)
	{
		this.strength = strength;
	}
	
	public Sex getSex()
	{
		return this.sex;
	}
	
	public final boolean isFemale()
	{
		return this.sex == Sex.FEMALE;
	}
	
	public final boolean isMale()
	{
		return this.sex == Sex.MALE;
	}
	
	/**
	 * @param sex of the animal
	 */
	public void setSex(Sex sex)
	{
		this.sex = sex;
	}
	
	/**
	 * @param sex of the animal
	 */
	public void setSex(char sex)
	{
		if(sex == 'm')
			this.sex = Sex.MALE;
		else if(sex == 'f')
			this.sex = Sex.FEMALE;
		else
			System.err.println("Animal: sex not recognized ("+sex+")");
	}
	
	/**
	 * @param sex of the animal
	 */
	public void setSex(String sex)
	{
		if(sex == null)
			System.err.println("Animal: sex can not be null");
		else
		{
			sex = sex.trim().toLowerCase();
			if(sex.length() == 0)
				System.err.println("Animal: sex can not be set with an empty string");
			else if(sex.length() == 1)
				this.setSex(sex.charAt(0));
			else if(sex == "male")
				this.sex = Sex.MALE;
			else if(sex == "female")
				this.sex = Sex.FEMALE;
			else
				System.err.println("Animal: sex not recognized ("+sex+")");
		}
	}
	
	public boolean isAlive()
	{
		return this.health > 0;
	}
	
	public abstract void birth(Animal father);
	
	
	public boolean isReproducable()
	{
		return this.age > 1 && this.age < 60 && this.canBePregnant;
	}
	
	@Override
	public void run()
	{
		this.brain.think();
	}
	
	
	@Override
	public String toString()
	{
		String res = "";
		
		String name = this.getClass().getSimpleName();
		res += name +":\n"+
				"  size\t= "  + this.size           + "\n"+
				"  health\t= "+ this.health         + "\n"+
				"  hunger\t= "+ this.hunger         + "\n"+
				"  thrist\t= "+ this.thirst         + "\n"+
				"  vision\t= "+ (float)this.vision  + "\n"+
				"  sex\t= "   + this.sex.toString() + "\n"+
				"  pos\t= "   + this.getPosition()  + "\n\n";
		
		if(this.isAlive())
			res += name + " is alive.\n";
		else
			res += name + " is dead.\n";
		
		if(this.isLeader())
			res += name + " is leader.\n";
		
		if(this.isHungry())
			res += name + " is hungry.\n";
		
		if(this.isThirsty())
			res += name + " is thirsty.\n";
		
		return res;
	}
	
	public Position2D computeNextPosition(Position2D pTarget)
	{
		double width  = this.getPosition().distanceXBetween(pTarget);
		double height = this.getPosition().distanceYBetween(pTarget);
		double coef = this.speed / this.getPosition().distanceBetween(width, height);
		return new Position2D(
				this.getPosition().getX() + width*coef,
				this.getPosition().getY() + height*coef
				);
	}
	
	public void move(Position2D pTarget)
	{
		Environment.getFootStepList().addAFootStep(this);
		this.setPosition(this.computeNextPosition(pTarget));
	}
	
	public Brain getBrain()
	{
		return this.brain;
	}
	
	public void setBrain(Brain brain)
	{
		this.brain = brain;
	}
	
	public boolean isNearToAct(Element e) {
		return this.getPosition().isNear(e.getPosition(), actionRay);
	}
	
	public boolean isNearToSee(Element e) {
		return this.getPosition().isNear(e.getPosition(), vision);
	}
	
	public void fireIsAttacked(Animal actor) {
		this.brain.alertAttacked(actor);
	}
	
	public boolean isHungry() {
		return this.hunger > 40;
	}
	
	public boolean isThirsty() {
		return this.thirst > 40;
	}
	
	public boolean isTired(){
		return this.tiredness > 40;
	}
	
	public ArrayList<Element> getVisibleThings() {
		ArrayList<Element> visibleThings = new ArrayList<>();
		for (Element element : this.getEnvironment().getElements())
		{
			if (element.isVisibleFor(this)){
				this.tryToMemorize(element);
				visibleThings.add(element);
			}
		}
		return visibleThings;
	}
	
	private void tryToMemorize(Element element) {
		Information info = null;
		if (element instanceof Animal) {
			Animal otherAnimal = (Animal) element;
			InformationType infoType;
			if (this instanceof Herbivore) {
				if (otherAnimal instanceof Herbivore) {
					infoType = InformationType.MATE;
				}
				else {
					infoType = InformationType.DANGER;
				}
			}
			else { // Pour le carnivore
				if (otherAnimal instanceof Carnivore) {
					infoType = InformationType.MATE;
				}
				else {
					infoType = InformationType.FOOD;
				}
			}
			info = new Information(infoType, otherAnimal, otherAnimal.getWidth()*otherAnimal.getHeight());
		}
		else if (element instanceof Vegetal) {
			Vegetal v = (Vegetal) element;
			if (this instanceof Herbivore)
				info = new Information(InformationType.FOOD, v, v.getWidth()*v.getHeight());
		}
		else if (element instanceof Zone) {
			Zone z = (Zone) element;
			info = new Information(InformationType.FOOD, z, z.getWidth()*z.getHeight());
		}
		this.brain.isAskToMemorize(info);
	}
	
	public Message getMessage() {
		return this.message;
	}
	
	public void fireIsSaid(Message message) {
		Action action = brain.getAction();
		if (action instanceof Wait || (action instanceof Move && message.getPriority() > 1) ||
				(!(action instanceof Sleep) && message.getPriority() > 2) ||
				message.getPriority() > 3 )
		{
			this.brain.analyseMessage(message);
		}
	}
	
	public ArrayList<Animal> getVisibleSameSpeceAnimals() {
		ArrayList<Animal> visibleSameSpeceAnimals = new ArrayList<>();
		for (Element element : this.getVisibleThings()) {
			if(element.getClass().isAssignableFrom(this.getClass()))
				visibleSameSpeceAnimals.add((Animal) element);
		}
		return visibleSameSpeceAnimals;
	}
	
	public void fireSomethingIsComing(Animal actor) {
		this.brain.analyseDanger(actor);
	}
	
	public void fireActionPerformed() {
		this.brain.resetWillingAction();
	}
	
	public int getAge() {
		return this.age;
	}
	
	@Override
	public void grow() {
		super.grow();
		++this.age;
		this.canBePregnant = true;
	}
	
	public void unPregnantable() {
		this.canBePregnant = false;
	}
	
	
	@Override
	public void live() {
		if(this.isAlive())
		{
			this.addHunger(1);
			this.addThirst(1);
			this.addTiredNess(0.3);
			if (this instanceof Carnivore) {
				if (this.hunger > 93)
					this.addHealth(-1);
				else if (this.hunger < 10)
					this.addHealth(5);
			}
			else {
				if (hunger > 80 || thirst > 80)
					this.addHealth(-1);
				else if (hunger < 50)
					this.addHealth(10);
			}
			if (this.age > 100)
				this.setHealth(0); // mort de vieillesse
		}
	}
	
	public void addTiredNess(double d) {
		this.setTiredness(this.tiredness + d);
	}
	
	private void setTiredness(double d) {
		this.tiredness = (d > 100) ? 100 : (d < 0) ? 0 : d;
	}
	
	public double getTiredNess() {
		return this.tiredness;
	}
	
	public boolean isLeader() {
		return this.hasAGroup() && this == this.group.getLeader();
	}
	
	public boolean hasAGroup() {
		return this.group != null;
	}
	
	public Group getGroup() {
		Group g = null;
		if (hasAGroup())
			g = group;
		return g;
	}
	
	public void removeGroup() {
		this.group = null;
	}
	
	public void setGroup(Group group) {
		this.group = group;
	}
	
	public void fireGroupRequest(Animal animal) {
		if (!this.hasAGroup())
			this.group = new Group(this);
		this.group.add(animal);
	}
	
	public boolean seemsWeakerThan(Animal a) {
		return a.health*a.strength > health*strength;
	}
	
	public void transmit(Message message) {
	}
	
	public Position2D getAPosFarFrom(Position2D p) {
		Position2D choice = null;
		
		ArrayList<Element> elements = this.getVisibleThings();
		double distMax = 0;
		double dist;
		for (Element element : elements)
		{
			dist = p.distanceBetween(element.getPosition());
			if (dist > distMax)
			{
				distMax = dist;
				choice = element.getPosition();
			}
		}
		
		if (choice == null)
			choice = getAMemoryPosFarFrom(p);
		return choice;
	}
	
	private Position2D getAMemoryPosFarFrom(Position2D p) {
		Position2D choice = null;
		ArrayList<Information> infos = this.getMemoryThings();
		double distMax = 0;
		double dist;
		for (Information i : infos) {
			dist = p.distanceBetween(i.getPosition());
			if (dist > distMax)
			{
				distMax = dist;
				choice = i.getPosition();
			}
		}
		return choice;
	}
	
	private ArrayList<Information> getMemoryThings() {
		return this.brain.getInformations();
	}
	
	public Behaviour getBehaviour() {
		return this.behaviour;
	}
	
	public void setBehaviour(Behaviour behaviour) {
		this.behaviour = behaviour;
	}
	
	public double getTiredness() {
		return this.tiredness;
	}
	
	/**
	 * Give an order to his group.
	 * @param Action action
	 * @param Element target
	 */
	public void order(Action action, Element target) {
		this.group.order(action, target);
	}
	
	public void broadcast(Information info) {
		this.group.memorize(info);
	}
	
	public void setShouldDisappear(boolean b) {
		this.shouldDisappear = b;
	}
	
	public boolean getShouldDisappear() {
		return this.shouldDisappear;
	}
}