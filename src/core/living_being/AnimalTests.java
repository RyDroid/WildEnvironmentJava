package core.living_being;

import org.junit.Test;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Alexi Coard
 */
public final class AnimalTests {
	/**
	 * Don't let anyone instantiate this class.
	 */
	private AnimalTests() {}
	
	
	@Test
	public void testConstructors() {
		Vegetal vegetable = new Vegetal(null);
		Herbivore herbivore = new Herbivore(null);
		Carnivore carnivore = new Carnivore(null);
		
		System.out.println(vegetable);
		System.out.println(herbivore);
		System.out.println(carnivore);
	}
	
	@Test
	public void testHealth() {
		Animal animal = new Carnivore(null);
		System.out.println(animal);
		animal.addHealth(15);
		System.out.println(animal);
		animal.addHealth(-50);
		System.out.println(animal);
		System.out.println("L'animal est " +(animal.isAlive() ? "vivant" : "mort") +".");
	}
}
