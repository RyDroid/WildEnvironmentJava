package core.living_being;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon
 */
public enum Behaviour {
	AGRESSIVE("AGRESSIVE"), 
	DEFENSIVE("DEFENSIVE"), 
	FEARFUL("FEARFUL"), 
	RECKLESS("RECKLESS"), 
	SOCIABLE("RECKLESS");
	
	private final String name;
	
	private Behaviour(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
