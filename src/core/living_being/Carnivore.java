package core.living_being;

import core.environment.Environment;
import core.utils.Position2D;

public class Carnivore extends Animal
{
	public Carnivore(final Environment env)
	{
		super(env);
		super.setStrength(30);
		this.setSpeed(7);
		this.setSociability(10);
		this.setSex(Sex.getRandom());
	}
	
	public Carnivore(final Environment env, final Position2D p)
	{
		super(env, p);
		super.setStrength(30);
		this.setSpeed(6);
		this.setSociability(10);
		this.setSex(Sex.getRandom());
	}
	
	public Carnivore(final Carnivore mother, final Carnivore father)
	{
		super(mother, father);
		super.setStrength(30);
		this.setSpeed(6);
		this.setSociability(70);
		this.setSex(Sex.getRandom());
	}
	
	@Override
	public void birth(final Animal father)
	{
		if (this.isReproducable() && !this.isMale())
		{
			Carnivore daddy = (Carnivore) father;
			new Carnivore(this, daddy);
		}
	}
}
