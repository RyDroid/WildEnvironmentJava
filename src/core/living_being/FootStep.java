package core.living_being;

import core.utils.Position2D;

/**
 * A foot step of an animal
 * @author Labiod Jason, Nicola Spanti (RyDroid)
 */
public class FootStep {
	private double x;
	private double y;
	private long time;
	private int size = 1;
	private boolean fromCarnivore;
	
	
	public FootStep(double x, double y, boolean fromCarnivore) {
		this.x = x;
		this.y = y;
		this.time = System.currentTimeMillis();
		this.fromCarnivore = fromCarnivore;
	}
	
	public FootStep(double x, double y, int size, boolean fromCarnivore) {
		this(x, y, fromCarnivore);
		this.size = size;
	}
	
	public FootStep(Position2D position, boolean fromCarnivore) {
		this(position.getX(), position.getY(), fromCarnivore);
	}
	
	public FootStep(Position2D position, int size, boolean fromCarnivore) {
		this(position.getX(), position.getY(), size, fromCarnivore);
	}
	
	
	public boolean isACarnivorStep() {
		return this.fromCarnivore;
	}
	
	public long getTime() {
		return this.time;
	}
	
	/**
	 * Returns the size of the foot step
	 * @return the size of the foot step
	 */
	public int size() {
		return this.size;
	}
	
	/**
	 * Returns the x position
	 * @return the x position
	 */
	public int getX() {
		return (int) this.x;
	}
	
	/**
	 * Returns the y position
	 * @return the y position
	 */
	public int getY() {
		return (int) this.y;
	}
}
