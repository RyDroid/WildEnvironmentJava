package core.living_being;

import java.util.ArrayList;

import core.utils.Position2D;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid)
 */
@SuppressWarnings("serial")
public class FootStepsList extends ArrayList<FootStep> implements Runnable {
	private static long timeToStay = 1000;
	
	public void addAFootStep(Animal animal)
	{
		Position2D p = animal.getPosition();
		super.add(new FootStep(p.getX()+animal.size()/2,
				p.getY()+animal.size()/2, (int) (animal.size()/5),
				animal instanceof Carnivore)
				);
	}
	
	
	@Override
	public void run()
	{
		int size = super.size();
		int i = 0;
		FootStep footStep;
		while (i < size)
		{
			footStep = super.get(i);
			if (System.currentTimeMillis() > footStep.getTime() + FootStepsList.timeToStay) {
				super.remove(i);
				--size;
			}
			else {
				++i;
			}
		}
	}
	
	public static void setTimeToStay(long timeToStay) {
		FootStepsList.timeToStay = timeToStay;
	}
}