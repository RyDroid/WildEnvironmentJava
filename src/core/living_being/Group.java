package core.living_being;

import java.util.LinkedList;

import core.action.Action;
import core.environment.Element;
import core.living_being.brain.Information;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon
 */
public class Group {
	private Animal leader;
	private LinkedList<Animal> animals;
	
	
	public Group(Animal leader, LinkedList<Animal> animals) {
		this.leader = leader;
		
		if(animals == null)
			this.animals = new LinkedList<Animal>();
		else
			this.animals = animals;
	}
	
	public Group(Animal leader) {
		this.leader = leader;
		this.animals = new LinkedList<Animal>();
	}
	
	
	/**
	 * Ajouter un animal au groupe
	 * @param a : l'animal à ajouter
	 */
	public void add(Animal a) {
		this.animals.add(a);
		a.setGroup(this);
	}
	
	/**
	 * Retirer un animal du groupe
	 * @param animal : l'animal à retirer
	 */
	public void remove(Animal animal) {
		this.animals.remove(animal);
		animal.removeGroup();
		if (this.leader == animal)
			this.leader = this.election();
	}
	
	// To choose a new leader
	private Animal election() {
		Animal selected = null;
		if (animals.size() > 0){
			int maxValue = 0;
			int value;
			for (Animal a : animals){
				value = (int) ((a.getStrength()*a.getHealth())/a.getAge());
				if (value > maxValue){
					maxValue = value;
					selected = a;
				}
			}
		}
		return selected;
	}
	
	/**
	 * Dissoudre un groupe.
	 */
	private void delete() {
		this.leader = null;
		for (Animal a : this.animals)
			a.removeGroup();
		this.animals.clear();
	}
	
	/**
	 * Fusionner deux groupes, seul le leader peut décider de ça
	 */
	public void fusion(Animal a) {
		if (a.isLeader())
			this.fusion(a.getGroup());
	}
	
	/**
	 * Fusionner deux groupes.
	 * @param group
	 */
	private void fusion(Group group) {
		for (Animal a : group.animals)
			this.animals.add(a);
		
		group.delete();
	}
	
	public Animal getLeader() {
		return this.leader;
	}
	
	public void order(Action action, Element target) {
		for (Animal a : this.animals)
			a.getBrain().isOrdered(action, target);
	}
	
	public void memorize(Information info) {
		for (Animal a : this.animals)
			a.getBrain().isAskToMemorize(info);
	}
}