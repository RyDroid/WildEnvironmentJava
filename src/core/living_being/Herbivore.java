package core.living_being;

import core.environment.Environment;
import core.utils.Position2D;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon, Alexi Coard
 */
public class Herbivore extends Animal {
	public final static int DEFAULT_SOCIABILITY = 80;
	
	public Herbivore(Environment env) {
		super(env);
		this.setSociability(DEFAULT_SOCIABILITY);
		this.setSex(Sex.getRandom());
	}
	
	public Herbivore(Environment env, Position2D p) {
		super(env, p);
		this.setSociability(DEFAULT_SOCIABILITY);
		this.setSex(Sex.getRandom());
	}
	
	public Herbivore(Herbivore mother, Herbivore father) {
		super(mother, father);
		this.setSociability(DEFAULT_SOCIABILITY);
		this.setSex(Sex.getRandom());
	}
	
	@Override
	public void birth(Animal father) {
		if (this.isReproducable() && !this.isMale()) {
			Herbivore daddy = (Herbivore) father;
			new Herbivore(this, daddy);
		}
	}
}
