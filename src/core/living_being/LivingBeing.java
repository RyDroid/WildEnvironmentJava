package core.living_being;

import core.environment.Element;
import core.utils.Position2D;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Alexi Coard
 */
abstract public class LivingBeing extends Element implements Runnable
{
	public final static double SIZE_MAX = 50;
	
	protected double size;
	
	abstract void live();
	
	
	public LivingBeing(final Position2D position, double size)
	{
		super(position);
		this.setSize(size);
	}
	
	public LivingBeing(Position2D position)
	{
		this();
		this.setPosition(position);
	}
	
	public LivingBeing(double size)
	{
		this(new Position2D(), size);
	}
	
	public LivingBeing()
	{
		this(10); // TODO par default pour les tests graphiques
	}
	
	
	public double size()
	{
		return this.size;
	}
	
	@Override
	public double getWidth()
	{
		return this.size;
	}
	
	@Override
	public double getHeight()
	{
		return this.size;
	}
	
	public void setSize(double size)
	{
		if(size < 0)
			this.size = 0;
		else
			this.size = size;
	}
	
	public void addToSize(double size)
	{
		this.setSize(this.size +size);
	}
	
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj) || !(obj instanceof LivingBeing))
			return false;
		LivingBeing other = (LivingBeing) obj;
		return this.size == other.size;
	}
	
	public void grow()
	{
		if (this.size < SIZE_MAX)
			++this.size;
	}
}
