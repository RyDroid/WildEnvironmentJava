/*
 * Copyright (C) 2015, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package core.living_being;

import java.util.Random;

public enum Sex
{
	FEMALE,
	MALE,
	UNKOWNN;
	
	
	public String getSexCharacter()
	{
		if(this == FEMALE)
			return "f";
		if(this == MALE)
			return "m";
		return "u";
	}
	
	@Override
	public String toString()
	{
		if(this == FEMALE)
			return "female";
		if(this == MALE)
			return "male";
		return "unknownSex";
	}
	
	public static Sex getRandom()
	{
		int i = (new Random()).nextInt(2);
		return i == 0 ? FEMALE : MALE;
	}
}
