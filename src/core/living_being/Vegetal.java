package core.living_being;

import core.environment.Environment;
import core.utils.Position2D;

public class Vegetal extends LivingBeing {
	public Vegetal(final Environment env) {
		super();
		env.add(this);
	}
	
	public Vegetal(final Environment env, final Position2D p) {
		super(p);
		env.add(this);
	}
	
	
	@Override
	public String toString() {
		return "Vegetable\n size\t= "+ this.size;
	}
	
	@Override
	public void run()
	{}
	
	@Override
	public void live()
	{}
}
