package core.living_being.brain;

import java.util.ArrayList;
import java.util.Random;

import core.action.Action;
import core.action.Attack;
import core.action.Drink;
import core.action.Eat;
import core.action.GroupRequest;
import core.action.Move;
import core.action.Reproduce;
import core.action.Sleep;
import core.action.Tell;
import core.action.Wait;
import core.environment.Element;
import core.environment.Zone;
import core.environment.ZoneType;
import core.living_being.Animal;
import core.living_being.Behaviour;
import core.living_being.Carnivore;
import core.living_being.Herbivore;
import core.living_being.LivingBeing;
import core.living_being.Vegetal;
import data_management.database.Connexion;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon, Geoffrey Thomas, Alexi Coard
 */
public class Brain {
	protected static int id = 0;
	
	// Permet de ne pas créer une action à chaque fois, les types d'actions sont
	// partagés par l'ensemble des êtres possédant un cerveau,
	// Exemple : créer une action Drink à chaque fois qu'un animal à soif est en
	// effet maladroit
	private static Action
	eat = new Eat(),
	attack = new Attack(),
	drink = new Drink(),
	group = new GroupRequest(),
	move = new Move(),
	reproduce = new Reproduce(),
	sleep = new Sleep(),
	tell = new Tell(),
	wait = new Wait();
	
	private int num;
	private ArrayList<Neuron> neurons;
	private Animal animal;
	private Action action;
	private Action willingAction = wait;
	private Element target;
	protected Connexion connection = new Connexion("database.db");
	private Message message;
	
	public Brain(ArrayList<Neuron> neurons) {
		this.num = ++Brain.id;
		this.neurons = neurons;
	}
	
	public Brain() {
		this(new ArrayList<Neuron>());
	}
	
	public void addNeuron(Neuron neuronToAdd) {
		this.neurons.add(neuronToAdd);
		neuronToAdd.updateDate();
		
		for (Neuron neuron : this.neurons) {
			neuron.link(neuronToAdd);
		}
	}
	
	public int getId() {
		return this.num;
	}
	
	public void setId(int id) {
		this.num = id;
	}
	
	public ArrayList<Neuron> getNeurons() {
		return this.neurons;
	}
	
	public void setNeurons(ArrayList<Neuron> neurons) {
		this.neurons = neurons;
	}
	
	public Animal getAnimal() {
		return this.animal;
	}
	
	public void setAnimal(Animal animal) {
		this.animal = animal;
		for (int i = animal.getNbMaxNeurons(); i > 0; --i) {
			this.neurons.add(new Neuron(this));
		}
	}
	
	public void updateNeuron(Neuron neuronToUpdate, Information info) {
		if (neuronToUpdate.getInfo() == null
				|| neuronToUpdate.getInfo().isAboutTheSameElement(info)
				|| info.isMoreImportantThan(neuronToUpdate.getInfo())) {
			// neuronToUpdate.setInfo(info); // TODO manage that without problem...
			neuronToUpdate.updateDate();
			neuronToUpdate.remove();
			for (Neuron neuron : this.neurons) {
				if (neuron != neuronToUpdate)
					neuron.link(neuronToUpdate);
			}
		}
	}
	
	public void removeNeuron(Neuron neuron) {
		neuron.remove();
		this.neurons.remove(neuron);
	}
	
	public void think() {
		if (this.willingAction == wait || this.target == null) {
			this.selectATarget();
			if (target == null) {
				willingAction = wait;
			}
		}
		
		if (this.willingAction != wait && this.willingAction != sleep) {
			if (!animal.isNearToAct(this.target)) {
				this.action = Brain.move;
			} else if (this.animal instanceof Carnivore) {
				if (this.willingAction == eat
						&& target instanceof Herbivore) {
					Herbivore t = (Herbivore) this.target;
					if (t.isAlive()) {
						this.action = attack;
						this.alertAttacked(t);
					} else {
						this.action = willingAction;
					}
				} else {
					this.action = willingAction;
				}
			} else if (this.animal instanceof Herbivore) {
				if (this.target instanceof Herbivore) {
					Herbivore t = (Herbivore) this.target;
					if (t.getHealth() < 10) {
						this.action = tell;
						this.analyseMessage(t.getMessage());
					} else
						this.action = willingAction;
				} else {
					this.action = willingAction;
				}
			}
		}
		else {
			this.action = wait;
		}
		
		this.action.act(animal, target);
	}
	
	public void selectATarget() {
		this.willingAction = this.analyseSelfState();
		if (this.willingAction == Brain.eat) { // Autre gros avantage des
			// actions
			// statique, l'utilisation du ==
			// est correcte !
			this.selectEatable();
		} else if (this.willingAction == Brain.drink) {
			this.selectDrinkable();
		} else if (this.willingAction == Brain.group) {
			System.out.println("L'animal a envie de grouper");
			this.selectGroupable();
		} else if (this.willingAction == Brain.reproduce) {
			System.out.println("L'animal a envie de Djodjo");
			this.selectReproducable();
		}
	}
	
	private void selectDrinkable() {
		ArrayList<Element> choices = new ArrayList<>();
		
		for (Element element : this.animal.getVisibleThings()) {
			if (element instanceof Zone) {
				if (((Zone) element).getZoneType() == ZoneType.WATER)
					choices.add(element);
			}
		}
		this.target = this.computeBestCaseDrinkable(choices);
	}
	
	private void selectReproducable() {
		ArrayList<Element> choices = new ArrayList<>();
		
		for (Element element : this.animal.getVisibleSameSpeceAnimals()) {
			if (((Animal) element).getSex() != this.animal.getSex()
					&& ((Animal) element).isReproducable())
				choices.add(element);
		}
		System.out.println("YoloReproduce");
		this.target = this.computeBestCaseReproducable(choices);
	}
	
	private Element computeBestCaseReproducable(ArrayList<Element> choices) {
		Element choice = null;
		double bestValue = 0;
		double value = 0;
		for (Element element : choices) {
			Animal a = (Animal) element;
			value = 1 / animal.distanceBetween(a);
			if (value > bestValue) {
				bestValue = value;
				choice = element;
			}
		}
		return choice;
	}
	
	private void selectGroupable() {
		System.out.println("YoloGroup"); // TODO delete?
		ArrayList<Element> choices = new ArrayList<>();
		for (Element element : this.animal.getVisibleSameSpeceAnimals())
			choices.add(element);
		this.target = this.computeBestCaseGroupable(choices);
	}
	
	private Element computeBestCaseGroupable(ArrayList<Element> choices) {
		Element choice = null;
		double bestValue = 0;
		double value = 0;
		for (Element element : choices) {
			Animal a = (Animal) element;
			value = 1 / animal.distanceBetween(a);
			if (value > bestValue) {
				bestValue = value;
				choice = element;
			}
		}
		System.out.println("YoloGroupCompute"); // TODO delete?
		return choice;
	}
	
	private Element computeBestCaseDrinkable(ArrayList<Element> choices) {
		Element choice = null;
		double bestValue = 0;
		double value = 0;
		for (Element element : choices) {
			Zone z = (Zone) element;
			value = 1 / animal.distanceBetween(z);
			if (value > bestValue) {
				bestValue = value;
				choice = element;
			}
		}
		return choice;
	}
	
	public void resetWillingAction() {
		this.willingAction = wait;
	}
	
	private void selectEatable() {
		ArrayList<Element> choices = new ArrayList<>();
		if (animal instanceof Carnivore) {
			for (Element element : animal.getVisibleThings()) {
				if (element instanceof Herbivore) {
					Herbivore herbivore = (Herbivore) element;
					if (herbivore.size() > 0)
						choices.add(herbivore);
				}
				/*else if (element != this.animal && animal.getEnvironment().getTrueHerbivores().size() == 0){
					Carnivore carnivore = (Carnivore) element;
					if (carnivore.size() > 0)
						choices.add(carnivore);
				}*/
			}
		} else if (this.animal instanceof Herbivore) {
			for (Element element : this.animal.getVisibleThings()) {
				if (element instanceof Vegetal) {
					Vegetal vegetal = (Vegetal) element;
					if (vegetal.size() > 0)
						choices.add(vegetal);
				}
			}
		}
		this.target = this.computeBestCaseEatable(choices);
	}
	
	private Element computeBestCaseEatable(ArrayList<Element> choices) {
		Element choice = null;
		double bestValue = 0;
		double value = 0;
		for (Element element : choices) {
			LivingBeing lb = (LivingBeing) element;
			value = lb.size() / this.animal.distanceBetween(lb);
			if (value > bestValue && lb.size() != 0) {
				bestValue = value;
				choice = element;
			}
		}
		return choice;
	}
	
	private Action analyseSelfState() {
		Random r = new Random();
		Behaviour b = this.animal.getBehaviour();
		int randomValue = r.nextInt(100);
		int hungerValue = (this.animal.getHunger() + r.nextInt(50)) / 2;
		if (this.animal instanceof Carnivore
				&& b == Behaviour.AGRESSIVE)
			hungerValue += 5 + r.nextInt(6);
		if (this.animal instanceof Herbivore
				&& !animal.hasAGroup()
				&& (b == Behaviour.DEFENSIVE || b == Behaviour.FEARFUL))
			hungerValue -= 5 + r.nextInt(6);
		if (randomValue < hungerValue)
			return eat;
		
		randomValue = r.nextInt(100);
		int thirstValue = (this.animal.getThirst() + r.nextInt(30)) / 2;
		if (this.animal instanceof Herbivore
				&& !animal.hasAGroup()
				&& (b == Behaviour.DEFENSIVE || b == Behaviour.FEARFUL))
			thirstValue -= 5 + r.nextInt(6);
		if (randomValue < thirstValue)
			return drink;
		
		randomValue = r.nextInt(100);
		//int groupValue = r.nextInt(50);
		/*
		 * if ( !this.animal.hasAGroup()) { switch (b) { case DEFENSIVE :
		 * groupValue += 15; break; case FEARFUL : groupValue += 40; break; case
		 * SOCIABLE : groupValue += 60; break; case RECKLESS : groupValue -= 25;
		 * break; case AGRESSIVE : groupValue -= 25; break; default: break; } if
		 * (randomValue < groupValue) return group; }
		 */
		
		randomValue = r.nextInt(10);
		int sleepValue = (int) animal.getTiredness() + r.nextInt(20);
		if (this.animal instanceof Herbivore
				&& !animal.hasAGroup()
				&& (b == Behaviour.DEFENSIVE || b == Behaviour.FEARFUL))
			sleepValue -= 5 + r.nextInt(6);
		if (randomValue < sleepValue)
			return sleep;
		
		if (this.animal.isReproducable()) {
			randomValue = r.nextInt(50);
			int reproduceValue = r.nextInt(100);
			switch (b) {
				case DEFENSIVE:
					reproduceValue += -5;
					break;
				case FEARFUL:
					reproduceValue += -15;
					break;
				case SOCIABLE:
					reproduceValue += 20;
					break;
				case RECKLESS:
					reproduceValue += 5;
					break;
				case AGRESSIVE:
					reproduceValue -= 20;
					break;
			}
			if (randomValue < reproduceValue) {
				return reproduce;
			}
		}
		return wait;
	}
	
	public void alertAttacked(Animal actor) {
		/*
		 * if (this.animal.hasAGroup()){ this.message = new Message(3, attack,
		 * actor); // Hey les mecs, défendez moi ! T-T this.willingAction =
		 * tell; this.target = this.animal.getGroup().getLeader(); } else if
		 * (actor.seemsWeakerThan(this.animal) && ! actor.hasAGroup()){ // S'il
		 * a l'air faible et sans groupe, on lui casse la gueule !
		 * this.willingAction = attack; this.target = actor; } else{
		 * this.message = new Message(3, attack, actor); // Hey les mecs,
		 * défendez moi ! T-T this.willingAction = tell; ArrayList<Animal>
		 * buddies = this.animal.getVisibleSameSpeceAnimals(); if
		 * (buddies.size() > 0){ this.target = buddies.get(0); } else {
		 * this.willingAction = move; this.target =
		 * this.animal.getEnvironment().
		 * getOneElementNear(this.animal.getAPosFarFrom(actor.getPosition())); }
		 * }
		 */// TODO manage groups for that !
		if (this.animal instanceof Herbivore) {
			Random r = new Random();
			int value = r.nextInt(100);
			switch (animal.getBehaviour()) {
				case DEFENSIVE:
					value += 10;
					break;
				case FEARFUL:
					value += 20;
					break;
				case SOCIABLE:
					value += 5;
					break;
				case RECKLESS:
					value -= 20;
					break;
				case AGRESSIVE:
					value -= 50;
					break;
			}
			if (value < 20) {
				this.willingAction = attack;
				this.target = actor;
			} else {
				this.willingAction = move;
				this.target = this.animal.getEnvironment().getOneElementNear(
						this.animal.getAPosFarFrom(actor.getPosition()));
			}
		}
	}
	
	public ArrayList<Information> getInformations() {
		ArrayList<Information> infos = new ArrayList<>();
		for (Neuron n : neurons) {
			infos.add(n.getInfo());
		}
		return infos;
	}
	
	public void analyseMessage(Message message) {
		/*
		 * Priorité 1 : N'est analysée que si l'animal ne fait rien Priorité 2 :
		 * Est analysée même en déplacement Priorité 3 : Est analysée même en
		 * action Priorité 4 : Est analysée même en sommeil, stop le sommeil
		 */
		Action a = message.getAction();
		Information i = message.getInformation();
		Element target = message.getAboutWhat();
		if (willingSay()) {
			if (a != null && target != null) {
				if (this.animal.hasAGroup()) {
					if (this.animal.isLeader()) {
						this.animal.order(a, target);
					} else {
						this.message = message;
						this.willingAction = tell;
						this.target = this.animal.getGroup().getLeader();
					}
				} else {
					this.willingAction = a;
					this.target = target;
				}
			}
			if (i != null) {
				if (this.animal.isLeader()) {
					this.animal.broadcast(i);
				} else {
					this.message = message;
					this.willingAction = tell;
					this.target = this.animal.getGroup().getLeader();
				}
			}
		}
	}
	
	private boolean willingSay() {
		int value = this.animal.getSociability();
		switch (this.animal.getBehaviour()) {
			case DEFENSIVE:
				value -= 5;
				break;
			case FEARFUL:
				value -= 10;
				break;
			case SOCIABLE:
				value += 5;
				break;
			case RECKLESS:
				value += 15;
				break;
			case AGRESSIVE:
				value -= 20;
		}
		return new Random().nextInt(100) < value;
	}
	
	public void analyseDanger(Animal actor) {
		if (this.animal instanceof Herbivore
				&& actor instanceof Carnivore)
		{
			this.alertAttacked(actor);
		}
	}
	
	public void setTarget(Element element) {
		this.target = element;
	}
	
	public Action getAction() {
		return action;
	}
	
	public void isOrdered(Action action2, Element target2) {
		this.willingAction = action2;
		this.target = target2;
	}
	
	public void isAskToMemorize(Information i) {
		Neuron choice = null;
		double minValue = Double.MAX_VALUE;
		double value;
		Neuron matchingNeuron = null;
		for (Neuron n : neurons) {
			if (n.getInfo() == null) {
				value = 0;
			} else {
				value = n.getInfo().getWeightedValue();
				if (n.getInfo().isAboutTheSameElement(i))
					matchingNeuron = n;
			}
			
			if (value < minValue) {
				minValue = value;
				choice = n;
			}
		}
		
		this.updateNeuron(matchingNeuron == null ? choice : matchingNeuron, i);
	}
}
