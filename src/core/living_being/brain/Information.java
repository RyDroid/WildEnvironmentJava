package core.living_being.brain;

import core.environment.Element;
import core.utils.Position2D;
import data_management.database.Connexion;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon, Alexi Coard
 */
public class Information {
	protected static int id = 0;
	
	private int num;
	private InformationType infoType;
	private Element element;
	private Position2D position;
	private double value;
	private double weightedValue;
	protected Connexion connection = new Connexion("database.db");
	
	/**
	 * Constructor
	 * @param infoType
	 * @param pos
	 * @param value
	 */
	public Information(InformationType infoType, Element elt, double value) {
		this.num = ++Information.id;
		this.infoType = infoType;
		this.element = elt;
		this.position = elt.getPosition();
		this.value = value;
		this.weightedValue = value;
		switch (infoType)
		{
			case DANGER:
				this.weightedValue *= -1;
				break;
			case WATER:
				this.weightedValue *= 0.5;
				break;
			case MATE:
				this.weightedValue *= 0.8;
			case ALERT:
				this.weightedValue *= -3;
				break;
		}
	}
	
	public Information(Information i) {
		this(i.getInfoType(), i.getElement(), i.getValue());
	}
	
	/**
	 * Default constructor.
	 */
	public Information()
	{
		this.num = 0;
		this.infoType = null;
		this.position = null;
		this.value = 0;
	}
	
	/**
	 * Return the weighted value of an information.
	 */
	public double getWeightedValue() {
		return this.weightedValue;
	}
	
	
	// Getters and setters
	/*
	 * public InformationType getInfoType() { return this.infoType; } public
	 * void setInfoType(InformationType infoType) { this.infoType = infoType; }
	 */
	
	
	//Getters and setters
	/*public InformationType getInfoType() {
		return this.infoType;
	}
	public void setInfoType(InformationType infoType) {
		this.infoType = infoType;
	}
	 */
	public Position2D getPosition() {
		return this.position;
	}
	
	/*
	 * public void setPosition(Position pos) { this.pos = pos; } public double
	 * getValue() { return this.value; } public void setValue(double value) {
	 * this.value = value; }
	 */
	
	public int getId() {
		return this.num;
	}
	
	public void setId(int id) {
		this.num = id;
	}
	
	public InformationType getInfoType() {
		return this.infoType;
	}
	
	public void setInfoType(InformationType infoType) {
		this.infoType = infoType;
	}
	
	public double getValue() {
		return this.value;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
	
	public void setPosition(Position2D position) {
		this.position = position;
	}
	
	public void setWeightedValue(double weightedValue) {
		this.weightedValue = weightedValue;
	}
	
	public Element getElement() {
		return this.element;
	}
	
	public void setElement(final Element element) {
		this.element = element;
	}
	
	public boolean isMoreImportantThan(final Information info) {
		return Math.abs(this.getWeightedValue()) > Math.abs(info.getWeightedValue());
	}
	
	public boolean isAboutTheSameElement(final Information info) {
		return this.getElement() == info.getElement();
	}
}
