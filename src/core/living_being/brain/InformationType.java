package core.living_being.brain;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Alexi Coard
 */
public enum InformationType {
	FOOD("Food"),
	WATER("Water"),
	DANGER("Danger"),
	MATE("Mate"),	//Mate, Feed, Kill, Repeat
	ALERT("Alert");
	
	private final String type;
	
	private InformationType(String type)
	{
		this.type = type;
	}
	
	public String getType()
	{
		return this.type;
	}
}
