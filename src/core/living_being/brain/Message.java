package core.living_being.brain;

import core.action.Action;
import core.environment.Element;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon
 */
public class Message {
	private int priority;
	private Information information;
	private Action action;
	private Element aboutWhat;
	
	public Message(int priority, Action action, Information information, Element aboutWhat) {
		this.priority = priority;
		this.action = action;
		this.information = information;
		this.aboutWhat = aboutWhat;
	}
	
	public Message(int priority, Action action, Element aboutWhat) {
		this(priority, action, null, aboutWhat);
	}
	
	public Message(Information information, Element aboutWhat) {
		this(1, null, information, aboutWhat);
	}
	
	public Message(Information information) {
		this(1, null, information, null);
	}
	
	public int getPriority() {
		return priority;
	}
	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	public Information getInformation() {
		return this.information;
	}
	
	public Element getAboutWhat() {
		return this.aboutWhat;
	}
	
	public Action getAction() {
		return action;
	}
}