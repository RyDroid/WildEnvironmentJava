package core.living_being.brain;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Jason Labiod, Nicola Spanti, Alexi Coard
 */
public class Neuron {
	protected static int id;
	private Brain brain;
	private int num;
	private Information info;
	private Date updateDate;
	private ArrayList<Neuron> connectedNeurons;
	
	public Neuron (Brain b, Information info, Date updateDate, ArrayList<Neuron> neurons)
	{
		++Neuron.id;
		this.num = Neuron.id;
		this.brain = b;
		this.info = info;
		this.updateDate = updateDate;
		if (neurons != null) {
			this.connectedNeurons = neurons;
		}
		else {
			this.connectedNeurons = new ArrayList<Neuron>();
		}
	}
	
	public Neuron(Brain b) {
		this(b, null, null, new ArrayList<Neuron>());
	}
	
	public void link(Neuron n) {
		if (this.info != null && n.info != null &&
				this.info.getPosition() != null && n.info.getPosition() != null &&
				this.info.getPosition().isNear(n.info.getPosition(), 15)) {
			this.connectedNeurons.add(n);
		}
	}
	
	public int getId() {
		return this.num;
	}
	
	public void setId(int id) {
		this.num = id;
	}
	
	public Brain getBrain() {
		return brain;
	}
	
	public void setBrain(Brain brain) {
		this.brain = brain;
	}
	public Information getInfo() {
		return this.info;
	}
	
	public void setInfo(Information info) {
		this.info = info;
	}
	
	public Date getUpdateDate() {
		return this.updateDate;
	}
	
	public void updateDate() {
		this.updateDate = new Date();
	}
	
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public void removeFromConnected(Neuron n){
		this.connectedNeurons.remove(n);
	}
	
	public ArrayList<Neuron> getConnectedNeurons() {
		return this.connectedNeurons;
	}
	
	public void setConnectedNeurons(ArrayList<Neuron> connectedNeurons) {
		this.connectedNeurons = connectedNeurons;
	}
	
	public void remove() {
		for (Neuron neuron : this.connectedNeurons) {
			neuron.removeFromConnected(this);
		}
		this.connectedNeurons.clear();
	}
}
