/*
 * Copyright (C) 2014, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package core.utils;

import java.net.URL;

public final class ImagePathGetter
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private ImagePathGetter() {}
	
	
	public static String imagesPath = "/res/drawable/";
	
	
	public static URL get(String name, String extension, int width, int height)
	{
		return ImagePathGetter.get(name +"."+ extension, width, height);
	}
	
	public static URL get(String name, String extension, int size)
	{
		return ImagePathGetter.get(name +"."+ extension, size, size);
	}
	
	public static URL get(String name, String folder)
	{
		return ImagePathGetter.get(folder +"/"+ name);
	}
	
	public static URL get(String name, int width, int height)
	{
		return ImagePathGetter.get(name, width +"x"+ height);
	}
	
	public static URL get(String name, int size)
	{
		return ImagePathGetter.get(name, size, size);
	}
	
	public static URL get(String path)
	{
		return Object.class.getResource(ImagePathGetter.imagesPath + path);
	}
}
