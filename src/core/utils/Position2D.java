package core.utils;

import java.awt.Point;
import java.util.Random;

/**
 * A position in two dimensions.
 * @author Labiod Jason, Nicola Spanti (RyDroid)
 */
@SuppressWarnings("serial")
public class Position2D extends Point.Double
{
	/**
	 * Constructs and initializes a Point2D with the specified coordinates.
	 * @param x position x
	 * @param y position y
	 */
	public Position2D(double x, double y)
	{
		super(x, y);
	}
	
	public Position2D(Point.Double position)
	{
		super(position.x, position.y);
	}
	
	/**
	 * Default constructor.
	 */
	public Position2D()
	{
		super();
	}
	
	
	public void setX(double x)
	{
		super.x = x;
	}
	
	public void setY(double y)
	{
		super.y = y;
	}
	
	
	/**
	 * Calculate if the given position is near this one or not.
	 * @param position
	 * @param r
	 * @return boolean
	 */
	public boolean isNear(Position2D position, double r)
	{
		return Math.sqrt((position.x - this.x) * (position.x - this.x)
				+ (position.y - this.y) * (position.y - this.y)) <= r;
	}
	
	public double distanceXBetween(Position2D position)
	{
		return position.getX() - this.x;
	}
	
	public double distanceYBetween(Position2D position)
	{
		return position.getY() - this.y;
	}
	
	public double distanceBetween(double width, double height)
	{
		return Math.sqrt(width*width + height*height);
	}
	
	/**
	 * @param position
	 * @return double distance between the position of the instance and the one specified in parameter
	 */
	public double distanceBetween(Position2D position)
	{
		return this.distanceBetween(this.distanceXBetween(position), this.distanceYBetween(position));
	}
	
	
	/**
	 * Gives a random position.
	 * @param xMax
	 * @param yMax
	 * @return Position2D a random position
	 */
	public static Position2D getRandom(double xMax, double yMax)
	{
		Random rand = new Random();
		return new Position2D(rand.nextInt((int) xMax), rand.nextInt((int) yMax));
	}
	
	@Override
	public String toString()
	{
		return Math.round(x*100)/100 + ";" + Math.round(y*100)/100;
	}
}
