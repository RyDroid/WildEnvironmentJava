package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.action.Action;

/**
 * @author A. Coard
 */
public class ActionRequest extends Connexion {

	public ActionRequest(String dBPath) {
		super(dBPath);
	}
	
	/**
	 * Add the given Action to table Action
	 */
	public void addAction(Action a) {
		try {
			PreparedStatement preparedStatementAdd = connection
					.prepareStatement("INSERT INTO Action VALUES(?,?)");
			preparedStatementAdd.setInt(1, a.getId());
			preparedStatementAdd.setString(2, a.getClass().getSimpleName());
			preparedStatementAdd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update the data of the given Action
	 */
	public void updateAction(Action a) {
		try {
			PreparedStatement preparedStatement;
			preparedStatement = connection
					.prepareStatement("UPDATE Action SET idAction = ? ,libAction = ?  WHERE idAction = ?");

			preparedStatement.setInt(1, a.getId());
			preparedStatement.setString(2, a.getClass().getSimpleName());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete the given Action from database
	 */
	public void supprimerAction(Action a) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("DELETE FROM Action WHERE idAction = ?");

			preparedStatement.setInt(1, a.getId());
			preparedStatement.executeUpdate();
			System.out.println("Action deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Save the given Action in the database
	 */
	public void sauvegarderAction(Action a) {
		this.connect();

		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Action WHERE idAction="
						+ a.getId());
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				addAction(a);
				System.out.println("Done");
			} else {
				System.out.print("Updating...");
				updateAction(a);
				System.out.println("Done");
			}
			System.out.println("Action saved");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void cleanAction() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idAction FROM Action");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement;
				
					preparedStatement = connection
							.prepareStatement("DELETE FROM Action WHERE idAction = ?");

					preparedStatement.setInt(1, resultTest.getInt("idAction"));
					preparedStatement.executeUpdate();
			}
			System.out.println("Table Action is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();	
	}
}
