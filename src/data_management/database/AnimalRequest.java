package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.living_being.Animal;

/**
 * author: A. Coard
 */
public class AnimalRequest extends Connexion {
	
	public AnimalRequest(String dBPath) {
		super(dBPath);
	}
	
	/**
	 * Add to table Animal
	 */
	public void addAnimal(Animal a) {
		try {
			PreparedStatement preparedStatementAdd = connection
					.prepareStatement("INSERT INTO Animal VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
			preparedStatementAdd.setInt(1, a.getId());
			preparedStatementAdd.setDouble(2, a.getPosition().getX());
			preparedStatementAdd.setDouble(3, a.getPosition().getY());
			preparedStatementAdd.setDouble(4, a.size());
			preparedStatementAdd.setInt(5, a.getHealth());
			preparedStatementAdd.setInt(6, a.getHunger());
			preparedStatementAdd.setInt(7, a.getThirst());
			preparedStatementAdd.setDouble(8, a.getVision());
			preparedStatementAdd.setDouble(9, a.getSpeed());
			preparedStatementAdd.setDouble(10, a.getStrength());
			preparedStatementAdd.setString(11, a.getSex().toString());
			preparedStatementAdd.setInt(12, a.getBrain().getId());
			preparedStatementAdd.executeUpdate();
			BrainRequest b = new BrainRequest("database.db");
			b.connect();
			b.saveBrain(a.getBrain());
			b.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update the data of the given Animal
	 */
	public void updateAnimal(Animal a) {
		try {
			PreparedStatement preparedStatement;
			preparedStatement = connection
					.prepareStatement("UPDATE Animal SET idElement = ? ,posX= ? ,posY= ? ,size= ? ,health= ? ,hunger= ? ,thirst= ? ,vision= ? ,speed = ? ,strength= ? ,sex =? ,idBrain= ? WHERE idElement = ?");
			
			preparedStatement.setInt(1, a.getId());
			preparedStatement.setDouble(2, a.getPosition().getX());
			preparedStatement.setDouble(3, a.getPosition().getY());
			preparedStatement.setDouble(4, a.size());
			preparedStatement.setInt(5, a.getHealth());
			preparedStatement.setInt(6, a.getHunger());
			preparedStatement.setInt(7, a.getThirst());
			preparedStatement.setDouble(8, a.getVision());
			preparedStatement.setDouble(9, a.getSpeed());
			preparedStatement.setDouble(10, a.getStrength());
			preparedStatement.setString(11, a.getSex().toString());
			preparedStatement.setInt(12, a.getBrain().getId());
			preparedStatement.setInt(13, a.getId());
			preparedStatement.executeUpdate();
			BrainRequest b = new BrainRequest("database.db");
			b.connect();
			b.saveBrain(a.getBrain());
			b.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete the given Animal from the database
	 */
	public void deleteAnimal(Animal a) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("DELETE FROM Animal WHERE idElement = ?");
			
			preparedStatement.setInt(1, a.getId());
			preparedStatement.executeUpdate();
			BrainRequest b = new BrainRequest("database.db");
			b.connect();
			b.deleteBrain(a.getBrain());
			b.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Save the given animal in the database,if the animal is already is the
	 * database, use the updateAnimal(Animal a) method else use the
	 * addAnimal(Animal a) method
	 */
	
	public void saveAnimal(Animal a) {
		super.connect();
		
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM ANIMAL WHERE idElement="
						+ a.getId());
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.println("Inserting...");
				addAnimal(a);
			} else {
				System.out.println("Updating...");
				updateAnimal(a);
			}
			System.out.println("Animal saved");
			BrainRequest b = new BrainRequest("database.db");
			b.connect();
			b.saveBrain(a.getBrain());
			b.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanAnimal() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idElement FROM Animal");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement;
				
				preparedStatement = connection
						.prepareStatement("DELETE FROM Animal WHERE idElement = ?");
				
				preparedStatement.setInt(1, resultTest.getInt("idElement"));
				preparedStatement.executeUpdate();
			}
			System.out.println("Table Animal is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
	}
}