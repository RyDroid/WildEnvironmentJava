package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import core.living_being.Behaviour;

/**
 * author: A. Coard
 */
public class BehaviourRequest extends Connexion {

	public BehaviourRequest(String dBPath) {
		super(dBPath);
	}

	/**
	 * Add the given Behaviour to table Behaviour
	 */
	public void addBehaviour(Behaviour b) {
		try {
			PreparedStatement preparedStatementAdd = connection
					.prepareStatement("INSERT INTO Behaviour VALUES(?)");
			preparedStatementAdd.setString(1,b.getName());
			preparedStatementAdd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update the data of the given Behaviour
	 */
	public void updateBehaviour(Behaviour b) {
		try {
			PreparedStatement preparedStatement;
			preparedStatement = connection
					.prepareStatement("UPDATE Behaviour SET libBehaviour = ? WHERE libBehaviour = ?");
			preparedStatement.setString(1,b.getName());
			preparedStatement.setString(2,b.getName());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Delete the given Behaviour from database
	 */
	public void deleteBehaviour(Behaviour b) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("DELETE FROM Behaviour WHERE libBehaviour = ?");

			preparedStatement.setString(1,b.getName());
			preparedStatement.executeUpdate();
			System.out.println("Behaviour deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save the given Behaviour in the database
	 */
	public void saveBehaviour(Behaviour b) {
		super.connect();

		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Behaviour WHERE libBehaviour="
						+ b.getName());
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				addBehaviour(b);
				System.out.println("Done");
			} else {
				System.out.print("Updating...");
				updateBehaviour(b);
				System.out.println("Done");
			}
			System.out.println("Behaviour saved");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanBehaviour() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT libBehaviour FROM Behaviour");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement;
				
					preparedStatement = connection
							.prepareStatement("DELETE FROM Behaviour WHERE libBehaviour = ?");

					preparedStatement.setInt(1, resultTest.getInt("libBehaviour"));
					preparedStatement.executeUpdate();
			}
			System.out.println("Table Behaviour is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
	}
	
}