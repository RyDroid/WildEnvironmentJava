package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.living_being.Animal;
import core.living_being.brain.Brain;
import core.living_being.brain.Neuron;

/**
 * author: A. Coard
 */
public class BrainRequest extends Connexion {
	public BrainRequest(String dBPath) {
		super(dBPath);
	}

	/**
	 * Add the given Brain to table Brain
	 */
	public void addBrain(Brain b) {
		try {
			PreparedStatement preparedStatementAdd = connection
					.prepareStatement("INSERT INTO Brain VALUES(?,?)");
			preparedStatementAdd.setInt(1, b.getId());
			preparedStatementAdd.setInt(2, b.getAnimal().getId());
			preparedStatementAdd.executeUpdate();
			/*NeuronRequest n = new NeuronRequest("database.db");
			n.connect();
			for(Neuron ne: b.getNeurons())
			{
				n.saveNeuron(ne);
			}
			n.close();*/
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update the data of the given Brain
	 */
	public void updateBrain(Brain b) {
		try {
			PreparedStatement preparedStatement;
			preparedStatement = connection
					.prepareStatement("UPDATE Brain SET idBrain = ? ,idElement= ? WHERE idBrain = ?");

			preparedStatement.setInt(1, b.getId());
			preparedStatement.setInt(2, b.getAnimal().getId());
			preparedStatement.executeUpdate();
			NeuronRequest n = new NeuronRequest("database.db");
			n.connect();
			for(Neuron ne: b.getNeurons())
			{
				n.saveNeuron(ne);
			}
			n.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete the given Brain from the database
	 */
	public void deleteBrain(Brain b) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("DELETE FROM Brain WHERE idElement = ?");

			preparedStatement.setInt(1, b.getId());
			preparedStatement.executeUpdate();
			System.out.println("Brain deleted");
			/*NeuronRequest n = new NeuronRequest("database.db");
			n.connect();
			for(Neuron ne: b.getNeurons())
			{
				n.deleteNeuron(ne);
			}
			n.close();*/
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Save the given Brain in the database
	 */
	public void saveBrain(Brain b) {

		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Brain WHERE idBrain=" + b.getId());
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				addBrain(b);
				System.out.println("Done");
			} else {
				System.out.print("Updating...");
				updateBrain(b);
				System.out.println("Done");
			}
			System.out.println("Brain saved");
			/*NeuronRequest n = new NeuronRequest("database.db");
			n.connect();
			for(Neuron ne: b.getNeurons())
			{
				n.saveNeuron(ne);
			}
			n.close();*/
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanBrain() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idBrain FROM Brain");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement;
				
					preparedStatement = connection
							.prepareStatement("DELETE FROM Brain WHERE idBrain = ?");

					preparedStatement.setInt(1, resultTest.getInt("idBrain"));
					preparedStatement.executeUpdate();
			}
			System.out.println("Table Brain is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
		
	}
	
	public int getIdInfoFromAnimal(Animal a)
	{
		int id= -1;
		try {
			ResultSet resultTest = this
				.query("SELECT idBrain FROM Animal WHERE idElement=\""+ a.getId() + "\"");
			id= resultTest.getInt("idBrain");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}
}