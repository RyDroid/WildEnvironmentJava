package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.living_being.Carnivore;
import core.living_being.brain.Brain;
import core.utils.Position2D;

/**
 * @author A. Coard
 */
public class CarnivoreRequest extends Connexion {
	
	public CarnivoreRequest(String dBPath) {
		super(dBPath);
	}
	
	/**
	 * Add to table Carnivore
	 */
	public void addCarnivore(Carnivore c) {
		try {
			PreparedStatement preparedStatementAdd = connection
					.prepareStatement("INSERT INTO Carnivore VALUES(?)");
			preparedStatementAdd.setInt(1, c.getId());
			preparedStatementAdd.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update the data of the given Carnivore
	 */
	public void updateCarnivore(Carnivore c) {
		try {
			PreparedStatement preparedStatement;
			preparedStatement = connection
					.prepareStatement("UPDATE Carnivore SET idElement = ? WHERE idElement = ?");
			preparedStatement.setInt(1, c.getId());
			preparedStatement.setInt(2, c.getId());
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete the given Carnivore from database
	 */
	public void deleteCarnivore(Carnivore c) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("DELETE FROM Carnivore WHERE idElement = ?");
			
			preparedStatement.setInt(1, c.getId());
			preparedStatement.executeUpdate();
			System.out.println("Carnivore deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/*
	 * Synchronize the tables Animal and Carnivore
	 */
	
	public void CarnivoretoAnimalRequest(Carnivore c) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Animal WHERE idElement= \""
						+ c.getId() + "\"");
		
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				try {
					PreparedStatement preparedStatementAdd = connection
							.prepareStatement("INSERT INTO Animal VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
					preparedStatementAdd.setInt(1, c.getId());
					preparedStatementAdd.setDouble(2, c.getPosition().getX());
					preparedStatementAdd.setDouble(3, c.getPosition().getY());
					preparedStatementAdd.setDouble(4, c.size());
					preparedStatementAdd.setInt(5, c.getHealth());
					preparedStatementAdd.setInt(6, c.getHunger());
					preparedStatementAdd.setInt(7, c.getThirst());
					preparedStatementAdd.setDouble(8, c.getVision());
					preparedStatementAdd.setDouble(9, c.getSpeed());
					preparedStatementAdd.setDouble(10, c.getStrength());
					preparedStatementAdd.setString(11, c.getSex().toString());
					preparedStatementAdd.setInt(12, c.getBrain().getId());
					preparedStatementAdd.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("Updating...");
				PreparedStatement preparedStatement;
				preparedStatement = connection
						.prepareStatement("UPDATE Animal SET idElement = ? ,posX= ? ,posY= ? ,size= ? ,health= ? ,hunger= ? ,thirst= ? ,vision= ? ,speed = ? ,strength= ? ,sex =? ,idBrain= ? WHERE idElement = ?");
				
				preparedStatement.setInt(1, c.getId());
				preparedStatement.setDouble(2, c.getPosition().getX());
				preparedStatement.setDouble(3, c.getPosition().getY());
				preparedStatement.setDouble(4, c.size());
				preparedStatement.setInt(5, c.getHealth());
				preparedStatement.setInt(6, c.getHunger());
				preparedStatement.setInt(7, c.getThirst());
				preparedStatement.setDouble(8, c.getVision());
				preparedStatement.setDouble(9, c.getSpeed());
				preparedStatement.setDouble(10, c.getStrength());
				preparedStatement.setString(11, c.getSex().toString());
				preparedStatement.setInt(12, c.getBrain().getId());
				preparedStatement.setInt(13, c.getId());
				preparedStatement.executeUpdate();
			}
			System.out.println("Save of Animal done");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Synchronize the tables Carnivore and LivingBeing
	 */
	
	public void CarnivoretoLivingBeingRequest(Carnivore c) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM LivingBeing WHERE idElement= \""
						+ c.getId() + "\"");
		
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				try {
					PreparedStatement preparedStatementAdd = connection
							.prepareStatement("INSERT INTO LivingBeing VALUES(?,?,?,?)");
					preparedStatementAdd.setInt(1, c.getId());
					preparedStatementAdd.setDouble(2, c.getPosition().getX());
					preparedStatementAdd.setDouble(3, c.getPosition().getY());
					preparedStatementAdd.setDouble(4, c.size());
					preparedStatementAdd.executeUpdate();
					System.out.println("Done");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				System.out.print("Updating...");
				try {
					PreparedStatement preparedStatement;
					
					preparedStatement = connection
							.prepareStatement("UPDATE LivingBeing SET idElement = ? ,posX= ? ,posY= ? ,size= ? WHERE idElement = ?");
					
					preparedStatement.setInt(1, c.getId());
					preparedStatement.setDouble(2, c.getPosition().getX());
					preparedStatement.setDouble(3, c.getPosition().getY());
					preparedStatement.setDouble(4, c.size());
					preparedStatement.setInt(5, c.getId());
					
					preparedStatement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.out.print("Done");
			}
			System.out.println("Save of LivingBeing done");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Synchronize the tables Carnivore and Element
	 */
	
	public void CarnivoretoElementRequest(Carnivore c) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Element WHERE idElement= \""
						+ c.getId() + "\"");
		
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				try {
					PreparedStatement preparedStatementAdd = connection
							.prepareStatement("INSERT INTO Element VALUES(?,?,?)");
					preparedStatementAdd.setInt(1, c.getId());
					preparedStatementAdd.setDouble(2, c.getPosition().getX());
					preparedStatementAdd.setDouble(3, c.getPosition().getY());
					preparedStatementAdd.executeUpdate();
					
					System.out.println("Done");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				System.out.print("Updating...");
				try {
					PreparedStatement preparedStatement;
					preparedStatement = connection
							.prepareStatement("UPDATE Element SET idElement = ? ,posX= ? ,posY= ? WHERE idElement = ?");
					
					preparedStatement.setInt(1, c.getId());
					preparedStatement.setDouble(2, c.getPosition().getX());
					preparedStatement.setDouble(3, c.getPosition().getY());
					preparedStatement.setInt(4, c.getId());
					preparedStatement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.out.print("Done");
			}
			
			System.out.println("Save of Carnivore done");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Save the given Carnivore in the database
	 */
	public void saveCarnivore(Carnivore c) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Carnivore WHERE idElement= \"" + c.getId() + "\"");
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				addCarnivore(c);
				System.out.println("Done");
			} else {
				System.out.print("Updating...");
				updateCarnivore(c);
				System.out.println("Done");
			}
			CarnivoretoAnimalRequest(c);
			CarnivoretoLivingBeingRequest(c);
			CarnivoretoElementRequest(c);
			
			System.out.println("Save of Carnivore done");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanCarnivore() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idElement FROM Carnivore");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement =
						connection.prepareStatement("DELETE FROM Carnivore WHERE idElement = ?");
				
				preparedStatement.setInt(1, resultTest.getInt("idElement"));
				preparedStatement.executeUpdate();
			}
			System.out.println("Table Carnivore is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
	}
	
	public void initialiseCarnivore(Carnivore c) {
		try {
			System.out.print("Initializating...");
			ResultSet resultSet = this
					.query("SELECT * FROM Animal WHERE idElement=\""
							+ c.getId() + "\"");
			
			c.setId(resultSet.getInt("idElement"));
			c.setPosition(new Position2D(resultSet.getDouble("posX"),
					resultSet.getDouble("posY")));
			c.setSize(resultSet.getDouble("size"));
			c.setHealth(resultSet.getInt("health"));
			c.setHunger(resultSet.getInt("hunger"));
			c.setThirst(resultSet.getInt("thirst"));
			c.setVision(resultSet.getDouble("vision"));
			c.setSpeed(resultSet.getDouble("speed"));
			c.setStrength(resultSet.getDouble("strength"));
			c.setSex(resultSet.getString("sex"));
			
			Brain b = new Brain();
			b.setId(resultSet.getInt("idBrain"));
			c.setBrain(b);
			
			System.out.println("Done");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
