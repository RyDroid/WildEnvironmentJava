package data_management.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * author: A. Coard
 */
public class Connexion {
	private String DBPath; // Path to database SQLite
	protected Connection connection;
	protected Statement statement;
	
	
	public Connexion(String dBPath) {
		this.DBPath = dBPath;
	}
	
	
	/**
	 * Create a connection with the database
	 */
	public void connect(String subprotocol, String subname) {
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:"+ subprotocol +":"+ subname);
			statement = connection.createStatement();

			System.out.println("Successfully connected to " + DBPath );
		} catch (ClassNotFoundException notFoundException) {
			notFoundException.printStackTrace();
			System.out.println("[JAVA] Error of connection");
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
			System.out.println("[SQL] Error of connection");
		}
	}
	
	public void connect(String subprotocol) {
		this.connect(subprotocol, this.DBPath);
	}
	
	public void connect() {
		this.connect("sqlite");
	}

	/**
	 * Query to the database
	 * @param String Request: The SQL request to be execute
	 */
	public ResultSet query(String request) {
		ResultSet result = null;
		try {
			result = statement.executeQuery(request);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error in request: " + request);
		}
		return result;
	}
	
	public void cleanDatabase()
	{
		ActionRequest ac = new ActionRequest("database.db");
		ac.cleanAction();
		AnimalRequest a = new AnimalRequest("database.db");
		a.cleanAnimal();
		BehaviourRequest be = new BehaviourRequest("database.db");
		be.cleanBehaviour();
		BrainRequest b = new BrainRequest("database.db");
		b.cleanBrain();
		CarnivoreRequest c = new CarnivoreRequest("database.db");
		c.cleanCarnivore();
		ElementRequest e = new ElementRequest("database.db");
		e.cleanElement();
		HerbivoreRequest h = new HerbivoreRequest("database.db");
		h.cleanHerbivore();
		InformationRequest i = new InformationRequest("database.db");
		i.cleanInformation();
		LivingBeingRequest l = new LivingBeingRequest("database.db");
		l.cleanLivingBeing();
		NeuronRequest n = new NeuronRequest("database.db");
		n.cleanNeuron();
		VegetalRequest v = new VegetalRequest("database.db");
		v.cleanVegetal();
		ZoneRequest z = new ZoneRequest("database.db");
		z.cleanZone();

	}
	
	public void close() {
		try {
			connection.close();
			System.out.println("Connection closed");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
