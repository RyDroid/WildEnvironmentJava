package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.environment.Element;


	/**
	 * author: A. Coard
	 */


/**
 * author: A. Coard
 */

public class ElementRequest extends Connexion {

	public ElementRequest(String dBPath) {
		super(dBPath);
	}

	/**
	 * Add the given Element to the database
	 */
	public void addElement(Element el) {
		try {
			PreparedStatement preparedStatementAdd = connection
					.prepareStatement("INSERT INTO Element VALUES(?,?,?)");
			preparedStatementAdd.setInt(1, el.getId());
			preparedStatementAdd.setDouble(2, el.getPosition().getX());
			preparedStatementAdd.setDouble(3, el.getPosition().getY());
			preparedStatementAdd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update the data of the given element
	 */
	public void updateElement(Element el) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("UPDATE Element SET idElement = ? ,posX= ? ,posY= ? WHERE idElement = ?");

			preparedStatement.setInt(1, el.getId());
			preparedStatement.setDouble(2, el.getPosition().getX());
			preparedStatement.setDouble(3, el.getPosition().getY());
			preparedStatement.setInt(4, el.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Delete the given Element from the database
	 */
	public void supprimerElement(Element el) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("DELETE FROM Element WHERE idElement = ?");

			preparedStatement.setInt(1, el.getId());
			preparedStatement.executeUpdate();
			System.out.println("Element deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save the given Element in the database
	 */
	public void saveElement(Element el) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Element WHERE idElement=\""
						+ el.getId() + "\"");
		try {
			if (resultTest.getInt(1) == 0) {

				System.out.print("Inserting...");
				addElement(el);
				System.out.println("Done");
			} else {
				System.out.print("Updating...");
				updateElement(el);
				System.out.println("Done");

			}
			System.out.println("Element saved");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanElement() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idElement FROM Element");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement;
				
					preparedStatement = connection
							.prepareStatement("DELETE FROM Element WHERE idElement = ?");

					preparedStatement.setInt(1, resultTest.getInt("idElement"));
					preparedStatement.executeUpdate();
			}
			System.out.println("Table Element is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
		
	}
}
