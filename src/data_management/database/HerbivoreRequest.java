package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.living_being.Herbivore;
import core.living_being.brain.Brain;
import core.utils.Position2D;

/**
 * @author A. Coard
 */
public class HerbivoreRequest extends Connexion {
	
	public HerbivoreRequest(String dBPath) {
		super(dBPath);
	}
	
	/**
	 * Add to table Herbivore
	 */
	public void addHerbivore(Herbivore h) {
		try {
			PreparedStatement preparedStatementAdd = connection
					.prepareStatement("INSERT INTO Herbivore VALUES(?)");
			preparedStatementAdd.setInt(1, h.getId());
			preparedStatementAdd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update the data of the given Herbivore
	 */
	public void updateHerbivore(Herbivore h) {
		try {
			PreparedStatement preparedStatement;
			preparedStatement = connection
					.prepareStatement("UPDATE Herbivore SET idElement = ? WHERE idElement = ?");
			preparedStatement.setInt(1, h.getId());
			preparedStatement.setInt(2, h.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Synchronize the tables Animal and Carnivore
	 */
	
	public void HerbivoretoAnimalRequest(Herbivore h) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Animal WHERE idElement= \""
						+ h.getId() + "\"");
		
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				try {
					PreparedStatement preparedStatementAdd = connection
							.prepareStatement("INSERT INTO Animal VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
					preparedStatementAdd.setInt(1, h.getId());
					preparedStatementAdd.setDouble(2, h.getPosition().getX());
					preparedStatementAdd.setDouble(3, h.getPosition().getY());
					preparedStatementAdd.setDouble(4, h.size());
					preparedStatementAdd.setInt(5, h.getHealth());
					preparedStatementAdd.setInt(6, h.getHunger());
					preparedStatementAdd.setInt(7, h.getThirst());
					preparedStatementAdd.setDouble(8, h.getVision());
					preparedStatementAdd.setDouble(9, h.getSpeed());
					preparedStatementAdd.setDouble(10, h.getStrength());
					preparedStatementAdd.setString(11, h.getSex().toString());
					preparedStatementAdd.setInt(12, h.getBrain().getId());
					preparedStatementAdd.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.out.println("Done");
			} else {
				System.out.println("Updating...");
				PreparedStatement preparedStatement;
				preparedStatement = connection
						.prepareStatement("UPDATE Animal SET idElement = ? ,posX= ? ,posY= ? ,size= ? ,health= ? ,hunger= ? ,thirst= ? ,vision= ? ,speed = ? ,strength= ? ,sex =? ,idBrain= ? WHERE idElement = ?");
				
				preparedStatement.setInt(1, h.getId());
				preparedStatement.setDouble(2, h.getPosition().getX());
				preparedStatement.setDouble(3, h.getPosition().getY());
				preparedStatement.setDouble(4, h.size());
				preparedStatement.setInt(5, h.getHealth());
				preparedStatement.setInt(6, h.getHunger());
				preparedStatement.setInt(7, h.getThirst());
				preparedStatement.setDouble(8, h.getVision());
				preparedStatement.setDouble(9, h.getSpeed());
				preparedStatement.setDouble(10, h.getStrength());
				preparedStatement.setString(11, h.getSex().toString());
				preparedStatement.setInt(12, h.getBrain().getId());
				preparedStatement.setInt(13, h.getId());
				preparedStatement.executeUpdate();
				
				System.out.print("Done");
			}
			System.out.println("Save of Animal done");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Synchronize the tables Herbivore and LivingBeing
	 */
	
	public void HerbivoretoLivingBeingRequest(Herbivore h) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM LivingBeing WHERE idElement= \""
						+ h.getId() + "\"");
		
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				try {
					PreparedStatement preparedStatementAdd = connection
							.prepareStatement("INSERT INTO LivingBeing VALUES(?,?,?,?)");
					preparedStatementAdd.setInt(1, h.getId());
					preparedStatementAdd.setDouble(2, h.getPosition().getX());
					preparedStatementAdd.setDouble(3, h.getPosition().getY());
					preparedStatementAdd.setDouble(4, h.size());
					preparedStatementAdd.executeUpdate();
					System.out.println("Done");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				System.out.print("Updating...");
				try {
					PreparedStatement preparedStatement = connection
							.prepareStatement("UPDATE LivingBeing SET idElement = ? ,posX= ? ,posY= ? ,size= ? WHERE idElement = ?");
					
					preparedStatement.setInt(1, h.getId());
					preparedStatement.setDouble(2, h.getPosition().getX());
					preparedStatement.setDouble(3, h.getPosition().getY());
					preparedStatement.setDouble(4, h.size());
					preparedStatement.setInt(5, h.getId());
					
					preparedStatement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.out.print("Done");
			}
			System.out.println("Save of LivingBeing done");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Synchronize the tables Herbivore and Element
	 */
	
	public void HerbivoretoElementRequest(Herbivore h) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Element WHERE idElement= \""
						+ h.getId() + "\"");
		
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				try {
					PreparedStatement preparedStatementAdd = connection
							.prepareStatement("INSERT INTO Element VALUES(?,?,?)");
					preparedStatementAdd.setInt(1, h.getId());
					preparedStatementAdd.setDouble(2, h.getPosition().getX());
					preparedStatementAdd.setDouble(3, h.getPosition().getY());
					preparedStatementAdd.executeUpdate();
					
					System.out.println("Done");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				System.out.print("Updating...");
				try {
					PreparedStatement preparedStatement;
					preparedStatement = connection
							.prepareStatement("UPDATE Element SET idElement = ? ,posX= ? ,posY= ? WHERE idElement = ?");
					
					preparedStatement.setInt(1, h.getId());
					preparedStatement.setDouble(2, h.getPosition().getX());
					preparedStatement.setDouble(3, h.getPosition().getY());
					preparedStatement.setInt(4, h.getId());
					preparedStatement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.out.print("Done");
			}
			System.out.println("Save of Element done");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete the given Herbivore from database
	 */
	public void deleteHerbivore(Herbivore h) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("DELETE FROM Herbivore WHERE idElement = ?");
			
			preparedStatement.setInt(1, h.getId());
			preparedStatement.executeUpdate();
			
			BrainRequest b = new BrainRequest("database.db");
			b.connect();
			b.deleteBrain(h.getBrain());
			b.close();
			
			preparedStatement = connection
					.prepareStatement("DELETE FROM Animal WHERE idElement = ?");
			preparedStatement.setInt(1, h.getId());
			preparedStatement.executeUpdate();
			
			preparedStatement = connection
					.prepareStatement("DELETE FROM LivingBeing WHERE idElement = ?");
			preparedStatement.setInt(1, h.getId());
			preparedStatement.executeUpdate();
			
			preparedStatement = connection
					.prepareStatement("DELETE FROM Element WHERE idElement = ?");
			preparedStatement.setInt(1, h.getId());
			preparedStatement.executeUpdate();
			
			System.out.println("Herbivore deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Save the given Herbivore in the database
	 */
	public void saveHerbivore(Herbivore h) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Herbivore WHERE idElement= \""
						+ h.getId() + "\"");
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				addHerbivore(h);
				System.out.println("Done");
			} else {
				
				System.out.print("Updating...");
				updateHerbivore(h);
				System.out.println("Done");
			}
			HerbivoretoAnimalRequest(h);
			HerbivoretoLivingBeingRequest(h);
			HerbivoretoElementRequest(h);
			
			System.out.println("Herbivore saved");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanHerbivore() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idElement FROM Herbivore");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement = connection
						.prepareStatement("DELETE FROM Herbivore WHERE idElement = ?");
				
				preparedStatement.setInt(1, resultTest.getInt("idElement"));
				preparedStatement.executeUpdate();
			}
			System.out.println("Table Herbivore is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
		
	}
	
	public void initialiseHerbivore(Herbivore h)
	{
		try {
			System.out.print("Initializating...");
			ResultSet resultSet = this.query("SELECT * FROM Animal WHERE idElement=\""
					+ h.getId() + "\"");
			
			h.setId(resultSet.getInt("idElement"));
			h.setPosition(new Position2D(resultSet.getDouble("posX"), resultSet.getDouble("posY")));
			h.setSize(resultSet.getDouble("size"));
			h.setHealth(resultSet.getInt("health"));
			h.setHunger(resultSet.getInt("hunger"));
			h.setThirst(resultSet.getInt("thirst"));
			h.setVision(resultSet.getDouble("vision"));
			h.setSpeed(resultSet.getDouble("speed"));
			h.setStrength(resultSet.getDouble("strength"));
			h.setSex(resultSet.getString("sex"));
			Brain b = new Brain();
			b.setId(resultSet.getInt("idBrain"));
			h.setBrain(b);
			System.out.println("Done");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
