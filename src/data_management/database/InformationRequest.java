package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import core.living_being.brain.Information;
import core.living_being.brain.InformationType;
import core.living_being.brain.Neuron;
import core.utils.Position2D;

/**
 * author: A. Coard
 */
public class InformationRequest extends Connexion {

	public InformationRequest(String dBPath) {
		super(dBPath);
	}
	
	/**
	 * Add the given Information to table Information
	 */
	public void addInformation(Information i) {
		try {
			PreparedStatement preparedStatementAdd = connection.prepareStatement("INSERT INTO Information VALUES(?,?,?,?,?,?)");
			preparedStatementAdd.setInt(1, i.getId());
			preparedStatementAdd.setString(2, i.getInfoType().getType());
			preparedStatementAdd.setDouble(3, i.getPosition().getX());
			preparedStatementAdd.setDouble(4, i.getPosition().getY());
			preparedStatementAdd.setDouble(5, i.getValue());
			preparedStatementAdd.setDouble(6, i.getWeightedValue());
			preparedStatementAdd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update the data of the given Information
	 */
	public void updateInformation(Information i) {
		try {
			PreparedStatement preparedStatement;
			preparedStatement = connection.prepareStatement(
					"UPDATE Information SET idInfo = ?, infoType = ?, posX =  ? , posY= ? ,value = ?, weightedValue = ? WHERE idInfo = ?");
			preparedStatement.setInt(1, i.getId());
			preparedStatement.setString(2, i.getInfoType().getType());
			preparedStatement.setDouble(3, i.getPosition().getX());
			preparedStatement.setDouble(4, i.getPosition().getY());
			preparedStatement.setDouble(5, i.getValue());
			preparedStatement.setDouble(6, i.getWeightedValue());
			preparedStatement.setInt(7, i.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Delete the given Information from the database
	 */
	public void deleteInformation(Information i) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement("DELETE FROM Information WHERE idInfo = ?");
			preparedStatement.setInt(1, i.getId());
			preparedStatement.executeUpdate();
			System.out.println("Information deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save the given Information in the database
	 */
	public void saveInformation(Information i) {
		super.connect();

		ResultSet resultTest = this.query("SELECT COUNT(*) FROM Information WHERE idInfo="+ i.getId());
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				addInformation(i);
				System.out.println("Done");
			} else {
				System.out.println("Updating...");
				updateInformation(i);
				System.out.println("Done");
			}
			System.out.println("Save of Information");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanInformation() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idInfo FROM Information");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement;
				
					preparedStatement = connection
							.prepareStatement("DELETE FROM Information WHERE idInfo = ?");

					preparedStatement.setInt(1, resultTest.getInt("idInfo"));
					preparedStatement.executeUpdate();
					System.out.println("Table Information is now empty");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
	}
	
	public int getIdInfoFromNeuron(Neuron n)
	{
		int id= -1;
		try {
			ResultSet resultTest = this
				.query("SELECT idInfo FROM Neuron WHERE idNeuron=\""+ n.getId() + "\"");
			id= resultTest.getInt("idInfo");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
	
	
	public void initialiseInformation(Information i)
	{
		try {
			System.out.print("Initializating Information...");
			ResultSet resultSet = this.query("SELECT * FROM Information WHERE idInfo=\""
						+ i.getId() + "\"");
		
			
			switch(resultSet.getString("infoType"))
			{
				case "Food" 	: i.setInfoType(InformationType.FOOD);
				case "Water" 	: i.setInfoType(InformationType.WATER);
				case "Danger" 	: i.setInfoType(InformationType.DANGER);
				case "Mate"		: i.setInfoType(InformationType.MATE);
				case "Alert"	: i.setInfoType(InformationType.ALERT);
			}
			
			i.setPosition(new Position2D(resultSet.getDouble("posX"), resultSet.getDouble("posY")));	
			i.setValue(resultSet.getDouble("value"));
			i.setWeightedValue(resultSet.getDouble("weigthedValue"));
			System.out.println("Done");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	
}