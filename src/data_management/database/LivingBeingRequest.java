package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import core.living_being.LivingBeing;

/**
 * author: A. Coard
 */
public class LivingBeingRequest extends Connexion {

	public LivingBeingRequest(String dBPath) {
		super(dBPath);
	}

	/**
	 * Add to table LivingBeing
	 */
	public void addLivingBeing(LivingBeing l) {
		try {
			PreparedStatement preparedStatementAdd = connection
					.prepareStatement("INSERT INTO LivingBeing VALUES(?,?,?,?)");
			preparedStatementAdd.setInt(1, l.getId());
			preparedStatementAdd.setDouble(2, l.getPosition().getX());
			preparedStatementAdd.setDouble(3, l.getPosition().getY());
			preparedStatementAdd.setDouble(4, l.size());

			preparedStatementAdd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update the data of the given LivingBeing
	 */
	public void updateLivingBeing(LivingBeing l) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("UPDATE LivingBeing SET idElement = ? ,posX= ? ,posY= ? ,size= ? WHERE idElement = ?");

			preparedStatement.setInt(1, l.getId());
			preparedStatement.setDouble(2, l.getPosition().getX());
			preparedStatement.setDouble(3, l.getPosition().getY());
			preparedStatement.setDouble(4, l.size());
			preparedStatement.setInt(5, l.getId());
			
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteLivingBeing(LivingBeing l) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("DELETE FROM LivingBeing WHERE idElement = ?");

			preparedStatement.setInt(1, l.getId());
			preparedStatement.executeUpdate();
			System.out.println("LivingBeing deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save the given LivingBeing in the database
	 */
	public void saveLivingBeing(LivingBeing l) {
		this.connect();

		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM LivingBeing WHERE idElement="
						+ l.getId());
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				addLivingBeing(l);
				System.out.println("Done");
			} else {
				System.out.print("Updating...");
				updateLivingBeing(l);
				System.out.println("Done");
			}
			System.out.println("LivingBeing saved");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanLivingBeing() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idElement FROM LivingBeing");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement;
				
					preparedStatement = connection
							.prepareStatement("DELETE FROM LivingBeing WHERE idElement = ?");

					preparedStatement.setInt(1, resultTest.getInt("idElement"));
					preparedStatement.executeUpdate();
			}
			System.out.println("Table LivingBeing is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
	}
}