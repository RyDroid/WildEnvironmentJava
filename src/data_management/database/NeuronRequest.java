package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import core.living_being.brain.Neuron;

/**
 * author: A. Coard
 */
public class NeuronRequest extends Connexion {

	public NeuronRequest(String dBPath) {
		super(dBPath);
	}

	/**
	 * Add the given Neuron to table Neuron
	 */
	public void addNeuron(Neuron n) {
		try {
			PreparedStatement preparedStatementAdd = connection
					.prepareStatement("INSERT INTO Neuron VALUES(?,?,?,?)");
			preparedStatementAdd.setInt(1, n.getId());
			preparedStatementAdd.setInt(2, n.getBrain().getId());
			if(n.getInfo() != null)
			{
				preparedStatementAdd.setInt(3, n.getInfo().getId());
			}
			
			else{
				preparedStatementAdd.setInt(3, 0);
			}
			preparedStatementAdd.setString(4, n.getUpdateDate() + "");
			preparedStatementAdd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Update the data of the given Neuron
	 */
	public void updateNeuron(Neuron n) {
		try {
			PreparedStatement preparedStatement;
			preparedStatement = connection
					.prepareStatement("UPDATE Neuron SET idNeuron = ? ,idBrain = ? ,idInfo= ? ,dateUpdate = ? WHERE idNeuron = ?");
			preparedStatement.setInt(1, n.getId());
			preparedStatement.setInt(2, n.getBrain().getId());
			if(n.getInfo() != null)
			{
				preparedStatement.setInt(3, n.getInfo().getId());
			}
			
			else{
				preparedStatement.setInt(3, 0);
			}
			preparedStatement.setString(4, n.getUpdateDate() + "");
			preparedStatement.setInt(5, n.getId());
			preparedStatement.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Delete the given Neuron from database
	 */
	public void deleteNeuron(Neuron n) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("DELETE FROM Neuron WHERE idNeuron = ?");

			preparedStatement.setInt(1, n.getId());
			preparedStatement.executeUpdate();
			System.out.println("Neuron deleted");
			InformationRequest info = new InformationRequest("database.db");
			info.connect();
			info.deleteInformation(n.getInfo());
			info.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save the given Neuron in the database
	 */
	public void saveNeuron(Neuron n) {
		super.connect();

		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Neuron WHERE idNeuron="
						+ n.getId());
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.println("Inserting...");
				addNeuron(n);
			} else {
				System.out.println("Updating...");
				updateNeuron(n);
			}
			System.out.println("Save of the neuron done");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanNeuron() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idNeuron FROM Neuron");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement;
				
					preparedStatement = connection
							.prepareStatement("DELETE FROM Neuron WHERE idNeuron = ?");

					preparedStatement.setInt(1, resultTest.getInt("idNeuron"));
					preparedStatement.executeUpdate();
			}
			System.out.println("Table Neuron is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
	}
	
	public void initialiseNeuron(Neuron n)
	{
		try {
			System.out.print("Initializating...");
			ResultSet resultSet = this
					.query("SELECT * FROM Neuron WHERE idNeuron=\""
							+ n.getId() + "\"");
			while(resultSet.next())
			{
				n.getBrain().setId(resultSet.getInt("idBrain"));
				n.getInfo().setId(resultSet.getInt("idInfo"));
				String s =resultSet.getString("dateUpdate");
				Date date;
				try {
					date = new SimpleDateFormat("MMMM d, yyyy", Locale.FRENCH).parse(s);
					n.setUpdateDate(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
			
			}
			System.out.println("Done...");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}