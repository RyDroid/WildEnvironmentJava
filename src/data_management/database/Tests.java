package data_management.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

import core.environment.Zone;
import core.living_being.Carnivore;
import core.living_being.Herbivore;
import core.utils.Position2D;

public class Tests {
	/*@Test
	public void testTableElement() { // TODO voir qu'en faire (supprimer ?) (Element n'est plus instanciable)
		ElementRequest req = new ElementRequest("database.db");
		req.connect();	
		Position2D p = new Position2D(100,250);
		Element el = new Element(p);
		req.sauvegarderElement(el);
		ResultSet resultSet = req.query("SELECT * FROM Element");
	        try {
	            while (resultSet.next()) {
	            	System.out.println("id : " + resultSet.getInt("idElement")+ "\t" +"position : "+resultSet.getString("position"));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	     Position2D pp = new Position2D(250,100);
	     el.setPosition(pp);
	     req.sauvegarderElement(el);
	     Element e2 = new Element(p);
	     req.sauvegarderElement(e2);
	     ResultSet resultSetbis = req.query("SELECT * FROM Element");
	        try {
	            while (resultSetbis.next()) {
	            	System.out.println("id : " + resultSet.getInt("idElement")+ "\t" +"position : "+resultSet.getString("position"));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	     req.supprimerElement(el);
	     req.supprimerElement(e2);
		req.close();
	}*/
	

	@Test
	public void testZone() //TODO a refaire, le constructeur de zones à changé
	{
		Position2D p = new Position2D(100,250);

		Zone z = new Zone(null, p, 100, 200);
		ZoneRequest req= new ZoneRequest("database.db");
		req.connect();
		req.saveZone(z);
		ResultSet resultSet = req.query("SELECT * FROM Zone");
	        try {
	            while (resultSet.next()) {
	            	System.out.println("id : " + resultSet.getInt("idElement")+ "\t" +"width : "+resultSet.getDouble("width")+ "\t" +"height : "+resultSet.getDouble("height"));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	     System.out.println(z.getWidth());
	     req.saveZone(z);
	     Zone z2 = new Zone(null, p, 300, 500);
	     req.saveZone(z2);
	     ResultSet resultSetbis = req.query("SELECT * FROM Zone");
	        try {
	            while (resultSetbis.next()) {
	            	System.out.println("id : " + resultSetbis.getInt("idElement")+ "\t" +"width : "+resultSetbis.getDouble("width")+ "\t" +"height : "+resultSetbis.getDouble("height"));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
		req.deleteZone(z);
		req.deleteZone(z2);
		req.close();
	}
	
	
	@Test
	public void testCarnivore()
	{
		Carnivore c = new Carnivore(null);
		CarnivoreRequest req= new CarnivoreRequest("database.db");
		req.connect();
		req.saveCarnivore(c);
		ResultSet resultSet = req.query("SELECT * FROM Carnivore");
	        try {
	            while (resultSet.next()) {
	            	System.out.println("id : " + resultSet.getInt("idElement"));
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    req.deleteCarnivore(c);

		req.close();
	}

	
	@Test
	public void testHerbivore()
	{
		Herbivore h = new Herbivore(null);
		HerbivoreRequest req= new HerbivoreRequest("database.db");
		req.connect();
		req.saveHerbivore(h);
		ResultSet resultSet = req.query("SELECT * FROM Herbivore");
	        try {
	            while (resultSet.next())
	            	System.out.println("id: " + resultSet.getInt("idElement"));
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    req.deleteHerbivore(h);

		req.close();
	}
/*
	@Test
	public void testNeuron()
	{
		Position2D p = new Position2D(100,200);
		ArrayList<Neuron> tab= new ArrayList<>();
		Brain b = new Brain(tab);
		Information i = new Information(InformationType.MATE, p, 2);
		Date d = new Date();
		Neuron n = new Neuron(b, i, d, tab);
		NeuronRequest req = new NeuronRequest("database.db");
		req.connect();
		req.saveNeuron(n);
		ResultSet resultSet = req.query("SELECT * FROM Neuron");
        try {
            while (resultSet.next()) {
            	System.out.println("id : " + resultSet.getInt("idNeuron")+"\t" + "idBrain : " + resultSet.getInt("idBrain") + "\t" +"idInfo : "+resultSet.getInt("idInfo")+"\t"+ "dateUpdate"+ resultSet.getString("dateUpdate"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.deleteNeuron(n);
        req.close();
		
	}
	
	@Test
	public void testInformation()
	{
		Position2D p = new Position2D(100,200);
		Information i = new Information(InformationType.MATE, p, 2);
		InformationRequest req = new InformationRequest("database.db");
		req.connect();
		req.saveInformation(i);
		ResultSet resultSet = req.query("SELECT * FROM Information");
        try {
            while (resultSet.next()) {
            	System.out.println("id : " + resultSet.getInt("idInfo")+"\t" + "infoType : " + resultSet.getString("infoType") + "\t" +"position : "+resultSet.getString("position")+
            			"\t"+ "value : "+ resultSet.getDouble("value")+"\t" + "weightedValue : "+ resultSet.getDouble("weightedValue"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        i.setValue(12);
        req.saveInformation(i);
    	ResultSet resultSetbis = req.query("SELECT * FROM Information");
        try {
            while (resultSet.next()) {
            	System.out.println("id : " + resultSetbis.getInt("idInfo")+"\t" + "infoType : " + resultSetbis.getString("infoType") + "\t" +"position : "+resultSetbis.getString("position")+
            			"\t"+ "value : "+ resultSetbis.getDouble("value")+"\t" + "weightedValue : "+ resultSetbis.getDouble("weightedValue"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.deleteInformation(i);
        req.close();
	}*/
}