package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import core.living_being.Vegetal;
import core.utils.Position2D;

/**
 * author: A. Coard
 */
public class VegetalRequest extends Connexion {

	public VegetalRequest(String dBPath) {
		super(dBPath);
	}

	/**
	 * Add to table Vegetal
	 */
	public void addVegetal(Vegetal v) {
		try {
			PreparedStatement preparedStatementAdd = connection
					.prepareStatement("INSERT INTO Vegetal VALUES(?)");
			preparedStatementAdd.setInt(1, v.getId());
			preparedStatementAdd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update the data of the given Vegetal
	 */
	public void updateVegetal(Vegetal v) {
		try {
			PreparedStatement preparedStatement;
			preparedStatement = connection
					.prepareStatement("UPDATE Vegetal SET idElement = ? WHERE idElement = ?");
			preparedStatement.setInt(1, v.getId());
			preparedStatement.setInt(2, v.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Delete the given Vegetal from the database
	 */
	public void deleteVegetal(Vegetal v) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("DELETE FROM Vegetal WHERE idElement = ?");
			preparedStatement.setInt(1, v.getId());
			preparedStatement.executeUpdate();
			System.out.println("Vegetal deleted");

			preparedStatement = connection
					.prepareStatement("DELETE FROM LivingBeing WHERE idElement = ?");
			preparedStatement.setInt(1, v.getId());
			preparedStatement.executeUpdate();

			preparedStatement = connection
					.prepareStatement("DELETE FROM Element WHERE idElement = ?");
			preparedStatement.setInt(1, v.getId());
			preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Synchronize the tables Vegetal and LivingBeing
	 */

	public void VegetaltoLivingBeingRequest(Vegetal v) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM LivingBeing WHERE idElement= \""
						+ v.getId() + "\"");

		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				try {
					PreparedStatement preparedStatementAdd = connection
							.prepareStatement("INSERT INTO LivingBeing VALUES(?,?,?,?)");
					preparedStatementAdd.setInt(1, v.getId());
					preparedStatementAdd.setDouble(2, v.getPosition().getX());
					preparedStatementAdd.setDouble(3, v.getPosition().getY());
					preparedStatementAdd.setDouble(4, v.size());
					preparedStatementAdd.executeUpdate();
					System.out.println("Done");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				System.out.print("Updating...");
				try {
					PreparedStatement preparedStatement;

					preparedStatement = connection
							.prepareStatement("UPDATE LivingBeing SET idElement = ? ,posX= ? ,posY= ? ,size= ? WHERE idElement = ?");

					preparedStatement.setInt(1, v.getId());
					preparedStatement.setDouble(2, v.getPosition().getX());
					preparedStatement.setDouble(3, v.getPosition().getY());
					preparedStatement.setDouble(4, v.size());
					preparedStatement.setInt(5, v.getId());

					preparedStatement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.out.print("Done");
			}
			System.out.println("Save of LivingBeing done");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Synchronize the tables Vegetal and Element
	 */

	public void VegetaltoElementRequest(Vegetal v) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Element WHERE idElement= \""
						+ v.getId() + "\"");

		try {
			if (resultTest.getInt(1) == 0) {
				System.out.print("Inserting...");
				try {
					PreparedStatement preparedStatementAdd = connection
							.prepareStatement("INSERT INTO Element VALUES(?,?,?)");
					preparedStatementAdd.setInt(1, v.getId());
					preparedStatementAdd.setDouble(2, v.getPosition().getX());
					preparedStatementAdd.setDouble(3, v.getPosition().getY());
					preparedStatementAdd.executeUpdate();

					System.out.println("Done");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				System.out.print("Updating...");
				try {
					PreparedStatement preparedStatement;
					preparedStatement = connection
							.prepareStatement("UPDATE Element SET idElement = ? ,posX= ? ,posY= ? WHERE idElement = ?");

					preparedStatement.setInt(1, v.getId());
					preparedStatement.setDouble(2, v.getPosition().getX());
					preparedStatement.setDouble(3, v.getPosition().getY());
					preparedStatement.setInt(4, v.getId());
					preparedStatement.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.out.print("Done");
			}
			System.out.println("Save of Element done");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save the given Vegetal in the database
	 */
	public void saveVegetal(Vegetal v) {

		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Vegetal WHERE idElement="
						+ v.getId());
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.println("Inserting...");
				addVegetal(v);
			} else {
				System.out.println("Updating...");
				updateVegetal(v);
			}
			System.out.println("Vegetal saved");
			VegetaltoLivingBeingRequest(v);
			VegetaltoElementRequest(v);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanVegetal() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idElement FROM Vegetal");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement;
				
					preparedStatement = connection
							.prepareStatement("DELETE FROM Vegetal WHERE idElement = ?");

					preparedStatement.setInt(1, resultTest.getInt("idElement"));
					preparedStatement.executeUpdate();
			}
			System.out.println("Table Vegetal is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
		
	}
	
	public void initialiseVegetal(Vegetal v)
	{
		try {
			System.out.print("Initializating...");
			ResultSet resultSet = this.query("SELECT * FROM LivingBeing WHERE idElement=\""
						+ v.getId() + "\"");
		
			v.setId(resultSet.getInt("idElement"));
			v.setPosition(new Position2D(resultSet.getDouble("posX"), resultSet.getDouble("posY")));
			v.setSize(resultSet.getDouble("size"));
			System.out.println("Done");

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
}