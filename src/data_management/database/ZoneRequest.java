package data_management.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import core.environment.Zone;
import core.environment.ZoneType;
import core.utils.Position2D;
	
/**
 * author: A. Coard
 */
public class ZoneRequest extends Connexion {

	public ZoneRequest(String dBPath) {
		super(dBPath);
	}

	/**
	 * Add to table Zone
	 */
	public void addZone(Zone z) {
		try {
			PreparedStatement preparedStatementAdd = connection.prepareStatement("INSERT INTO Zone VALUES(?,?,?,?,?,?)");
			preparedStatementAdd.setInt(1, z.getId());
			preparedStatementAdd.setString(2, z.getZoneType().getType());
			preparedStatementAdd.setDouble(3, z.getPosition().getX());
			preparedStatementAdd.setDouble(4, z.getPosition().getY());			
			preparedStatementAdd.setDouble(5, z.getWidth());
			preparedStatementAdd.setDouble(6, z.getHeight());
			preparedStatementAdd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update the data of the given Zone
	 */
	public void updateZone(Zone z) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection
					.prepareStatement("UPDATE Zone SET idElement = ? ,type= ?, posX= ?, posY= ? ,width= ? ,height= ?  WHERE idElement = ?");
			preparedStatement.setInt(1, z.getId());
			preparedStatement.setString(2, z.getZoneType().getType());
			preparedStatement.setDouble(3, z.getPosition().getX());
			preparedStatement.setDouble(4, z.getPosition().getY());
			preparedStatement.setDouble(5, z.getWidth());
			preparedStatement.setDouble(6, z.getHeight());
			preparedStatement.setInt(7, z.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Delete the given Zone
	 */
	public void deleteZone(Zone z) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement("DELETE FROM Zone WHERE idElement = ?");
			preparedStatement.setInt(1, z.getId());
			preparedStatement.executeUpdate();
			System.out.println("Zone deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save the given zone in the database
	 */
	public void saveZone(Zone z) {
		ResultSet resultTest = this
				.query("SELECT COUNT(*) FROM Zone WHERE idElement=\"" + z.getId() + "\"");
		try {
			if (resultTest.getInt(1) == 0) {
				System.out.println("Inserting...");
				addZone(z);
			} else {
				System.out.println("Updating...");
				updateZone(z);
			}

			System.out.println("Zone saved");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cleanZone() {
		this.connect();
		ResultSet resultTest = this
				.query("SELECT idElement FROM Zone");
		try {
			while(resultTest.next())
			{
				PreparedStatement preparedStatement;
				
					preparedStatement = connection
							.prepareStatement("DELETE FROM Zone WHERE idElement = ?");

					preparedStatement.setInt(1, resultTest.getInt("idElement"));
					preparedStatement.executeUpdate();
			}
			System.out.println("Table Zone is now empty");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.close();
	}
	
	public void initialiseZone(Zone z) {
		try {
			System.out.print("Initializating...");
			ResultSet resultSet = this
					.query("SELECT * FROM Zone WHERE idElement=" + z.getId());

			switch (resultSet.getString("type")) {
			case "CAVERN":
				z.setZoneType(ZoneType.CAVERN);
			case "DESERT":
				z.setZoneType(ZoneType.DESERT);
			case "MOUNTAIN":
				z.setZoneType(ZoneType.MOUNTAIN);
			case "PLAIN":
				z.setZoneType(ZoneType.PLAIN);
				;
			case "WATER":
				z.setZoneType(ZoneType.WATER);
			}

			z.setPosition(new Position2D(resultSet.getDouble("posX"),
					resultSet.getDouble("posY")));
			z.setWidth(resultSet.getDouble("width"));
			z.setHeight(resultSet.getDouble("height"));
			System.out.println("Done");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}