package data_management.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @license newBSD (http://directory.fsf.org/wiki/License:BSD_3Clause)
 */
public class Log
{
	public static String defaultPathLogFile = System.getProperty("user.home") +"/.history.txt";
	
	
	/**
	 * @return String content of the specified file
	 * @throws IOException
	 * @source https://stackoverflow.com/questions/326390/how-to-create-a-java-string-from-the-contents-of-a-file
	 */
	public static String readFile(String pathname) throws IOException
	{
		return new String(Files.readAllBytes(Paths.get(pathname)));
	}
	
	/**
	 * @return String content of the default file
	 * @throws IOException
	 */
	public static String readFile() throws IOException
	{
		return Log.readFile(Log.defaultPathLogFile);
	}
	
	/**
	 * @throws IOException
	 * @source https://stackoverflow.com/questions/1625234/how-to-append-text-to-an-existing-file-in-java
	 */
	public static void addToFile(String pathname, String textToAdd, boolean newline) throws IOException
	{
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(pathname, true)));
		
		out.print(textToAdd);
		if(newline)
			out.print("\n");
		
		out.close();
	}

	/**
	 * @throws IOException
	 */
	public static void addToFile(String pathname, String textToAdd) throws IOException
	{
		Log.addToFile(pathname, textToAdd, true);
	}

	/**
	 * @throws IOException
	 */
	public static void addToFile(String textToAdd, boolean newline) throws IOException
	{
		Log.addToFile(Log.defaultPathLogFile, textToAdd, newline);
	}

	/**
	 * @param String text to add
	 * @throws IOException
	 */
	public static void addToFile(String textToAdd) throws IOException
	{
		Log.addToFile(Log.defaultPathLogFile, textToAdd);
	}
	
	/**
	 * Delete the content of a specified file
	 * @param String path name of the file
	 * @throws IOException
	 */
	public static void deleteContentFile(String pathname) throws IOException
	{
		new FileWriter(new File(pathname)).close();
	}

	/**
	 * Delete the content of the default file
	 * @throws IOException
	 */
	public static void deleteContentFile() throws IOException
	{
		Log.deleteContentFile(Log.defaultPathLogFile);
	}
}