/*
 * Copyright (C) 2014, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package graphics.specific;

import graphics.utils.DefaultComboBoxModel2;
import core.environment.Environment;
import core.utils.SetWidth;

@SuppressWarnings("serial")
public class ComboBoxModelFilter extends DefaultComboBoxModel2<String>
{
	private Environment environment;
	
	
	/**
	 * Constructor
	 * @param Environment environment to filter
	 */
	public ComboBoxModelFilter(Environment environment)
	{
		super();
		this.environment = environment;
		this.update();
	}
	
	/**
	 * Default constructor
	 */
	public ComboBoxModelFilter()
	{
		this(null);
	}
	
	
	/**
	 * Update the model with environment values
	 */
	public void update()
	{
		int index = super.getSelectedIndex();
		if(index < 0)
			index = 0;
		
		super.removeAllElements();
		if(this.environment == null)
		{
			super.addElement("All");
			super.addElement("Vegetals");
			super.addElement("Animals");
			super.addElement("Herbivores");
			super.addElement("Carnivores");
		}
		else
		{
			int nbLivingBeings = this.environment.getNbLivingBeings();
			int nbDigits = String.valueOf(nbLivingBeings).length();
			super.addElement("Living beings ("+ nbLivingBeings                                                                   +"/"+ nbLivingBeings +")");
			super.addElement("Vegetals      ("+ SetWidth.left(String.valueOf(this.environment.getNbVegetals()),   nbDigits, '0') +"/"+ nbLivingBeings +")");
			super.addElement("Animals       ("+ SetWidth.left(String.valueOf(this.environment.getNbAnimals()),    nbDigits, '0') +"/"+ nbLivingBeings +")");
			super.addElement("Herbivores    ("+ SetWidth.left(String.valueOf(this.environment.getNbHerbivores()), nbDigits, '0') +"/"+ nbLivingBeings +")");
			super.addElement("Carnivores    ("+ SetWidth.left(String.valueOf(this.environment.getNbCarnivores()), nbDigits, '0') +"/"+ nbLivingBeings +")");
			// Probablement à cause de la police certains caractères sont plus longs que d'autres, du coup ce n'est pas aligné
		}
		
		super.setSelectedIndex(index);
	}
	
	public boolean showLivingBeings()
	{
		return super.getSelectedIndex() == 0;
	}
	
	public boolean showVegetals()
	{
		return this.showLivingBeings() || super.getSelectedIndex() == 1;
	}
	
	public boolean showAnimals()
	{
		return this.showLivingBeings() || super.getSelectedIndex() == 2;
	}
	
	public boolean showHerbivores()
	{
		return this.showAnimals() || super.getSelectedIndex() == 3;
	}
	
	public boolean showCarnivores()
	{
		return this.showAnimals() || super.getSelectedIndex() == 4;
	}
}
