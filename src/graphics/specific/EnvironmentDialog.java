package graphics.specific;

import graphics.specific.panel.PanelSpinners;
import graphics.utils.PanelSpinner;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import core.environment.Environment;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid)
 */
@SuppressWarnings("serial")
public class EnvironmentDialog extends JDialog
{
	public EnvironmentDialog(Frame owner, final Environment environment, final boolean resetEnvironment, boolean modal)
	{
		super(owner, modal);
		
		if(resetEnvironment)
			super.setTitle("New environment");
		else
			super.setTitle("Update environment");
		
		
		final PanelSpinner nbVegetalsPanel   = PanelSpinners.getForVegetals();
		this.add(nbVegetalsPanel);
		
		final PanelSpinner nbHerbivoresPanel = PanelSpinners.getForHerbivores();
		this.add(nbHerbivoresPanel);
		
		final PanelSpinner nbCarnivoresPanel = PanelSpinners.getForCarnivores();
		this.add(nbCarnivoresPanel);
		
		
		JButton buttonValidate = new JButton("Validate");
		buttonValidate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(resetEnvironment)
					environment.removeAll();
				boolean pause = environment.isPlay();
				if (pause) environment.togglePlay();
				environment.add(nbVegetalsPanel.getValueInt(), nbHerbivoresPanel.getValueInt(), nbCarnivoresPanel.getValueInt());
				if (pause) environment.togglePlay();
				dispose();
			}
		});
		
		JButton buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.add(buttonValidate);
		buttonsPanel.add(buttonCancel);
		this.add(buttonsPanel);
		
		
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.pack();
		this.setVisible(true);
	}
	
	public EnvironmentDialog(Frame owner, Environment environment, boolean resetEnvironment)
	{
		this(owner, environment, resetEnvironment, true);
	}
	
	public EnvironmentDialog(Frame owner, Environment environment)
	{
		this(owner, environment, false);
	}
}