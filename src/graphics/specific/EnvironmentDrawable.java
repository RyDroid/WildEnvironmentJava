package graphics.specific;

import graphics.specific.panel.FilterPanel;
import graphics.specific.panel.GraphicPanel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import core.environment.Element;
import core.environment.Environment;
import core.environment.Zone;
import core.living_being.Animal;
import core.living_being.Carnivore;
import core.living_being.FootStep;
import core.living_being.Herbivore;
import core.living_being.LivingBeing;
import core.living_being.Vegetal;
import core.utils.ImagePathGetter;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon
 */
public class EnvironmentDrawable extends Environment
{
	private GraphicPanel graphicPanel;
	private BufferedImage bufferedImage;
	private FilterPanel filterPanel;
	
	
	public EnvironmentDrawable(final GraphicPanel graphicPanel, final FilterPanel filterPanel, int nbVegatals, int nbHerbivores, int nbCarnivores)
	{
		this(graphicPanel, filterPanel);
		this.add(nbVegatals, nbHerbivores, nbCarnivores);
	}
	
	public EnvironmentDrawable(final GraphicPanel graphicPanel, final FilterPanel filterPanel, int nbVegatals, int nbAnimals)
	{
		this(graphicPanel, filterPanel);
		this.add(nbVegatals, nbAnimals);
	}
	
	
	public EnvironmentDrawable(final GraphicPanel graphicPanel, final FilterPanel filterPanel)
	{
		super();
		
		this.graphicPanel = graphicPanel;
		if (this.graphicPanel != null)
		{
			super.setWidth(graphicPanel.getWidth());
			super.setHeight(graphicPanel.getHeight());
		}
		this.bufferedImage = new BufferedImage(super.getWidth(), super.getHeight(), BufferedImage.TYPE_INT_ARGB);
		
		this.filterPanel = filterPanel;
		
		this.graphicPanel.setEnvironment(this);
	}
	
	
	public void setGraphicPanel(GraphicPanel graphicPanel)
	{
		if(graphicPanel != null)
			this.graphicPanel = graphicPanel;
	}
	
	
	public void setFilterPanel(final FilterPanel filterPanel)
	{
		if(filterPanel != null)
			this.filterPanel = filterPanel;
	}
	
	public GraphicPanel getPanel()
	{
		return this.graphicPanel;
	}
	
	public BufferedImage getBufferedImage()
	{
		return this.bufferedImage;
	}
	
	public Graphics getGraphics()
	{
		return this.bufferedImage.getGraphics();
	}
	
	public Graphics2D getGraphics2D()
	{
		return (Graphics2D) this.bufferedImage.getGraphics();
	}
	
	
	public void draw(final Graphics2D graphics, final Element element, final Color color)
	{
		if(element == null)
			System.out.println("draw: element is null !");
		else
		{
			Color initialColor = graphics.getColor();
			graphics.setColor(color);
			
			if(element instanceof LivingBeing)
				this.drawLivingBeing(graphics, (LivingBeing) element);
			else if(element instanceof Zone)
				this.drawZone(graphics, (Zone) element);
			else
				System.out.println("Unknow class/type: "+ element.getClass());
			
			graphics.setColor(initialColor);
		}
	}
	
	public void drawLivingBeing(final Graphics2D graphics, final LivingBeing livingBeing, boolean showStates)
	{
		if(livingBeing != null)
		{
			int x = (int) livingBeing.getPosition().getX();
			int y = (int) livingBeing.getPosition().getY();
			int size = (int) livingBeing.size();
			graphics.fillOval(x, y, size, size);
			
			if(showStates && livingBeing instanceof Animal)
			{
				Animal animal = (Animal) livingBeing;
				try {
					if(!animal.isAlive())
					{
						BufferedImage image = ImageIO.read(ImagePathGetter.get("state-dead.png", 22));
						graphics.drawImage(image, x, y, size/2, size/2, null);
					}
					else
					{
						if(!animal.isLeader())
						{
							BufferedImage image = ImageIO.read(ImagePathGetter.get("state-leader.png", 22));
							graphics.drawImage(image, x, y + size/2, size/2, size/2, null);
						}
						if(animal.isHungry())
						{
							BufferedImage image = ImageIO.read(ImagePathGetter.get("state-hungry.png", 22));
							graphics.drawImage(image, x + size/2, y, size/2, size/2, null);
						}
						if(animal.isThirsty())
						{
							BufferedImage image = ImageIO.read(ImagePathGetter.get("state-thirsty.png", 22));
							graphics.drawImage(image, x + size/2, y + size/2, size/2, size/2, null);
						}
						/*if(animal.isTired())
						{
							BufferedImage image = ImageIO.read(ImagePathGetter.get("state-tired.png", 22));
							graphics.drawImage(image, x, y, size/2, size/2, null);
						}
						if(animal.isReproducable())
						{
							BufferedImage image = ImageIO.read(ImagePathGetter.get("state-reproducable.png", 22));
							graphics.drawImage(image, x, y, size/2, size/2, null);
						}
						if(animal.isMale())
						{
							BufferedImage image = ImageIO.read(ImagePathGetter.get("male.png", 22));
							graphics.drawImage(image, x, y, size/2, size/2, null);
						}
						else
						{
							BufferedImage image = ImageIO.read(ImagePathGetter.get("female.png", 22));
							graphics.drawImage(image, x, y, size/2, size/2, null);
						}*/
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public void drawLivingBeing(final Graphics2D graphics, final LivingBeing livingBeing)
	{
		this.drawLivingBeing(graphics, livingBeing, true);
	}
	
	public void drawLivingBeings(final Graphics2D graphics, final LivingBeing[] livingBeings, boolean showStates)
	{
		for (LivingBeing livingBeing : livingBeings)
			this.drawLivingBeing(graphics, livingBeing, showStates);
	}
	
	public void drawLivingBeings(final Graphics2D graphics, final LivingBeing[] livingBeings)
	{
		this.drawLivingBeings(graphics, livingBeings, true);
	}
	
	public void drawLivingBeings(final Graphics2D graphics, final LivingBeing[] livingBeings, Color color, boolean showStates)
	{
		Color initialColor = graphics.getColor();
		graphics.setColor(color);
		
		this.drawLivingBeings(graphics, livingBeings, showStates);
		
		graphics.setColor(initialColor);
	}
	
	public void drawLivingBeings(final Graphics2D graphics, final LivingBeing[] livingBeings, Color color)
	{
		this.drawLivingBeings(graphics, livingBeings, color, true);
	}
	
	public void drawVegetals(final Graphics2D graphics, final Vegetal[] vegetals, boolean showStates)
	{
		this.drawLivingBeings(graphics, vegetals, new Color(10, 190, 65), showStates);
	}
	
	public void drawVegetals(Graphics2D graphics, Vegetal[] vegetals)
	{
		this.drawVegetals(graphics, vegetals, true);
	}
	
	public void drawHerbivores(final Graphics2D graphics, Herbivore[] herbivores, boolean showStates)
	{
		this.drawLivingBeings(graphics, herbivores, Color.YELLOW, showStates);
	}
	
	public void drawHerbivores(final Graphics2D graphics, Herbivore[] herbivores)
	{
		this.drawHerbivores(graphics, herbivores, true);
	}
	
	public void drawCarnivores(final Graphics2D graphics, Carnivore[] carnivores, boolean showStates)
	{
		this.drawLivingBeings(graphics, carnivores, Color.RED, showStates);
	}
	
	public void drawCarnivores(final Graphics2D graphics, Carnivore[] carnivores)
	{
		this.drawCarnivores(graphics, carnivores, true);
	}
	
	public void drawZone(final Graphics2D graphics, Zone zone)
	{
		if(zone != null)
		{
			graphics.fillRect(
					(int) zone.getPosition().getX(), (int) zone.getPosition().getY(),
					(int) zone.getWidth(), (int) zone.getHeight());
		}
	}
	
	public void drawZones(final Graphics2D graphics)
	{
		for (Zone zone : super.getZones())
		{
			switch (zone.getZoneType()) {
				case CAVERN  : this.draw(graphics, zone, new Color(112,  20,   1, 70)); break;
				case DESERT  : this.draw(graphics, zone, new Color(112,  20,   1, 70)); break;
				case MOUNTAIN: this.draw(graphics, zone, new Color( 85,  85,  73, 70)); break;
				case PLAIN   : this.draw(graphics, zone, new Color(  0, 210,  66, 70)); break;
				case WATER   : this.draw(graphics, zone, new Color( 69,  91, 255, 70)); break;
				default: System.out.println("Unknow zone type: "+ zone.getZoneType());
			}
		}
	}
	
	public void drawFootSteps(boolean showHerbivores, boolean showCarnivores)
	{
		BufferedImage image = new BufferedImage(super.getWidth(), super.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setColor(Color.BLACK);
		
		for (FootStep footStep : super.getFootSteps())
		{
			/*synchronized (footStep)
			{*/
			if(footStep != null)
			{
				if (showCarnivores && footStep.isACarnivorStep())
					graphics.fillOval(footStep.getX(), footStep.getY(), footStep.size(), footStep.size());
				else if (showHerbivores && !footStep.isACarnivorStep())
					graphics.fillOval(footStep.getX(), footStep.getY(), footStep.size(), footStep.size());
			}
			/*}*/
		}
		
		this.getGraphics2D().drawImage(image, null, null);
	}
	
	public void drawFootSteps(boolean showAnimals)
	{
		this.drawFootSteps(showAnimals, showAnimals);
	}
	
	public void drawFootSteps()
	{
		this.drawFootSteps(true);
	}
	
	public BufferedImage draw(boolean showVegetals, boolean showHerbivores, boolean showCarnivores, boolean showZones, boolean showFootSteps, boolean showStates)
	{
		Graphics2D graphics = this.getGraphics2D();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, this.graphicPanel.getWidth(), this.graphicPanel.getHeight());
		
		if(showZones)
			this.drawZones(graphics);
		if(showFootSteps)
			this.drawFootSteps(showHerbivores, showCarnivores);
		if(showVegetals)
			this.drawVegetals(graphics,   this.getVegetals(),   showStates);
		if(showHerbivores)
			this.drawHerbivores(graphics, this.getHerbivores(), showStates);
		if(showCarnivores)
			this.drawCarnivores(graphics, this.getCarnivores(), showStates);
		
		return this.bufferedImage;
	}
	
	public BufferedImage draw(boolean showVegetals, boolean showHerbivores, boolean showCarnivores, boolean showZones, boolean showFootSteps)
	{
		return this.draw(showVegetals, showHerbivores, showCarnivores, showZones, showFootSteps, true);
	}
	
	public BufferedImage draw(boolean showVegetals, boolean showHerbivores, boolean showCarnivores, boolean showZones)
	{
		return this.draw(showVegetals, showHerbivores, showCarnivores, showZones, true);
	}
	
	public BufferedImage draw(boolean showVegetals, boolean showHerbivores, boolean showCarnivores)
	{
		return this.draw(showVegetals, showHerbivores, showCarnivores, true);
	}
	
	public BufferedImage draw(boolean showVegetals, boolean showAnimals)
	{
		return this.draw(showVegetals, showAnimals, showAnimals);
	}
	
	public BufferedImage draw()
	{
		return this.draw(true, true);
	}
	
	
	@Override
	public void doInRun()
	{
		if(this.graphicPanel != null)
			this.graphicPanel.repaint();
		
		/*if(this.counterTime == 0 && this.filterPanel != null)
			this.filterPanel.update();*/
	}
}