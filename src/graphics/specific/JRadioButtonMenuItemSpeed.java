/*
 * Copyright (C) 2014, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package graphics.specific;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButtonMenuItem;

import core.environment.Environment;

@SuppressWarnings("serial")
public class JRadioButtonMenuItemSpeed extends JRadioButtonMenuItem
{
	public JRadioButtonMenuItemSpeed(String text, final Environment environment, final short timeBetweenThreadSleep)
	{
		super(text);
		super.setSelected(environment.getTimeBetweenThreadSleep() == timeBetweenThreadSleep);
		
		super.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				environment.setTimeBetweenThreadSleep(timeBetweenThreadSleep);
			}
		});
	}
}
