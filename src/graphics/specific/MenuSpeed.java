/*
 * Copyright (C) 2014, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package graphics.specific;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;

import core.environment.Environment;

@SuppressWarnings("serial")
public class MenuSpeed extends JMenu
{
	private ButtonGroup buttonGroup;
	
	public MenuSpeed(Environment environment, String text)
	{
		super(text);
		this.buttonGroup = new ButtonGroup();
		
		JRadioButtonMenuItemSpeed menuBarEditSpeedVeryZombie = new JRadioButtonMenuItemSpeed("Zombie", environment, Environment.TIME_BETWEEN_THREAD_SLEEP_ZOMBIE);
		this.add(menuBarEditSpeedVeryZombie);
		
		JRadioButtonMenuItemSpeed menuBarEditSpeedVerySlow = new JRadioButtonMenuItemSpeed("Very slow", environment, Environment.TIME_BETWEEN_THREAD_SLEEP_VERY_SLOW);
		this.add(menuBarEditSpeedVerySlow);
		
		JRadioButtonMenuItemSpeed menuBarEditSpeedSlow = new JRadioButtonMenuItemSpeed("Slow", environment, Environment.TIME_BETWEEN_THREAD_SLEEP_SLOW);
		this.add(menuBarEditSpeedSlow);
		
		JRadioButtonMenuItemSpeed menuBarEditSpeedNormal = new JRadioButtonMenuItemSpeed("Normal", environment, Environment.TIME_BETWEEN_THREAD_SLEEP_NORMAL);
		this.add(menuBarEditSpeedNormal);
		
		JRadioButtonMenuItemSpeed menuBarEditSpeedFast = new JRadioButtonMenuItemSpeed("Fast", environment, Environment.TIME_BETWEEN_THREAD_SLEEP_FAST);
		this.add(menuBarEditSpeedFast);
		
		JRadioButtonMenuItemSpeed menuBarEditSpeedVeryFast = new JRadioButtonMenuItemSpeed("Very Fast", environment, Environment.TIME_BETWEEN_THREAD_SLEEP_VERY_FAST);
		this.add(menuBarEditSpeedVeryFast);
		
		JRadioButtonMenuItemSpeed menuBarEditSpeedIncrediblyFast = new JRadioButtonMenuItemSpeed("Incredibly Fast", environment, Environment.TIME_BETWEEN_THREAD_SLEEP_INCREDIBLY_FAST);
		this.add(menuBarEditSpeedIncrediblyFast);
		
		JRadioButtonMenuItemSpeed menuBarEditSpeedLight = new JRadioButtonMenuItemSpeed("Lightspeed", environment, Environment.TIME_BETWEEN_THREAD_SLEEP_LIGHT);
		this.add(menuBarEditSpeedLight);
	}
	
	public MenuSpeed(Environment environment)
	{
		this(environment, "Speed");
	}
	
	
	private void add(JRadioButtonMenuItemSpeed item)
	{
		super.add(item);
		this.buttonGroup.add(item);
	}
}
