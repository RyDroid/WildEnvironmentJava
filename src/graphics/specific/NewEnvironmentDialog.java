package graphics.specific;

import graphics.specific.frame.MainWindowEnvironment;
import graphics.specific.panel.PanelSpinners;
import graphics.utils.PanelSpinner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * @author Nicola Spanti (RyDroid), Nicolas Foulon
 */
@SuppressWarnings("serial")
public class NewEnvironmentDialog extends JDialog
{
	private static ExecutorService pool = Executors.newSingleThreadExecutor();
	
	
	public NewEnvironmentDialog()
	{
		super.setTitle("New environment");
		super.setLocationRelativeTo(null);
		
		final PanelSpinner nbVegetalsPanel   = PanelSpinners.getForVegetals  (100);
		this.add(nbVegetalsPanel);
		
		final PanelSpinner nbHerbivoresPanel = PanelSpinners.getForHerbivores( 50);
		this.add(nbHerbivoresPanel);
		
		final PanelSpinner nbCarnivoresPanel = PanelSpinners.getForCarnivores(  5);
		this.add(nbCarnivoresPanel);
		
		
		JButton buttonValidate = new JButton("Validate");
		buttonValidate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						MainWindowEnvironment frame = new MainWindowEnvironment(
								nbVegetalsPanel.getValueInt(), nbHerbivoresPanel.getValueInt(), nbCarnivoresPanel.getValueInt());
						pool.submit(frame.environment);
					}
				});
				
			}
		});
		
		JButton buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.add(buttonValidate);
		buttonsPanel.add(buttonCancel);
		this.add(buttonsPanel);
		
		
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.pack();
		this.setVisible(true);
	}
}