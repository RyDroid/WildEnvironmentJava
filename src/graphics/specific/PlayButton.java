package graphics.specific;

import graphics.specific.listener.ActionListenerPlayMultiple;

import javax.swing.JButton;

import core.environment.Environment;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon
 */
@SuppressWarnings("serial")
public class PlayButton extends JButton
{
	private Environment environment;
	
	
	public PlayButton(final Environment environment)
	{
		super("Stop");
		
		this.environment = environment;
		this.addActionListener(new ActionListenerPlayMultiple(this.environment, this));
	}
}
