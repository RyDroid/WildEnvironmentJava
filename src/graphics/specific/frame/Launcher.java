package graphics.specific.frame;

import graphics.specific.NewEnvironmentDialog;
import graphics.specific.listener.ActionListenerLicense;
import graphics.utils.ImageIconUtils;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import core.utils.ImagePathGetter;

/**
 * @author Nicolas Foulon, Nicola Spanti (RyDroid), Alexi Coard
 */
@SuppressWarnings("serial")
public class Launcher extends JFrame
{
	public Launcher(String title)
	{
		super(title);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final ImageIcon icon16 = ImageIconUtils.create("icon.png", 16);
		if(icon16 != null)
		{
			super.setIconImage(icon16.getImage());
		}
		
		this.setSize(new Dimension(400, 400));
		this.setLocationRelativeTo(null);
		JButton launchNewEnvironment = new JButton("Launch new environment");
		launchNewEnvironment.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new NewEnvironmentDialog();
				dispose();
			}
		});
		
		
		JButton launchExistingEnvironment = new JButton("Launch existing environment");
		launchExistingEnvironment.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final MainWindowEnvironment m = new MainWindowEnvironment();
				m.environment.load();
				m.environment.run();
			}
		});
		JButton license = new JButton("License");
		license.addActionListener(new ActionListenerLicense(this));
		
		Icon icon = ImageIconUtils.create(ImagePathGetter.get("launcher.jpg", "others"));
		JLabel topLabel = icon == null ? new JLabel() : new JLabel(icon);
		this.add(launchNewEnvironment, BorderLayout.WEST);
		this.add(launchExistingEnvironment, BorderLayout.CENTER);
		this.add(license,  BorderLayout.EAST);
		this.add(topLabel, BorderLayout.NORTH);
		//launchNewEnvironment.setSize(15, 50);
		this.setResizable(false);
		this.setVisible(true);
		this.pack();
	}
	
	public Launcher()
	{
		this("Animal life simulation");
	}
	
	
	public static final void main(final String[] args)
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				@SuppressWarnings("unused")
				final Launcher launcher = new Launcher();
			}
		});
	}
}
