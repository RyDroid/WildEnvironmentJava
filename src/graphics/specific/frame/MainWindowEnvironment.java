package graphics.specific.frame;

import graphics.specific.EnvironmentDialog;
import graphics.specific.EnvironmentDrawable;
import graphics.specific.MenuSpeed;
import graphics.specific.listener.ActionListenerLicense;
import graphics.specific.listener.ActionListenerPlayMultiple;
import graphics.specific.panel.FilterPanel;
import graphics.specific.panel.GraphicPanel;
import graphics.utils.ImageIconUtils;
import graphics.utils.JCheckBoxMenuItemShower;
import graphics.utils.LogWindow;
import graphics.utils.MainWindow;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import data_management.utils.Log;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon, Jérémy Bonnardel, Anthony Demongeot, Alexi Coard
 */
@SuppressWarnings("serial")
public class MainWindowEnvironment extends MainWindow
{
	private static final ExecutorService pool = Executors.newSingleThreadExecutor();

	public  EnvironmentDrawable environment;
	private GraphicPanel graphicPanel;
	private FilterPanel filterPanel;
	
	
	/**
	 * Create the frame.
	 */
	public MainWindowEnvironment(int width, int height, int nbVegetals, int nbHerbivores, int nbCarnivores)
	{
		super("Wild Environment");
		this.setSize(width, height);
		
		final ImageIcon icon16 = ImageIconUtils.create("icon.png", 16);
		if(icon16 != null)
		{
			super.setIconImage(icon16.getImage());
		}
		
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				exit();
			}
		});
		
		
		this.graphicPanel = new GraphicPanel(this);
		this.setSize(graphicPanel.getWidth()+230, graphicPanel.getHeight()+50);
		
		this.environment = new EnvironmentDrawable(
				this.graphicPanel, null , nbVegetals, nbHerbivores, nbCarnivores);
		this.environment.createZone(85); // TODO rm magic value
		
		JMenuBar menuBar = new JMenuBar();
		super.setJMenuBar(menuBar);
		
		JMenu menuBarFile = new JMenu("File");
		menuBar.add(menuBarFile);
		
		JMenuItem menuBarFileItemNew = new JMenuItem("New");
		menuBarFileItemNew.setIcon(ImageIconUtils.create("document-new.png", 16));
		menuBarFileItemNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_MASK));
		menuBarFile.add(menuBarFileItemNew);
		
		JMenuItem menuBarFileItemAdd = new JMenuItem("Add");
		menuBarFileItemAdd.setIcon(ImageIconUtils.create("list-add.png", 16));
		menuBarFileItemAdd.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_MASK));
		menuBarFile.add(menuBarFileItemAdd);
		menuBarFile.addSeparator();
		
		final JMenuItem menuBarFileItemPausePlay = new JMenuItem("Pause");
		menuBarFileItemPausePlay.setIcon(ImageIconUtils.create("media-playback-pause.png", 16));
		menuBarFileItemPausePlay.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.CTRL_MASK));
		menuBarFileItemPausePlay.addActionListener(new ActionListenerPlayMultiple(this.environment, menuBarFileItemPausePlay));
		menuBarFile.add(menuBarFileItemPausePlay);
		menuBarFile.addSeparator();
		
		JMenuItem menuBarFileItemOpen = new JMenuItem("Open");
		menuBarFileItemOpen.setIcon(ImageIconUtils.create("document-open.png", 16));
		menuBarFileItemOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_MASK));
		menuBarFile.add(menuBarFileItemOpen);
		
		JMenuItem menuBarFileItemSave = new JMenuItem("Save");
		menuBarFileItemSave.setIcon(ImageIconUtils.create("document-save.png", 16));
		menuBarFileItemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK));
		menuBarFile.add(menuBarFileItemSave);
		menuBarFile.addSeparator();
		
		JMenuItem menuBarFileItemPrint = new JMenuItem("Print");
		menuBarFileItemPrint.setIcon(ImageIconUtils.create("document-print.png", 16));
		menuBarFileItemPrint.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_MASK));
		menuBarFile.add(menuBarFileItemPrint);
		menuBarFile.addSeparator();
		
		JMenuItem menuBarFileItemQuit = new JMenuItem("Quit");
		menuBarFileItemQuit.setIcon(ImageIconUtils.create("process-stop.png", 16));
		menuBarFileItemQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_MASK));
		menuBarFile.add(menuBarFileItemQuit);
		
		JMenu menuBarEdit = new JMenu("Edit");
		menuBar.add(menuBarEdit);
		
		menuBarEdit.add(new MenuSpeed(this.environment));
		
		JMenu menuBarHistory = new JMenu("History");
		menuBar.add(menuBarHistory);
		
		JMenuItem menuBarHistoryItemShow = new JMenuItem("Show");
		menuBarHistoryItemShow.setIcon(ImageIconUtils.create("edit-find.png", 16));
		menuBarHistoryItemShow.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, KeyEvent.CTRL_MASK));
		menuBarHistory.add(menuBarHistoryItemShow);
		
		JMenuItem menuBarHistoryItemSaveAs = new JMenuItem("Save as");
		menuBarHistoryItemSaveAs.setIcon(ImageIconUtils.create("document-save-as.png", 16));
		menuBarHistory.add(menuBarHistoryItemSaveAs);
		
		JMenuItem menuBarHistoryItemClear = new JMenuItem("Clear");
		menuBarHistoryItemClear.setIcon(ImageIconUtils.create("edit-clear.png", 16));
		menuBarHistory.add(menuBarHistoryItemClear);
		
		JMenu menuBarView = new JMenu("View");
		menuBar.add(menuBarView);
		
		JMenu menuBarHelp = new JMenu("Help");
		menuBar.add(menuBarHelp);
		
		JMenuItem menuBarHelpItemHelp = new JMenuItem("Help");
		menuBarHelpItemHelp.setIcon(ImageIconUtils.create("dialog-information.png", 16));
		menuBarHelpItemHelp.setAccelerator(KeyStroke.getKeyStroke("F1"));
		menuBarHelp.add(menuBarHelpItemHelp);
		
		JMenuItem menuBarHelpItemAbout = new JMenuItem("About");
		menuBarHelpItemAbout.setIcon(ImageIconUtils.create("help.png", 16));
		menuBarHelp.add(menuBarHelpItemAbout);
		
		JMenuItem menuBarHelpItemLicense = new JMenuItem("License");
		menuBarHelpItemLicense.setIcon(ImageIconUtils.create("help.png", 16));
		menuBarHelp.add(menuBarHelpItemLicense);
		
		// creating the toolbar
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setVisible(true);
		toolBar.setOrientation(SwingConstants.VERTICAL);
		
		JButton toolBarButtonNew = new JButton();
		toolBarButtonNew.setToolTipText("New");
		toolBarButtonNew.setIcon(ImageIconUtils.create("document-new.png", 22));
		toolBar.add(toolBarButtonNew);
		
		JButton toolBarButtonAdd = new JButton();
		toolBarButtonAdd.setToolTipText("Add");
		toolBarButtonAdd.setIcon(ImageIconUtils.create("list-add.png", 22));
		toolBar.add(toolBarButtonAdd);
		toolBar.addSeparator();
		
		JButton toolBarButtonOpen = new JButton();
		toolBarButtonOpen.setToolTipText("Open");
		toolBarButtonOpen.setIcon(ImageIconUtils.create("document-open.png", 22));
		toolBar.add(toolBarButtonOpen);
		
		JButton toolBarButtonSave = new JButton();
		toolBarButtonSave.setToolTipText("Save");
		toolBarButtonSave.setIcon(ImageIconUtils.create("document-save.png", 22));
		toolBar.add(toolBarButtonSave);
		toolBar.addSeparator();
		
		JButton toolBarButtonShowHistory = new JButton();
		toolBarButtonShowHistory.setToolTipText("Show history");
		toolBarButtonShowHistory.setIcon(ImageIconUtils.create("edit-find.png", 22));
		toolBar.add(toolBarButtonShowHistory);
		
		JPanel contentPane = new JPanel();
		this.setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		// adding the toolbar
		contentPane.add(toolBar);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane);
		scrollPane.setViewportView(this.graphicPanel);
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		this.filterPanel = new FilterPanel(this.environment);
		this.graphicPanel.setFilterPanel(filterPanel);
		panel.add(filterPanel);
		this.environment.setFilterPanel(filterPanel);
		
		JCheckBoxMenuItem menuBarViewCheckFilters = new JCheckBoxMenuItemShower(filterPanel, "Filters");
		menuBarView.add(menuBarViewCheckFilters);
		
		JCheckBoxMenuItem menuBarViewCheckToolBar = new JCheckBoxMenuItemShower(toolBar, "Toolbar");
		menuBarView.add(menuBarViewCheckToolBar);
		
		ActionListener actionListenerNew = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new EnvironmentDialog((Frame) getParent(), environment, true);
				filterPanel.update(); // TODO do it in EnvironmentDialog
				try {
					Log.deleteContentFile();
				} catch (final IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		};
		menuBarFileItemNew.addActionListener(actionListenerNew);
		toolBarButtonNew.addActionListener(actionListenerNew);
		
		ActionListener actionListenerAdd = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new EnvironmentDialog((Frame) getParent(), environment, false);
				filterPanel.update(); // TODO le faire dans EnvironmentDialog
			}
		};
		menuBarFileItemAdd.addActionListener(actionListenerAdd);
		toolBarButtonAdd.addActionListener(actionListenerAdd);
		
		menuBarFileItemPrint.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PrinterJob printJob = PrinterJob.getPrinterJob();
				//le dialogue d’impression du système
				if (printJob.printDialog())
				{
					PageFormat pageFormat = printJob.defaultPage();
					printJob.setPrintable(null, pageFormat); // TODO remplacer le null par l'objet à imprimer
					try {
						printJob.print();
					} catch (final PrinterException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		
		ActionListener actionListenerOpen = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO
				/*JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileFilter();
				
				int answer = fileChooser.showOpenDialog(getParent());
				
				if (answer == JFileChooser.APPROVE_OPTION)
					this.environment.load();*/
			}
		};
		menuBarFileItemOpen.addActionListener(actionListenerOpen);
		toolBarButtonOpen.addActionListener(actionListenerOpen);
		
		ActionListener actionListenerSave = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new JDialog((Frame) getParent(), "Saving", false); // TODO does not work
				dialog.add(new JLabel("Saving...\nThis can take some minutes."));
				dialog.setVisible(true);
				dialog.pack();
				environment.save();
				dialog.dispose();
			}
		};
		menuBarFileItemSave.addActionListener(actionListenerSave);
		toolBarButtonSave.addActionListener(actionListenerSave);
		
		menuBarFileItemQuit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				exit();
			}
		});
		
		ActionListener actionListenerShowHistory = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final LogWindow logWindow = new LogWindow("Action History", true);
				logWindow.setLocation(MainWindowEnvironment.this.getWidth(), MainWindowEnvironment.this.getY());
				logWindow.setIconImage(ImageIconUtils.create("edit-find.png", 16).getImage());
				logWindow.showInfo();
			}
		};
		menuBarHistoryItemShow.addActionListener(actionListenerShowHistory);
		toolBarButtonShowHistory.addActionListener(actionListenerShowHistory);
		
		menuBarFileItemOpen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				environment.load();
			}
		});
		
		menuBarHistoryItemSaveAs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser filesaver = new JFileChooser();
				int answer = filesaver.showSaveDialog(getParent());
				File file = new File(filesaver.getCurrentDirectory().getPath().toString() + "/" + filesaver.getSelectedFile().getName());
				
				filesaver.setSelectedFile(file); // will create the file
				if (answer == JFileChooser.APPROVE_OPTION)
				{
					try {
						// TODO MS Excel, WTF?
						final FileOutputStream exportExcel = new FileOutputStream(filesaver.getSelectedFile().getPath());
						Log.addToFile(file.getPath().toString(),Log.readFile());
						exportExcel.close();
					} catch (final FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (final IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		
		menuBarHistoryItemClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Log.deleteContentFile();
				} catch (final IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		menuBarHelpItemHelp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(
						getParent(),
						"Some neurons and connections between them are probably missing...",
						"Help of this program",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		menuBarHelpItemAbout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(
						getParent(),
						"This program simulates living beings that live and think. Enjoy.\nThe program is written in the Java language with Swing UI. The database management system is SQLite.",
						"About this program",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		menuBarHelpItemLicense.addActionListener(new ActionListenerLicense(this));
		
		this.graphicPanel.repaint();
		//super.pack();
	}
	
	public MainWindowEnvironment(int width, int height)
	{
		this(width, height, 0, 0, 0);
	}
	
	public MainWindowEnvironment(int nbVegetals, int nbHerbivores, int nbCarnivores)
	{
		this(750, 650, nbVegetals, nbHerbivores, nbCarnivores);
	}
	
	public MainWindowEnvironment()
	{
		this(0, 0, 0);
	}
	
	
	public FilterPanel getFilterPanel()
	{
		return this.filterPanel;
	}
	
	
	/**
	 * Launch the main window with an environment.
	 */
	public static final void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				final MainWindowEnvironment frame = new MainWindowEnvironment();
				pool.submit(frame.environment);
			}
		});
	}
}
