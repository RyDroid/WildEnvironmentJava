/*
 * Copyright (C) 2014, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package graphics.specific.listener;

import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JOptionPane;

public class ActionListenerLicense implements ActionListener
{
	private Component parentComponent;
	
	public ActionListenerLicense(final Component parentComponent)
	{
		this.parentComponent = parentComponent;
	}
	
	@Override
	public void actionPerformed(final ActionEvent event)
	{
		String message = "";
		try
		{
			message = new String(Files.readAllBytes(Paths.get("LICENSE")));
		}
		catch (HeadlessException | IOException e)
		{}
		if(message.isEmpty())
			message = "The license file seems to have disappeared.\nHe has probably be eaten by a carnivorous developer.";
		
		JOptionPane.showMessageDialog(
				this.parentComponent,
				message,
				"License of this program",
				JOptionPane.INFORMATION_MESSAGE);
	}
}
