/*
 * Copyright (C) 2014, 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package graphics.specific.listener;

import graphics.utils.ImageIconUtils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;

import core.environment.Environment;

public class ActionListenerPlay implements ActionListener
{
	private Environment environment;
	private AbstractButton button;
	
	
	public ActionListenerPlay(final Environment environment, final AbstractButton button)
	{
		this.environment = environment;
		this.button = button;
		this.update();
	}
	
	
	private void update()
	{
		if(this.environment != null && this.button != null)
		{
			if(this.environment.isPlay())
			{
				this.button.setText("Stop");
				this.button.setIcon(ImageIconUtils.create("media-playback-pause.png", 16));
			}
			else
			{
				this.button.setText("Play");
				this.button.setIcon(ImageIconUtils.create("media-playback-start.png", 16));
			}
		}
	}
	
	@Override
	public void actionPerformed(final ActionEvent e)
	{
		if(this.environment != null)
			this.environment.togglePlay();
		this.update();
	}
}
