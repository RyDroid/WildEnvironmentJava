/*
 * Copyright (C) 2014, 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package graphics.specific.listener;

import graphics.utils.ImageIconUtils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.AbstractCollection;
import java.util.ArrayList;

import javax.swing.AbstractButton;

import core.environment.Environment;

public class ActionListenerPlayMultiple implements ActionListener
{
	private static Environment environment;
	private static AbstractCollection<AbstractButton> buttons;
	
	
	public ActionListenerPlayMultiple(final Environment environment, final AbstractCollection<AbstractButton> buttons)
	{
		ActionListenerPlayMultiple.environment = environment;
		
		if(ActionListenerPlayMultiple.buttons == null)
			ActionListenerPlayMultiple.buttons = buttons;
		else
			ActionListenerPlayMultiple.buttons.addAll(buttons);
		this.update();
	}
	
	public ActionListenerPlayMultiple(final Environment environment, final AbstractButton button)
	{
		this(environment, new ArrayList<AbstractButton>());
		this.add(button);
	}
	
	public ActionListenerPlayMultiple(final AbstractButton button)
	{
		this.add(button);
	}
	
	public ActionListenerPlayMultiple()
	{}
	
	
	public void add(final AbstractButton button)
	{
		buttons.add(button);
		this.update();
	}
	
	private void update()
	{
		if(environment != null && buttons != null)
		{
			if(environment.isPlay())
			{
				for(final AbstractButton button : buttons)
				{
					button.setText("Stop");
					button.setIcon(ImageIconUtils.create("media-playback-pause.png", 16));
				}
			}
			else
			{
				for(final AbstractButton button : buttons)
				{
					button.setText("Play");
					button.setIcon(ImageIconUtils.create("media-playback-start.png", 16));
				}
			}
		}
	}
	
	@Override
	public void actionPerformed(final ActionEvent e)
	{
		if(environment != null)
			environment.togglePlay();
		this.update();
	}
}
