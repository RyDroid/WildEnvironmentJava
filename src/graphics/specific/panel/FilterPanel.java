package graphics.specific.panel;

import graphics.specific.ComboBoxModelFilter;
import graphics.specific.EnvironmentDrawable;
import graphics.specific.PlayButton;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import core.environment.Element;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Nicolas Foulon, Anthony Demongeot
 */
@SuppressWarnings("serial")
public class FilterPanel extends JPanel
{
	private EnvironmentDrawable environment;
	private ComboBoxModelFilter comboBoxModelFilter;
	private JCheckBox checkboxStates;
	private JCheckBox checkboxFootSteps;
	private JCheckBox checkboxZones;
	private JTextArea textArea;
	
	public FilterPanel(final EnvironmentDrawable environment)
	{
		super();
		
		this.environment = environment;
		super.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		this.comboBoxModelFilter = new ComboBoxModelFilter(this.environment);
		this.update();
		final JComboBox<String> comboBoxFilters = new JComboBox<String>(this.comboBoxModelFilter);
		comboBoxFilters.setMaximumSize(new Dimension(200, 20));
		comboBoxFilters.setToolTipText("Filter");
		comboBoxFilters.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				environment.getPanel().setIndexCombobox(comboBoxFilters.getSelectedIndex());
				environment.getPanel().paintComponent(environment.getBufferedImage().getGraphics());
			}
		});
		super.add(comboBoxFilters);
		
		this.checkboxStates = new JCheckBox("States");
		this.checkboxStates.setSelected(false);
		this.add(this.checkboxStates);
		
		this.checkboxFootSteps = new JCheckBox("Foot steps");
		this.checkboxFootSteps.setSelected(true);
		this.add(this.checkboxFootSteps);
		
		this.checkboxZones = new JCheckBox("Zones");
		this.checkboxZones.setSelected(true);
		this.add(this.checkboxZones);
		
		/*JTextPane textPane = new JTextPane();
		textPane.setEnabled(false);
		textPane.setEditable(false);
		textPane.setMaximumSize(new Dimension(200, 1000));
		super.add(textPane);*/
		this.textArea = new JTextArea();
		this.textArea.setMaximumSize(new Dimension(200,1000)); //TODO manage the size
		this.textArea.setEditable(false);
		this.add(textArea);
		this.add(new PlayButton(environment));
	}
	
	
	public boolean showLivingBeings()
	{
		return this.comboBoxModelFilter.showLivingBeings();
	}
	
	public boolean showVegetals()
	{
		return this.comboBoxModelFilter.showVegetals();
	}
	
	public boolean showAnimals()
	{
		return this.comboBoxModelFilter.showAnimals();
	}
	public boolean showHerbivores()
	{
		return this.comboBoxModelFilter.showHerbivores();
	}
	
	public boolean showCarnivores()
	{
		return this.comboBoxModelFilter.showCarnivores();
	}
	
	public boolean showStates()
	{
		return this.checkboxStates.isSelected();
	}
	
	public boolean showFootSteps()
	{
		return this.checkboxFootSteps.isSelected();
	}
	
	public boolean showZones()
	{
		return this.checkboxZones.isSelected();
	}
	
	public void setDescription(Element e)
	{
		this.textArea.setText(e.toString());
	}
	
	public void update()
	{
		this.comboBoxModelFilter.update();
	}
}