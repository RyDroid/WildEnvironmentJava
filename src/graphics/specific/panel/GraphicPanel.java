package graphics.specific.panel;

import graphics.specific.EnvironmentDrawable;
import graphics.specific.frame.MainWindowEnvironment;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import core.environment.Element;
import core.environment.Environment;
import core.environment.Zone;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Anthony Demongeot, Nicolas Foulon, Jérémy Bonnardel
 */
@SuppressWarnings("serial")
public class GraphicPanel extends JPanel
{
	private EnvironmentDrawable environment;
	private int indexCombo;
	private FilterPanel filterPanel;
	
	
	public GraphicPanel(int width, int height)
	{
		super();
		super.setSize(width, height);
	}
	
	public GraphicPanel(final FilterPanel fp, final Environment environment)
	{
		this(environment.getWidth(), environment.getHeight());
		
	}
	
	public GraphicPanel(final MainWindowEnvironment mw)
	{
		this(mw.getWidth(), mw.getHeight());
	}
	
	
	public int getIndexCombobox()
	{
		return this.indexCombo;
	}
	
	public void setIndexCombobox(int index)
	{
		this.indexCombo = index;
	}
	
	public void setEnvironment(final EnvironmentDrawable environment)
	{
		if(environment == null)
			System.out.println("environment must not be null");
		else
			this.environment = environment;
	}
	
	public void setFilterPanel(final FilterPanel filterPanel)
	{
		this.filterPanel = filterPanel;
		
		this.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				if(environment != null /*&& filterPanel != null*/)
				{
					for (Element element : environment.getElements())
					{
						if(element.getClass() != Zone.class)
						{
							if(e.getX() >= element.getPosition().getX()
									&& e.getY() >= element.getPosition().getY()
									&& e.getX() <= element.getPosition().getX() + element.getWidth()
									&& e.getY() <= element.getPosition().getY() + element.getHeight())
							{
								GraphicPanel.this.filterPanel.setDescription(element);
							}
						}
					}
				}
			}
		});
	}
	
	@Override
	public void paintComponent(Graphics graphics)
	{
		super.paintComponent(graphics);
		if(this.filterPanel != null)
		{
			BufferedImage image = this.environment.draw(
					this.filterPanel.showVegetals(), this.filterPanel.showHerbivores(), this.filterPanel.showCarnivores(),
					this.filterPanel.showZones(), this.filterPanel.showFootSteps(), this.filterPanel.showStates());
			graphics.drawImage(image, 0, 0, this);
		}
		this.repaint();
	}
}