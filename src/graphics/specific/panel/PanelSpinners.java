/*
 * Copyright (C) 2014, Nicola Spanti (also known as RyDroid) <rydroid_dev@yahoo.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package graphics.specific.panel;

import graphics.utils.PanelSpinner;

public class PanelSpinners
{
	public static PanelSpinner getForVegetals(int value)
	{
		return new PanelSpinner("Number of vegetals",    value, 0, 2000, 2);
	}
	
	public static PanelSpinner getForVegetals()
	{
		return PanelSpinners.getForVegetals(0);
	}
	
	public static PanelSpinner getForHerbivores(int value)
	{
		return new PanelSpinner("Number of herbivores",  value, 0,  500, 1);
	}
	
	public static PanelSpinner getForHerbivores()
	{
		return PanelSpinners.getForHerbivores(0);
	}
	
	public static PanelSpinner getForCarnivores(int value)
	{
		return new PanelSpinner("Number of carnivores",  value, 0,  200, 1);
	}
	
	public static PanelSpinner getForCarnivores()
	{
		return PanelSpinners.getForCarnivores(0);
	}
}