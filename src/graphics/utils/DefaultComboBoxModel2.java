/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     
 *     (2) Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *     
 *     (3) The name of the author(s) may not be used to
 *     endorse or promote products derived from this software without
 *     specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


package graphics.utils;

import javax.swing.DefaultComboBoxModel;

@SuppressWarnings("serial")
public class DefaultComboBoxModel2<E> extends DefaultComboBoxModel<E>
{
	/**
	 * Returns the selected index.
	 * @return int selected index
	 */
	public int getSelectedIndex()
	{
		return super.getIndexOf(super.getSelectedItem());
	}
	
	/**
	 * Set the given index.
	 * @param int index to select
	 */
	public void setSelectedIndex(int index)
	{
		super.setSelectedItem(super.getElementAt(index));
	}
	
	/**
	 * Add elements to the model.
	 * @param E[] elements to add
	 */
	public void addElements(E[] elements)
	{
		if(elements != null)
		{
			for(E element : elements)
			{
				super.addElement(element);
			}
		}
	}
	/**
	 * Remove all elements and add new elements to the model.
	 * @param E[] new elements of the model
	 */
	public void changeElements(E[] elements)
	{
		super.removeAllElements();
		this.addElements(elements);
	}
	
	public void insertElements(E[] elements, int startAt)
	{
		for(E element : elements)
		{
			super.insertElementAt(element, startAt++);
		}
	}
	
	public void insertElements(E[] elements)
	{
		this.insertElements(elements, 0);
	}
}