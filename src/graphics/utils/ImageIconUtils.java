/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     
 *     (2) Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *     
 *     (3) The name of the author(s) may not be used to
 *     endorse or promote products derived from this software without
 *     specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


package graphics.utils;

import java.net.URL;

import javax.swing.ImageIcon;

import core.utils.ImagePathGetter;


public final class ImageIconUtils
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private ImageIconUtils() {}
	
	
	public static ImageIcon create(final String name, final String extension, int width, int height)
	{
		return create(ImagePathGetter.get(name, extension, width, height));
	}
	
	public static ImageIcon create(final String name, final String extension, int size)
	{
		return create(ImagePathGetter.get(name +"."+ extension, size, size));
	}
	
	public static ImageIcon create(final String name, final String folder)
	{
		return create(ImagePathGetter.get(name, folder));
	}
	
	public static ImageIcon create(final String name, int width, int height)
	{
		return create(ImagePathGetter.get(name, width, height));
	}
	
	public static ImageIcon create(final String name, int size)
	{
		return create(ImagePathGetter.get(name, size, size));
	}
	
	public static ImageIcon create(final String path)
	{
		return create(ImagePathGetter.get(path));
	}
	
	public static ImageIcon create(final URL url)
	{
		return url == null ? null : new ImageIcon(url);
	}
}
