/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *     
 *     (2) Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *     
 *     (3) The name of the author(s) may not be used to
 *     endorse or promote products derived from this software without
 *     specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


package graphics.utils;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;


@SuppressWarnings("serial")
public class JCheckBoxMenuItemShower extends JCheckBoxMenuItem
{
	/**
	 * Constructor.
	 * @param Component component to show or not
	 */
	public JCheckBoxMenuItemShower(final Component componentToShowOrNot)
	{
		super();
		if(componentToShowOrNot != null)
		{
			this.setSelected(componentToShowOrNot.isVisible());
			super.addActionListener(
					new ActionListenerShowerCheckbox(componentToShowOrNot, this));
		}
	}
	
	/**
	 * Constructor.
	 * @param Component component to show or not
	 * @param String text of the checkBox item
	 */
	public JCheckBoxMenuItemShower(final Component componentToShowOrNot, String text)
	{
		this(componentToShowOrNot);
		super.setText(text);
	}
	
	/**
	 * Constructor.
	 * @param Component component to show or not
	 * @param Icon icon the checkBox item
	 */
	public JCheckBoxMenuItemShower(final Component componentToShowOrNot, Icon icon)
	{
		this(componentToShowOrNot);
		super.setIcon(icon);
	}
	
	/**
	 * Constructor.
	 * @param Component component to show or not
	 * @param String text of the checkBox item
	 * @param Icon icon the checkBox item
	 */
	public JCheckBoxMenuItemShower(final Component componentToShowOrNot, String text, Icon icon)
	{
		this(componentToShowOrNot);
		super.setText(text);
		super.setIcon(icon);
	}
}