package graphics.utils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

import data_management.utils.Log;

/**
 * @author Labiod Jason, Nicola Spanti (RyDroid), Jérémy Bonnardel, Nicolas Foulon, Anthony Demongeot
 */
@SuppressWarnings("serial")
public class LogWindow extends JFrame
{
	private JTextArea textArea;
	private JScrollPane pane;
	
	/**
	 * Constructor.
	 * @param String title of the window
	 * @param int width of the window
	 * @param int height of the window
	 * @param boolean visible or not
	 */
	public LogWindow(final String title, int width, int height, boolean isVisible)
	{
		this(title, width, height);
		super.setVisible(isVisible);
	}
	
	/**
	 * Constructor.
	 * @param String title of the window
	 * @param int width of the window
	 * @param int height of the window
	 */
	public LogWindow(final String title, int width, int height)
	{
		super(title);
		super.setSize(width, height);
		
		// creation de la BoxLayout
		this.setLayout(new BoxLayout(this.getContentPane(),BoxLayout.Y_AXIS));
		
		// creation de la ToolBar
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setVisible(true);
		this.add(toolBar);
		
		// creation du bouton d'actualisation
		JButton buttonRefresh = new JButton("Refresh");
		buttonRefresh.setIcon(ImageIconUtils.create("view-refresh.png", 16));
		toolBar.add(buttonRefresh);
		
		// creation de la zone de texte
		this.textArea = new JTextArea();
		this.pane = new JScrollPane(textArea);
		this.textArea.setEditable(false);
		this.add(pane);
		
		// action sur le bouton d'actualisation
		buttonRefresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showInfo();
			}
		});
		
		//this.pack();
	}
	
	/**
	 * Constructor.
	 * @param int width of the window
	 * @param int height of the window
	 * @param boolean visible or not
	 */
	public LogWindow(int width, int height)
	{
		this("History");
	}
	
	/**
	 * Constructor.
	 * @param String title of the window
	 * @param boolean visible or not
	 */
	public LogWindow(final String title, boolean isVisible)
	{
		this(title, 300, 700);
		super.setVisible(isVisible);
	}
	
	/**
	 * Constructor.
	 * @param String title of the window
	 */
	public LogWindow(final String title)
	{
		this(title, 300, 700);
	}
	
	/**
	 * Default constructor.
	 */
	public LogWindow()
	{
		this("Log View");
	}
	
	
	public void showInfo(final String data)
	{
		this.textArea.append(data);
		this.getContentPane().validate();
	}
	
	public void showInfo()
	{
		try {
			this.showInfo(Log.readFile());
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
