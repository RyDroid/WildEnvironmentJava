/*
 * Copyright (C) 2014, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package graphics.utils;

import java.awt.GraphicsConfiguration;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class MainWindow extends JFrame
{
	public MainWindow(final String title, GraphicsConfiguration gc, final JMenuBar menuBar, final Image icon)
	{
		this(title, gc, menuBar);
		super.setIconImage(icon);
	}
	
	public MainWindow(final String title, GraphicsConfiguration gc, final JMenuBar menuBar)
	{
		this(title, gc);
		super.setJMenuBar(menuBar);
	}
	
	public MainWindow(final String title, final GraphicsConfiguration gc)
	{
		super(title, gc);
		this.setVisible(true);
	}
	
	public MainWindow(final GraphicsConfiguration gc)
	{
		super(gc);
		this.setTitle("Main Window");
		this.setVisible(true);
	}
	
	public MainWindow(final String title, final Image icon, int width, int height)
	{
		this(title, icon);
		this.setSize(width, height);
	}
	
	public MainWindow(final String title, final Image icon, int widthAndHeight)
	{
		this(title, icon, widthAndHeight, widthAndHeight);
	}
	
	public MainWindow(final String title, final ImageIcon icon, int widthAndHeight)
	{
		this(title, icon.getImage(), widthAndHeight, widthAndHeight);
	}
	
	public MainWindow(final String title, int width, int height)
	{
		this(title, null, width, height);
	}
	
	public MainWindow(final String title, int widthAndHeight)
	{
		this(title, null, widthAndHeight, widthAndHeight);
	}
	
	public MainWindow(final String title, Image icon)
	{
		this(title);
		super.setIconImage(icon);
	}
	
	public MainWindow(final Image icon)
	{
		this();
		super.setIconImage(icon);
	}
	
	public MainWindow(final ImageIcon icon)
	{
		this();
		this.setIconImage(icon);
	}
	
	public MainWindow(final String title)
	{
		super(title);
		this.setVisible(true);
	}
	
	/**
	 * Default constructor.
	 */
	public MainWindow()
	{
		this("Main Window");
	}
	
	/**
	 * Sets the image to be displayed as the icon for this window.
	 * This method can be used instead of setIconImages() to specify a single image as a window's icon.
	 * @param Icon the icon image to be displayed
	 */
	public void setIconImage(final ImageIcon icon)
	{
		super.setIconImage(icon.getImage());
	}
	
	protected void dispose(boolean closeProgram)
	{
		super.dispose();
		if(closeProgram) {
			System.exit(0);
		}
	}
	
	@Override
	public void dispose()
	{
		this.dispose(true);
	}
	
	protected void dispose(boolean closeProgram, boolean confirmExit)
	{
		String[] optionsButtons = {"Yes","No"};
		int promptResult = JOptionPane.showOptionDialog(
				getParent(),
				"Are you sure you want to exit?",
				"Confirm Exit",
				JOptionPane.DEFAULT_OPTION,
				JOptionPane.WARNING_MESSAGE,
				null,
				optionsButtons,
				optionsButtons[1]);
		if(promptResult == JOptionPane.OK_OPTION) {
			this.dispose(closeProgram);
		}
	}
	
	/**
	 * Exit the program.
	 * @param boolean confirm with a window to exit or not
	 */
	protected void exit(boolean confirmExit)
	{
		this.dispose(true, confirmExit);
	}
	
	/**
	 * Exit the program.
	 */
	protected void exit()
	{
		this.exit(true);
	}
}
