/*
 * Copyright (C) 2013, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * Copyright (C) 2013, Anthony Demongeot
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package graphics.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PrintPanel extends JPanel implements Printable
{
	public PrintPanel() {}
	
	// Copied and pasted? If yes, where?
	@Override
	public int print(final Graphics g, PageFormat pf, int pi)
	{
		// Print 2 pages
		if(pi >= 2) {
			return Printable.NO_SUCH_PAGE;
		}
		
		g.setFont(new Font("arial", Font.BOLD, 30));
		FontMetrics fm = g.getFontMetrics();
		int X = (int) pf.getImageableX();
		int Y = (int) pf.getImageableY();
		int W = (int) pf.getImageableWidth();
		int H = (int) pf.getImageableHeight();
		java.awt.geom.Rectangle2D r = fm.getStringBounds("" + pi, g);
		// Cadre noir autour de la partie Imageable...
		g.drawRect(X+1, Y+1, W-2, H-2);
		// impression du numéro de page
		// en rouge et au milieu de la page
		int x = (int) (W - r.getWidth())  / 2;
		int y = (int) (H - r.getHeight()) / 2;
		g.setColor(Color.RED);
		g.drawString(String.valueOf(pi), X+x, (int)(Y+y+r.getHeight()));
		return Printable.PAGE_EXISTS;
	}
}
